<?php

/**
 * Form to change the current leader of a small group
 */
require_once 'CRM/Core/Form.php';

//require_once('CustomPHP/FirePHPCore/fb.php');

/**
 * Form controller class
 *
 * @see http://wiki.civicrm.org/confluence/display/CRMDOC43/QuickForm+Reference
 */

class CRM_Smallgrouptracking_Form_ChangeSmallgroupLeader extends CRM_Core_Form {

    function buildQuickForm() {
        
        /* ADD FORM ELEMENTS */

        /**
         * Select Group for Leader to Change
         */
        $dcIds = $this->getDCGroups();
        

        // SMALL GROUP TO MOVE
        $this->addEntityRef('field_smallGroupGroup', ts('Select Small Group to Change Leader'), array(
            'api' => array(
                'params' => array(
                    'parents' => array('IN' => $dcIds) // only return small groups under a DC
                )),
            'select' => array(minimumInputLength => 0), // auto populate results
            'create' => FALSE,
            'entity' => 'group',
                ), TRUE
        );

        /**
         * Select New Leader
         */
        $this->addEntityRef('field_newLeader', ts('Select NEW Small Group Leader'), array(
            'contact_type' => 'Individual'
                ), true // require 
        );

        /**
         * Option to remove current leader from group
         */
        $this->addYesNo('removeCurrentLeader', ts('Remove Current Leader from Group?'), FALSE, TRUE);

        /**
         * BUTTONS
         */
        $this->addButtons(array(
            array(
                'type' => 'submit',
                'name' => ts('Change Small Group Leader'),
                'isDefault' => TRUE,
            ),
            array(
                'type' => 'cancel',
                'name' => ts('Cancel'),
                'isDefault' => FALSE,
            ),
        ));
        // export form elements
        $this->assign('elementNames', $this->getRenderableElementNames());
        parent::buildQuickForm();
    }

    function postProcess() {
        $values = $this->exportValues();
       // fb($values);

        $group_id = $values['field_smallGroupGroup'];
        $newLeader_id = $values['field_newLeader'];
        $removeLeader = $values['removeCurrentLeader']; // 0 or 1
        
        //$this->SetNewLeaderGroup(9869, 1);
        
        $this->ChangeGroupLeader($group_id, $newLeader_id, $removeLeader);

        /*
          $options = $this->getColorOptions();
          CRM_Core_Session::setStatus(ts('You picked color "%1"', array(
          1 => $options[$values['favorite_color']]
          )));
         *
         */
        CRM_Core_Session::setStatus(ts('Small Group Leader has been changed!'));
        parent::postProcess();
    }

    /**
     * Perform modifications to change the current group leader to the 
     * newly selected contact
     */
    function ChangeGroupLeader($group_id, $newLeader_id, $removeLeader) {
        //PhpConsole\Helper::register();
        $leaderTag = GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'small_group_leader_tag');
        $leadsMultiple = false; // keep track of whether current leader is leading multiple groups
        /*
         * Check to see if the new desired leader is currently leading
         * another group.
         */
        $leadsMultipleNew = $this->CheckForLeaderTag($newLeader_id,$leaderTag);
        if($leadsMultipleNew) {
            //CRM_Core_Session::setStatus(ts('ERROR: You cannot make someone a leader that is already leading another group (tagged as Small Group Leader)'));
            //return;
        }
        
        /*
         * Get current leader of the group
         */
        $currentLeader_id = $this->GetGroupLeader($group_id);

        /*
         * CURRENT LEADER
         * 
         * - Check if the current leader is leading another small group
         * 
         * IF NOT
         * - Remove Small Group Leader Tag
         * - Remove from THIS group if flagged
         */
        
        // Get tags associated with this contact
        $result = civicrm_api3('EntityTag', 'get', array(
            'sequential' => 1,
            'entity_id' => $currentLeader_id,
        ));
        foreach ($result['values'] as $value) {
            if($value['tag_id'] == $leaderTag) {
                $leadsMultiple = true; // current leader leads another group
                break;
            }
        }
        
        /**
         * Remove "Small Group Leader" Tag from the current leader
         * if they do NOT lead another group
         */
        if(!$leadsMultiple) {
            $this->RemoveLeaderTag($currentLeader_id);
        };

        /*
         * Remove the current leader from current group if desired
         * 
         * If they lead another group then only remove from group and not 
         * custom fields.
         */
        if ($removeLeader == 1 && !$leadsMultiple) {
            $this->RemoveCurrentLeader($group_id, $currentLeader_id);
        } else if ($removeLeader == 1)
        {
            $this->RemoveFromGroup($currentLeader_id, $group_id);
        }

        /*
         * NEW LEADER
         * 
         * This should eventually call the API so that we can have everythign
         * centralized in case of changes to how we deal with leaders
         * 
         * - Add to group
         * - Update custom fields
         * - Add small group leader tag
         */
        
        $this->SetSmallGroupTitle($newLeader_id,$group_id,$leadsMultipleNew);

        /*
         * Add New Leader to their group
         */
        $this->SetNewLeaderGroup($newLeader_id,$group_id,$leadsMultipleNew);
        
        /*
         * Set the small group leader and role fields
         */
        $this->SetCustomFields($newLeader_id);
        
        /*
         * Add "Small Group Leader" Tag to the leader
         */
        $this->SetLeaderTag($leaderTag,$newLeader_id);
        
        /*
         * Update custom field for all contacts in this group
         */
        $this->SetCustomFieldLeader($group_id, $newLeader_id);
    }
    
    /**
     * Check to determine if the desired new leader is already a leader
     * 
     * @param type $cid
     * @param type $leaderTag
     * @return boolean
     * true if contact has leader tag
     */
    function CheckForLeaderTag($cid, $leaderTag) {
        $result = civicrm_api3('EntityTag', 'get', array(
            'sequential' => 1,
            'entity_id' => $cid,
        ));
        
        foreach($result['values'] as $val) {
            if($leaderTag == $val['tag_id']) {
                // this contact is a leader, stop the the process!
                return true;
            }
        }
        return false;
        
    }

    /**
     * SET the small group leader tag on the new leader
     * @param type $leaderTag
     * @param type $newLeader_id
     */
    function SetLeaderTag($leaderTag,$newLeader_id) {
        $result = civicrm_api3('EntityTag', 'create', array(
            'sequential' => 1,
            'tag_id' => $leaderTag, // small group leader tag ID
            'entity_id' => $newLeader_id,
        ));
   //     fb('Add new leader tag');
    }
    
    /**
     * SET the Custom group of fields for the leader (leader/role)
     * @param type $newLeader_id
     */
    function SetCustomFields($newLeader_id) {
        /*
         *  UPDATE CUSTOM FIELDS
         */
        // Small Group -> Role/Leader
        $custom1 = 'custom_' . GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'small_group_member_role_custom_field');
        $custom2 = 'custom_' . GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'small_group_leader_custom_field');

        $result = civicrm_api3('CustomValue', 'create', array(
            'sequential' => 1,
            'entity_id' => $newLeader_id,
            $custom1 => "Regular", // role
            $custom2 => $newLeader_id, // leader
        ));
//        fb('Update new leader custom fields');
    }
    
    /**
     * Update custom field for all contacts in group to reflect new leader
     * 
     * 
     */
    function SetCustomFieldLeader($group_id, $newLeader_id) {
        $customField = 'custom_' . GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'small_group_leader_custom_field');
        
        /*
         * Get all contacts in group
         */
        $result = civicrm_api3('GroupContact', 'get', array(
            'sequential' => 1,
            'group_id' => $group_id,
        ));
        
        /*
         * Assign each member the new leader
         */
        foreach ($result['values'] as $val) {
            civicrm_api3('CustomValue', 'create', array(
                'sequential' => 1,
                'entity_id' => $val['contact_id'],
                $customField => $newLeader_id, // leader
            ));
        }
    }

    /**
     * Deal with the gorup for the new leader
     * 
     * @param type $newLeader_id
     * @param type $group_id
     */
    function SetNewLeaderGroup($newLeader_id, $group_id, $leadsMultiple) {
        /*
         * CHECK to see if the new leader is part of another small group
         * by comparing the contacts groups to a list of all small group ids
         * 
         */
        
        /*
         * Only remove from other groups IF they are not a small group leader
         * for another group
         * 
         */
        if(!$leadsMultiple) {
            $group_list = GetSmallGroupList();

            /*
             * Get groups this contact is in
             */
            $result = civicrm_api3('GroupContact', 'get', array(
                'sequential' => 1,
                'contact_id' => $newLeader_id,
            ));
            $contact_groups = $result['values'];


            /*
             * Compare the new leaders groups to all small groups
             * If a match is found, remove the contact from the group
             */
            foreach($contact_groups as $group) {
                if(in_array($group['group_id'], $group_list)) {
                   // fb($group['group_id'],'group match found');

                    /*
                     * Remove from matched group
                     */
                    $this->RemoveFromGroup($newLeader_id, $group['group_id']);
                    break;
                }
            }
        }

        /*
         * Add the leader to their own group
         */
        
        civicrm_api3('GroupContact', 'create', array(
            'sequential' => 1,
            'contact_id' => $newLeader_id,
            'group_id' => $group_id,
        ));
        // fb('add new leader to group');
         
         
    }

    /**
     * SET the new small group name
     * @param type $newLeader_id
     * @param type $group_id
     */
    function SetSmallGroupTitle($newLeader_id,$group_id,$leadsMultiple) {
        /*
         * Get new leader's name
         */
        $result = civicrm_api3('Contact', 'get', array(
            'sequential' => 1,
            'return' => array("display_name", "first_name", "last_name", "middle_name"),
            'id' => $newLeader_id,
        ));
        // fb('Get new leaders name');
        
        $displayName = $result['values'][0]['display_name'];
        //$lastname = $result['values'][0]['last_name'];
        //$firstname = $result['values'][0]['first_name'];
        //$middlename = $result['values'][0]['middle_name'];
   
        
        $sgTitle = $displayName . " Small Group"; // Group Title
        
        if($leadsMultiple) {
            $sgTitle = $sgTitle . '('. rand(0,99) .')';
        };
        /*
         * UPDATE GROUP TITLE
         * - Change the name of the group
         */
        $result = civicrm_api3('Group', 'create', array(
            'sequential' => 1,
            'id' => $group_id,
            'title' => $sgTitle,
        ));
        // fb('update group title');
        
    }
    
    /**
     * Remove small group leader tag from contact
     * @param type $leaderTag
     * @param type $currentLeader_id
     */
    function RemoveLeaderTag($currentLeader_id) {
        $leaderTag = GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'small_group_leader_tag');
        
        $result = civicrm_api3('EntityTag', 'delete', array(
            'sequential' => 1,
            'tag_id' => $leaderTag, // small group leader tag ID
            'entity_id' => $currentLeader_id,
        ));
        // fb($result,'Removed tag');
    }

    /**
     * Remove current leader from their own group
     * @param type $group_id
     * @param type $currentLeader_id
     */
    function RemoveCurrentLeader($group_id, $currentLeader_id) {
        /*
         * Clear Custom fields
         */
        $roleField = 'custom_' . GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'small_group_member_role_custom_field');
        $leaderField = 'custom_' . GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'small_group_leader_custom_field');
        
        $result = civicrm_api3('CustomValue', 'create', array(
            'sequential' => 1,
            'entity_id' => $currentLeader_id,
            $roleField => NULL, // role
            $leaderField => NULL, // leader
        ));
        /*
         * Remove from group
         */
        $this->RemoveFromGroup($currentLeader_id, $group_id);
    }

    /*
     * REMOVE CONTACT FROM A GROUP
     */
    function RemoveFromGroup($cid, $gid) {
        $result = civicrm_api3('GroupContact', 'delete', array(
            'sequential' => 1,
            'contact_id' => $cid,
            'group_id' => $gid, // group id
        ));
        // fb('Removed leader from group');
    }
    
    /**
     * Get list of all groups of DC
     * @return type
     */
    function getDCGroups() {

        /**
         * API call to get children of DC Root
         * This will only return immediate children
         */
        $result = civicrm_api3('Group', 'get', array(
            'sequential' => 1,
            'return' => "children",
            'id' => GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'discipleship_community_group'),
        ));
        $dcGroups = explode(',', $result['values'][0]['children']);

        //fb($dcGroups);
        return $dcGroups;
    }

    /**
     * Lookup the group leader for the selected group and return their CID
     * @param type $group_id
     * @return type
     */
    function GetGroupLeader($group_id) {

        $leaderTag = GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'small_group_leader_tag');
        $query = "select " .
                //"civicrm_group.parents as parent_id, " .
                //"civicrm_group.title, " .
                //"civicrm_group_contact.group_id, " .
                "civicrm_contact.id as contact_id " .
                //"civicrm_contact.display_name " .
                "from civicrm_group_contact " .
                "left join civicrm_contact on civicrm_contact.id = civicrm_group_contact.contact_id " .
                "left join civicrm_entity_tag on civicrm_entity_tag.entity_id = civicrm_contact.id " .
                "left join civicrm_group on civicrm_group.id = civicrm_group_contact.group_id and civicrm_group_contact.status = 'Added' " .
                "where civicrm_entity_tag.tag_id = " . $leaderTag . " " .
                "and civicrm_group.id = " . $group_id;

        $dao = CRM_Core_DAO::executeQuery($query);
        $results = array();
        while ($dao->fetch()) {
            $results[] = $dao->toArray();
        }
        //fb($results[0]['contact_id']);
        return $results[0]['contact_id'];
    }

    /**
     * Get the fields/elements defined in this form.
     *
     * @return array (string)
     */
    function getRenderableElementNames() {
        // The _elements list includes some items which should not be
        // auto-rendered in the loop -- such as "qfKey" and "buttons".  These
        // items don't have labels.  We'll identify renderable by filtering on
        // the 'label'.
        $elementNames = array();
        foreach ($this->_elements as $element) {
            /** @var HTML_QuickForm_Element $element */
            $label = $element->getLabel();
            if (!empty($label)) {
                $elementNames[] = $element->getName();
            }
        }
        return $elementNames;
    }

}
