<?php

require_once 'CRM/Core/Form.php';

/**
 * Form controller class
 *
 * @see http://wiki.civicrm.org/confluence/display/CRMDOC43/QuickForm+Reference
 */
class CRM_Smallgrouptracking_Form_CreateSmallGroupContact extends CRM_Core_Form {

    function buildQuickForm() {

        // add form elements
        // Pass some params to allow creation of contacts, set multiple and make the field required
        $this->addEntityRef('field_contact', ts('Select Contact'), array(
            'api' => array(
                'params' => array(
                    'contact_type' => 'Individual',
                //'last_name' => 'Gooding',
                )),
            'create' => TRUE,
            TRUE)
        );
        //28", "29", "33", "35", "46", "53", "54"
        //$dcIds = Array();
        $dcIds = $this->getDCGroups();
        //ChromePhp::log($dcIds);
        $this->addEntityRef('field_smallGroupGroup', ts('Select Small Group'), array(
            'api' => array(
                'params' => array(
                    'parents' => array('IN' => $dcIds) // only return small groups under a DC
                //'title'  => array('LIKE' => "%Small Group%"),
                // HOW CAN I SET MULTIPLE PARENTS!?!?!?!?
                //'parents' => array('29'=>29,'33'=>33,'35'=>35,'46'=>46),
                //'parents.2' => 33,
                //'parents.3' => 35,
                //'parents.4' => 46,
                //'parents' => array($this->getDCGroups()),
                //'parents' => 34, // populate this with list of DC Paster groups using api call
                //'last_name' => 'Gooding',
                )),
            'create' => FALSE,
            'entity' => 'group',
            TRUE)
        );

        $this->add(
                'select', // field type
                'field_role', // field name
                'Role', // field label
                array('' => ts('- select -'),
            'Member' => ts('Member '),
            'Visitor' => ts('Visitor'),
                ), // list of options
                true // is required
        );

        /*
          $this->addEntityRef('field_smallGroupGroup', ts('Select Small Group'),
          array('entity' => 'group'));
         */
        //$this->addEntityRef('field_2', ts('Select More Contacts'), array('create' => TRUE, 'multiple' => TRUE), TRUE);

        $this->addButtons(array(
            array(
                'type' => 'submit',
                'name' => ts('Add to Small Group'),
                'isDefault' => TRUE,
            ),
            array(
                'type' => 'cancel',
                'name' => ts('Cancel'),
                'isDefault' => FALSE,
            ),
        ));

        // export form elements
        $this->assign('elementNames', $this->getRenderableElementNames());
        parent::buildQuickForm();
    }

    function postProcess() {
        $values = $this->exportValues();
        $cid = $values['field_contact'];
        $sgid = $values['field_smallGroupGroup'];
        $role = $values['field_role'];

        $result = civicrm_api3('SmallGroupTracking', 'createsmallgroupcontact', array(
            'sequential' => 1,
            'cid' => $cid,
            'sgid' => $sgid,
            'role' => $role,
        ));

        parent::postProcess();
    }

    /**
     * Get the fields/elements defined in this form.
     *
     * @return array (string)
     */
    function getRenderableElementNames() {
        // The _elements list includes some items which should not be
        // auto-rendered in the loop -- such as "qfKey" and "buttons".  These
        // items don't have labels.  We'll identify renderable by filtering on
        // the 'label'.
        $elementNames = array();
        foreach ($this->_elements as $element) {
            $label = $element->getLabel();
            if (!empty($label)) {
                $elementNames[] = $element->getName();
            }
        }
        return $elementNames;
    }

    function getDCGroups() {
        $query = "SELECT * " .
                "FROM civicrm_group " .
                "WHERE title LIKE '%DC%'";
        //ChromePhp::log($query);

        $dao = CRM_Core_DAO::executeQuery($query);
        $results = array();
        $ids = array();
        while ($dao->fetch()) {
            $results[] = $dao->toArray();
            //$ids[] = $results['id'];
        }
        foreach ($results as $val) {
            $ids[] = $val['id'];
        }
        
        return $ids;
    }

}
