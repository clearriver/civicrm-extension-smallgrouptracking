<?php

require_once 'CRM/Core/Form.php';


/**
 * Form controller class
 *
 * @see http://wiki.civicrm.org/confluence/display/CRMDOC43/QuickForm+Reference
 */
class CRM_Smallgrouptracking_Form_CreateSmallGroup extends CRM_Core_Form {

    function buildQuickForm() {

        // add form elements
        // Contact to add as small group leader
        $this->addEntityRef('field_contact', ts('Select Contact'), array(
            'contact_type' => 'Individual'
            ), true // require 
                //'select', // field type
                //'createType', // field name 
                //'Create Type', // label - small group or DC
                //$this->getCreateOptions(), // list of options
                // is required
        );

        // Discipleship Community Group
        $this->addEntityRef('field_dcGroup', ts('Select DC Group'), array(
            'api' => array(
                'params' => array(
                    'parents' => GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'discipleship_community_group'), // DC Parent Group
                //'last_name' => 'Gooding',
                )),
            'select' => array(minimumInputLength=>0), // auto populate results
            'create' => FALSE,
            'entity' => 'group',
            ), TRUE
        );


        // display elements depending on what is selected
        /*
         * 
          $this->add(
          'select', // field type
          'favorite_color', // field name
          'Favorite Color', // field label
          $this->getColorOptions(), // list of options
          true // is required
          );
         */
        /*
          $this->add(
          'select', // field type
          'day', // field name
          'Day', // field label
          $this->getDays(), // list of options
          true // is required
          );
         */

        $this->addButtons(array(
            array(
                'type' => 'submit',
                'name' => ts('Submit'),
                'isDefault' => TRUE,
            ),
            array(
                'type' => 'cancel',
                'name' => ts('Cancel'),
                'isDefault' => FALSE,
            ),
        ));

        // export form elements
        $this->assign('elementNames', $this->getRenderableElementNames());
        parent::buildQuickForm();
    }

    // after clicking submit button
    function postProcess() {
        $values = $this->exportValues();
        $cid = $values['field_contact']; // id for the contact
        $dcGroupId = $values['field_dcGroup'];
       

        /*
          CRM_Core_Session::setStatus(ts(
          'Leader Firstname "%1"', 'Leader Lastname "%2"', array(
          1 => $options[$values['firstname']],
          2 => $options[$values['lastname']]
          )));
         * 
         */

        // CALL API
        
        $result = civicrm_api3('SmallGroupTracking', 'createsmallgroup', array(
            'sequential' => 1,
            'cid' => $cid,
            'dcGroupId' => $dcGroupId,
        ));
        if ($result['values'] == 'DB Error: already exists') {
            CRM_Core_Session::setStatus(ts(
                            'Group Already Exists: "%1"', array(
                1 => $result['values']
            )));

            //ChromePhp::log($result);
        }

        parent::postProcess();
    }

    function getDays() {
        $options = array(
            '' => ts('- select -'),
            'Sunday' => ts('Sunday'),
            'Monday' => ts('Monday'),
            'Tuesday' => ts('Tuesday'),
            'Wednesday' => ts('Wednesday'),
            'Thursday' => ts('Thursday'),
            'Friday' => ts('Friday'),
            'Saturday' => ts('Saturday'),
        );
        return $options;
    }

    /**
     * Get the fields/elements defined in this form.
     *
     * @return array (string)
     */
    function getRenderableElementNames() {
        // The _elements list includes some items which should not be
        // auto-rendered in the loop -- such as "qfKey" and "buttons".  These
        // items don't have labels.  We'll identify renderable by filtering on
        // the 'label'.
        $elementNames = array();
        foreach ($this->_elements as $element) {
            $label = $element->getLabel();
            if (!empty($label)) {
                $elementNames[] = $element->getName();
            }
        }
        return $elementNames;
    }

}
