<?php

require_once 'CRM/Core/Form.php';
// require_once('CustomPHP/FirePHPCore/fb.php');

CRM_Core_Resources::singleton()->addScriptFile('org.namelessnetwork.smallgrouptracking', 'js/Settings.js');

/**
 * Form controller class
 *
 * @see http://wiki.civicrm.org/confluence/display/CRMDOC43/QuickForm+Reference
 */
class CRM_Smallgrouptracking_Form_Settings extends CRM_Core_Form {
  function buildQuickForm() {
      
    // add form elements
    $this->addFormFields();
    
    $this->addButtons(array(
        array(
          'type' => 'submit',
          'name' => ts('Submit'),
          'isDefault' => TRUE,
        ),
        array(
            'type' => 'cancel',
            'name' => ts('Cancel'),
            'isDefault' => FALSE,
        )
    ));
    
    // load configuration data
    $this->assign('customFields', GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'CustomFields'), TRUE);
    $this->assign('showServingArea', GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'ShowServingArea'));

    // export form elements
    $this->assign('elementNames', $this->getRenderableElementNames());
    parent::buildQuickForm();
  }
  
  function addFormFields() {
    $this->add(
      'select', // field type
      'custom_table_dummy', // field name
      'Custom Group', // field label
      $this->getCustomTables(), // list of options
      false // is required
    );
    $this->add(
      'select', // field type
      'custom_field_dummy', // field name
      'Custom Field', // field label
      array('' => ts('- select -')), // list of options
      false // is required
    );
    $this->add(
      'text', // field type
      'alias_dummy', // field name
      'Alias', // field label
      false // is required
    );
  }

  function postProcess() {
    $values = $this->getSubmitValues();
    
    $customFields = '';
    
    // Get all POST fields and append them to the customFields string
    for ($index = 0; true; $index++) {
        if (array_key_exists('custom_table_' . $index, $values)
                && array_key_exists('custom_field_' . $index, $values)
                && $values['custom_table_' . $index] != ''
                && $values['custom_field_' . $index] != '') {
            if (\strlen($customFields) > 0) {
                $customFields .= ',';
            }
            $customFields .= $values['custom_table_' . $index].':'
                            .$values['custom_field_' . $index].':'
                            .$values['alias_' . $index];
        } elseif (!array_key_exists('custom_table_' . $index, $values)
                && !array_key_exists('custom_field_' . $index, $values)) {
            break;
        }
    }
    
    // Save setting to database
    // fb($customFields, 'Custom Fields Selected');
    CreateCiviSetting('org.namelessnetwork.smallgrouptracking', 
            'CustomFields', $customFields);
    CreateCiviSetting('org.namelessnetwork.smallgrouptracking', 
            'ShowServingArea', $values['showServingArea']);
    
    // Load changed settings.
    $this->assign('customFields', GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'CustomFields'), TRUE);
    $this->assign('showServingArea', GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'ShowServingArea'));
    
    parent::postProcess();
  }
  
  function getCustomTables() {
    $tables = array(
      '' => ts('- select -')
    );

    $query = "SELECT title, table_name "
            ."FROM civicrm_custom_group";

    //$dao = CRM_Core_DAO::executeQuery("select database()");
    $dao = CRM_Core_DAO::executeQuery($query);

    $results = array();
    while ($dao->fetch()) {
        $results[] = $dao->toArray();
    }
    
    foreach ($results as $result) {
        $tables[$result['table_name']] = $result['title'];
    }
    
    return $tables;
  }
  
  function getCustomTableColumns() {
    $columns = array(
      '' => ts('- select -')
    );

    $query = "SELECT column_name "
            ."FROM information_schema.columns "
            ."WHERE table_name LIKE 'civicrm_value_%' "
            ."AND table_schema IN (SELECT database())";

    //$dao = CRM_Core_DAO::executeQuery("select database()");
    $dao = CRM_Core_DAO::executeQuery($query);

    $results = array();
    while ($dao->fetch()) {
        $results[] = $dao->toArray();
    }
    
    foreach ($results as $result) {
        $columns[$result['column_name']] = $result['column_name'];
    }
    
    return $columns;
  }

  /**
   * Get the fields/elements defined in this form.
   *
   * @return array (string)
   */
  function getRenderableElementNames() {
    // The _elements list includes some items which should not be
    // auto-rendered in the loop -- such as "qfKey" and "buttons".  These
    // items don't have labels.  We'll identify renderable by filtering on
    // the 'label'.
    $elementNames = array();
    foreach ($this->_elements as $element) {
      /** @var HTML_QuickForm_Element $element */
      $label = $element->getLabel();
      if (!empty($label)) {
        $elementNames[] = $element->getName();
      }
    }
    return $elementNames;
  }
}
