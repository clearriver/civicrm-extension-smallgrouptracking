<?php

require_once 'CRM/Core/Form.php';
/**
 * Form controller class
 *
 * @see http://wiki.civicrm.org/confluence/display/CRMDOC43/QuickForm+Reference
 */
class CRM_Smallgrouptracking_Form_ChangeSmallGroupDC extends CRM_Core_Form {

    function buildQuickForm() {

        $dcIds = $this->getDCGroups();
        
        // SMALL GROUP TO MOVE
        $this->addEntityRef('field_smallGroupGroup', ts('Select Small Group'), array(
            'api' => array(
                'params' => array(
                    'parents' => array('IN' => $dcIds) // only return small groups under a DC
                )),
            'select' => array(minimumInputLength=>0), // auto populate results
            'create' => FALSE,
            'entity' => 'group',
            ),TRUE
        );
        
        // Discipleship Community Group
        $this->addEntityRef('field_dcGroup', ts('Select DC Group'), array(
            'api' => array(
                'params' => array(
                    'parents' => GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'discipleship_community_group'), // DC Parent Group
                //'last_name' => 'Gooding',
                )),
            'select' => array(minimumInputLength=>0), // auto populate results
            'create' => FALSE,
            'entity' => 'group',
            ),TRUE
        );

        // BUTTONS
        $this->addButtons(array(
            array(
                'type' => 'submit',
                'name' => ts('Move to DC'),
                'isDefault' => TRUE,
            ),
            array(
                'type' => 'cancel',
                'name' => ts('Cancel'),
                'isDefault' => FALSE,
            ),
        ));

        // export form elements
        $this->assign('elementNames', $this->getRenderableElementNames());
        parent::buildQuickForm();
    }

    function postProcess() {
        // GET VALUES
        $values = $this->exportValues();
        
        $sggId = $values['field_smallGroupGroup']; // id for the contact
        $dcGroupId = $values['field_dcGroup'];
        // REMOVE CURRENT PARENT DC
        
        // Get the parent
        /*
        $result = civicrm_api3('Group', 'get', array(
            'return' => "parents",
            'id' => 38,
        ));
         * 
         */
        CRM_Contact_BAO_GroupNesting::removeAllParentForChild($sggId);
        CRM_Contact_BAO_GroupNesting::add($dcGroupId,$sggId);
        //CRM_Contact_BAO_GroupNesting::remove($result['values'][0]['parents'], $group->id); // will fail if more than 1 parent
        
        // UPDATE THE NESTING CACHE
        CRM_Contact_BAO_GroupNestingCache::update();
        parent::postProcess();
    }

    function getDCGroups() {
      
        /**
         * API call to get children of DC Root
         * This will only return immediate children
         */
        $result = civicrm_api3('Group', 'get', array(
            'sequential' => 1,
            'return' => "children",
            'id' => GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'discipleship_community_group'),
        ));
        $dcGroups = explode(',', $result['values'][0]['children']) ;
        
        return $dcGroups;
    }

    /**
     * Get the fields/elements defined in this form.
     *
     * @return array (string)
     */
    function getRenderableElementNames() {
        // The _elements list includes some items which should not be
        // auto-rendered in the loop -- such as "qfKey" and "buttons".  These
        // items don't have labels.  We'll identify renderable by filtering on
        // the 'label'.
        $elementNames = array();
        foreach ($this->_elements as $element) {
            $label = $element->getLabel();
            if (!empty($label)) {
                $elementNames[] = $element->getName();
            }
        }
        return $elementNames;
    }

}
