<?php

require_once 'CRM/Core/Form.php';


/**
 * Form controller class
 *
 * @see http://wiki.civicrm.org/confluence/display/CRMDOC43/QuickForm+Reference
 */
class CRM_Smallgrouptracking_Form_CreateDCGroup extends CRM_Core_Form {

    function buildQuickForm() {

        // First Name
        $this->add(
                "text", "firstname", "First Name", "", true
        );
        // Last Name
        $this->add(
                "text", "lastname", "Last Name", "", true
        );

        $this->add(
                'select', // field type
                'day', // field name
                'Day', // field label
                $this->getDays(), // list of options
                true // is required
        );

        $this->addButtons(array(
            array(
                'type' => 'submit',
                'name' => ts('Submit'),
                'isDefault' => TRUE,
            ),
            array(
                'type' => 'cancel',
                'name' => ts('Cancel'),
                'isDefault' => FALSE,
            ),
        ));

        // export form elements
        $this->assign('elementNames', $this->getRenderableElementNames());
        parent::buildQuickForm();
    }

    // after clicking submit button
    function postProcess() {
        $values = $this->exportValues();
        
        $day = $values['day'];
        $firstname = $values['firstname'];
        $lastname = $values['lastname'];

        // CALL API
        $result = civicrm_api3('SmallGroupTracking', 'createsmallgroupdc', array(
            'sequential' => 1,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'day' => $day,
        ));

        if ($result['values'] == 'DB Error: already exists') {
            CRM_Core_Session::setStatus(ts(
                            'DC Group Already Exists: "%1"', array(
                1 => $result['values']
            )));

            
        }

        parent::postProcess();
    }

    function getDays() {
        $options = array(
            '' => ts('- select -'),
            'Sunday' => ts('Sunday'),
            'Monday' => ts('Monday'),
            'Tuesday' => ts('Tuesday'),
            'Wednesday' => ts('Wednesday'),
            'Thursday' => ts('Thursday'),
            'Friday' => ts('Friday'),
            'Saturday' => ts('Saturday'),
        );
        return $options;
    }

    /**
     * Get the fields/elements defined in this form.
     *
     * @return array (string)
     */
    function getRenderableElementNames() {
        // The _elements list includes some items which should not be
        // auto-rendered in the loop -- such as "qfKey" and "buttons".  These
        // items don't have labels.  We'll identify renderable by filtering on
        // the 'label'.
        $elementNames = array();
        foreach ($this->_elements as $element) {
            $label = $element->getLabel();
            if (!empty($label)) {
                $elementNames[] = $element->getName();
            }
        }
        return $elementNames;
    }

}
