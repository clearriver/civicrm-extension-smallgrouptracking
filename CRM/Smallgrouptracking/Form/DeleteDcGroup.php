<?php

require_once 'CRM/Core/Form.php';
/**
 * Form controller class
 *
 * @see http://wiki.civicrm.org/confluence/display/CRMDOC43/QuickForm+Reference
 */
class CRM_Smallgrouptracking_Form_DeleteDcGroup extends CRM_Core_Form {

    function buildQuickForm() {

        // add form elements
        // DC Group Lookup
        $this->addEntityRef('field_dc', ts('Select DC'), array(
            'entity' => 'group',
            'api' => array(
                'params' => array(
                    'parents' => GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'discipleship_community_group'), // filters results to groups in DC
                )),
            'select' => array(minimumInputLength => 0)), // auto populate results),
                true // require 
        );
        $this->add('checkbox', 'force_delete', ts('FORCE DELETE <br><br><b>WARNING:</b> FORCE DELETE will remove all groups within the DC. ONLY do this if you know what you are doing.')); // to allow force deletion of DC and child groups
        $this->addButtons(array(
            array(
                'type' => 'submit',
                'name' => ts('Delete DC Group'),
                'isDefault' => TRUE,
            ),
             array(
                'type' => 'cancel',
                'name' => ts('Cancel'),
                'isDefault' => FALSE,
            ),
        ));

        // export form elements
        $this->assign('elementNames', $this->getRenderableElementNames());
        parent::buildQuickForm();
    }
    
    // after clicking submit button
    function postProcess() {
        $values = $this->exportValues();
        $force = $values['force_delete'];
        $id = $values['field_dc']; // id for the DC Group
        // REMOVE ALL MEMBERS OF THE SMALL GROUP
        // CALL API
        $result = civicrm_api3('SmallGroupTracking', 'deletedcgroup', array(
            'sequential' => 1,
            'id' => $id,
            'force' => $force,
        ));
        
        if ($result['values']['error'] == 1) {
            CRM_Core_Session::setStatus(
                            ts($result['values']['alert'] . ". Please move the small groups to another DC or FORCE DELETE."), ts("Error"), 'error'
            );
        } else {
            CRM_Core_Session::setStatus(
                    ts('DC has been successfully removed'), ts("Success"), 'success'
            );
        }

        //}
        parent::postProcess();
    }

    /**
     * Get the fields/elements defined in this form.
     *
     * @return array (string)
     */
    function getRenderableElementNames() {
        // The _elements list includes some items which should not be
        // auto-rendered in the loop -- such as "qfKey" and "buttons".  These
        // items don't have labels.  We'll identify renderable by filtering on
        // the 'label'.
        $elementNames = array();
        foreach ($this->_elements as $element) {
            $label = $element->getLabel();
            if (!empty($label)) {
                $elementNames[] = $element->getName();
            }
        }
        return $elementNames;
    }

}
