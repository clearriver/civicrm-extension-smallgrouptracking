<?php

require_once 'CRM/Core/Form.php';

/**
 * Form controller class
 *
 * @see http://wiki.civicrm.org/confluence/display/CRMDOC43/QuickForm+Reference
 */
class CRM_Smallgrouptracking_Form_DeleteSmallGroup extends CRM_Core_Form {

    function buildQuickForm() {

        /*
         * CREATE GROUP SELECTION LIST FOR EACH DC
         * Need to have them separated
         * 
         */
        
        // add form elements
        // SELECT THE DC that the group is in
        $this->addEntityRef('field_dc_group', ts('Select DC for the Leader'), array(
            'entity' => 'group',
            'api' => array(
                'params' => array(
                    //'tag' => GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'small_group_leader_tag'), // filters results to just those with Small Group Leader Tag
                    'parents' => array('IN' => array(34)), // this will get the DC we want to select
                )),
            'select' => array(minimumInputLength => 0),// auto populate results),
            'id' => 'field_dc_group'), 
                true // require 
        );
        
        $this->addEntityRef('field_sg_group', ts('Select Small Group Leader'),array(
            'entity' => 'group',
            'id' => 'field_sg_group', 
            'select' => array(minimumInputLength => 0),
            ),
                true
        );
         
        /*
        $this->addEntityRef('field_contact', ts('Select Small Group Leader'), array(
            'entity' => 'group',
            'api' => array(
                'params' => array(
                    //'tag' => GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'small_group_leader_tag'), // filters results to just those with Small Group Leader Tag
                    'parents' => array('IN' => array(34,202)), // this will get the DC we want to select
                )),
            'select' => array(minimumInputLength => 0)), // auto populate results),
                true // require 
        );
        */

        
        // Get DC Groups
        $result = civicrm_api3('Group', 'get', array(
            'sequential' => 1,
            'parents' => 34, // DC Main Group ID sgt_settings.discipleship_community_group,
        ));
        
        // loop through and get child groups for each DC
        
           
        $this->addButtons(array(
            array(
                'type' => 'submit',
                'name' => ts('Delete Small Group'),
                'isDefault' => TRUE,
            ),
             array(
                'type' => 'cancel',
                'name' => ts('Cancel'),
                'isDefault' => FALSE,
            ),
        ));

        // export form elements
        $this->assign('elementNames', $this->getRenderableElementNames());
        parent::buildQuickForm();
    }
    
    // after clicking submit button
    function postProcess() {
        $values = $this->exportValues();
        ////ChromePhp::log($values);
        //$cid = $values['field_contact']; // id for the contact    
        $sgid = $values['field_sg_group']; // small group group id to delete
        // REMOVE ALL MEMBERS OF THE SMALL GROUP
        // CALL API
        
        $result = civicrm_api3('SmallGroupTracking', 'deletesmallgroup', array(
            'sequential' => 1,
            'sgid' => $sgid,
        ));
        

        CRM_Core_Session::setStatus(ts(
                "Small Group Deleted"
//                        'Result: "%1"', array(
//            1 => $result['values']
//)
                ));

        //}
        parent::postProcess();
    }

    /**
     * Get the fields/elements defined in this form.
     *
     * @return array (string)
     */
    function getRenderableElementNames() {
        // The _elements list includes some items which should not be
        // auto-rendered in the loop -- such as "qfKey" and "buttons".  These
        // items don't have labels.  We'll identify renderable by filtering on
        // the 'label'.
        $elementNames = array();
        foreach ($this->_elements as $element) {
            $label = $element->getLabel();
            if (!empty($label)) {
                $elementNames[] = $element->getName();
            }
        }
        return $elementNames;
    }

}
