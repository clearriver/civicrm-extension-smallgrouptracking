<?php
/*
 * Adding FORMS
 * From the extension dir
 * ~/tools/civix/civix generate:form CreateSmallGroup civicrm/createsmallgroup
 */

require_once 'CRM/Core/Page.php';


class CRM_Smallgrouptracking_Page_Smallgrouptracking extends CRM_Core_Page {
   
    function run() {
        
        // Example: Set the page-title dynamically; alternatively, declare a static title in xml/Menu/*.xml
        CRM_Utils_System::setTitle(ts('Small Group Tracking'));

        // Example: Assign a variable for use in a template
        $this->assign('currentTime', date('Y-m-d H:i:s'));
        
        // Assign Settings to template
        $this->assign('settings', GetCiviSettingAll('org.namelessnetwork.smallgrouptracking'));
        
        /*
         * Assign Custom Field Column Headers to template
         * 
         * Get setting CustomFields and break on the field delimiter.
         * Then iterate over each field and break of field element delimiter.
         * The field element arrays are then appended together to be sent to the template
         */
        $customColumnHeaders = array();
        
        $customFields = explode(',', GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'CustomFields'));
        
        foreach ($customFields as $customField) {
            $fieldElements = explode(':', $customField);
            array_push($customColumnHeaders, $fieldElements);
            //// fb($fieldElements[2],'$fieldElements[2]');
        }
        
        $this->assign_by_ref('customHeaders', $customColumnHeaders);
        //// fb($customColumnHeaders, "customHeaders");
        
        // Get setting ShowServingArea and assign a boolean string for use in the template
        $showServingArea = GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'ShowServingArea') == 'checked' ? 'true' : 'false';
        $this->assign_by_ref('showServingArea', $showServingArea);
        
        CRM_Core_Region::instance('html-header')->add(array(
            'markup' => '<meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />'
        ));
        
        //$ext_settings = GetCiviSettingAll('org.namelessnetwork.smallgrouptracking');
        
        // Get list of small group leaders
        //$tagId = smallgrouptracking_civicrm_GetTagId('Small Group Leader');

        //if ($tagId) {
        //    $groupLeaders = smallgrouptracking_civicrm_GetContactWithTag($tagId);
        //    $this->assign('groupLeaders', $groupLeaders);
        //}
        
        // SMALL GROUP LEADERS
        //$queryResults = smallgrouptracking_civicrm_sgl_view();
        //$this->assign('queryResults', $queryResults);
        //$this->assign('groupLeaders', $queryResults);
        //print_r2($queryResults[0]);
        
        // SMALL GROUP MEMBERS
        //$leaderId = 2; // leader we are finding members of
        //$queryResults = smallgrouptracking_civicrm_sg_view($leaderId);
        //$this->assign('queryResults', $queryResults);
        //print_r2($queryResults[0]);
        
        //$obj = (object) smallgrouptracking_civicrm_sg_view($leaderId);
        //$this->assign("common",$obj);
        //
            // Check for custom permission
        // here we will pass a variable telling if we can edit. If we can, show elements
        // based on that permission
        if (CRM_Core_Permission::check('edit SmallGroupTracking')) {
            $this->assign('editPermission', true);
        } else {
            $this->assign('editPermission', false);
        }
        if (CRM_Core_Permission::check('access SmallGroupTracking') || 
                CRM_Core_Permission::check('edit SmallGroupTracking')) {
            parent::run();
        } else {
            echo 'Access Denied';
        }
    }

}

/**
 * 
 * @param type $name The name of the tag you are searching for
 * https://clearriver.namelessnetwork.org/civicrm/api/explorer#/civicrm/ajax/rest?entity=Tag&action=get&debug=1&sequential=1&json=1&name=Small%20Group%20Leader
 */
function smallgrouptracking_civicrm_GetTagId($name) {
    $params = array(
        'version' => 3,
        'sequential' => 1,
        'name' => $name,
    );
    $result = civicrm_api('Tag', 'get', $params);
    if ($result['values'] == NULL) {
        //print_r2($result);
        return NULL;
    } else {
        //print_r2($result);
        return $result['id'];
    }
}

/**
 * 
 * @param type $tagId
 * https://clearriver.namelessnetwork.org/civicrm/api/explorer#/civicrm/ajax/rest?entity=Contact&action=get&debug=1&sequential=1&json=1&tag=6
 * Small group leader tag = 6
 */
function smallgrouptracking_civicrm_GetContactWithTag($tagId) {
    $leaderNames = '';
    $params = array(
        'version' => 3,
        'sequential' => 1,
        'tag' => $tagId,
    );
    $result = civicrm_api('Contact', 'get', $params);
    if ($result['values'] == NULL) {
        print_r2($result);
        return NULL;
    } else {
        foreach ($result['values'] as $value) {
            $leaderNames[] = $value['display_name'];    // pushes into the array
            //echo '</br>' . $value['display_name'];
        }
        return $leaderNames;
    }
}

function print_r2($val) {
    echo '<pre>';
    print_r($val);
    echo '</pre>';
}

function SQLTest() {

    //$whereClause = CRM_Contact_BAO_ProximityQuery::where($latitude, $longitude, $distance);
/*
    $query = "
        SELECT    civicrm_contact.id as contact_id,
                  civicrm_contact.display_name as display_name
        FROM      civicrm_contact
        LEFT JOIN civicrm_address ON civicrm_contact.id = civicrm_address.contact_id
        WHERE     $whereClause
        ";
*/
    
    $query = "SELECT 
civicrm_contact_sg_view.display_name
,civicrm_activity.subject
,civicrm_activity.activity_date_time
,civicrm_option_value.label as `activity_status`

FROM civicrm_contact_sg_view
left join civicrm_activity_contact
on civicrm_contact_sg_view.id = civicrm_activity_contact.contact_id
left join civicrm_activity
on civicrm_activity.id = civicrm_activity_contact.activity_id

left join civicrm_option_value
on civicrm_option_value.option_group_id = 25 #25 is the group of options for activity status
and civicrm_activity.status_id = civicrm_option_value.value
where civicrm_activity.activity_type_id = 58
";
            
    $dao = CRM_Core_DAO::executeQuery($query);
    $myArray = array();
    while ($dao->fetch()) {
        $myArray[] = $dao->toArray();
    }
    return $myArray;
}

// SMALL GROUP LEADER VIEW
function smallgrouptracking_civicrm_sgl_view() {
	
    $view = "civicrm_contact_sgl_view"; // view created by me
    
    $query = "SELECT *"
            //. "display_name"
            . " FROM " . $view;
    //$query = "SELECT * FROM clearriver_civicrm.civicrm_contact_sgl_view";
    $dao = CRM_Core_DAO::executeQuery($query);
    $results = array();
    while ($dao->fetch()) {
        $results[] = $dao->toArray();
    }
    return $results;
	/*
	$params = array(
        'version' => 3,
        'sequential' => 1,
    );
    $result = civicrm_api('SmallGroupTrackingLeader', 'get', $params);
	return $result;
	 * */
	 
}

// GET SMALL GROUP MEMBERS

function smallgrouptracking_civicrm_sg_view($leaderId) {
    $view = "civicrm_contact_sg_view"; // view created by me
    
    $query = "SELECT *"
            //. "display_name"
            . " FROM " . $view
            . "where group_leader_115=".$leaderId;
    
    $query = "select * from civicrm_contact_sg_view where group_leader_115=".$leaderId;
    //$query = "SELECT * FROM clearriver_civicrm.civicrm_contact_sgl_view";
    $dao = CRM_Core_DAO::executeQuery($query);
    $results = array();
    while ($dao->fetch()) {
        $results[] = $dao->toArray();
    }
    return $results;
}
