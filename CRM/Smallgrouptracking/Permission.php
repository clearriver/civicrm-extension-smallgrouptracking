<?php

class CRM_Smallgrouptracking_Permission extends CRM_Core_Permission {

  /**
   * Returns an array of permissions defined by this extension. Modeled off of
   * CRM_Core_Permission::getCorePermissions().
   *
   * @return array Keyed by machine names with human-readable labels for values
   */
  public static function getSmallgrouptrackingPermissions() {
        $prefix = ts('CiviCRM SmallGroup Tracking', array('domain' => 'org.namelessnetwork.smallgrouptracking')) . ': ';
        $permissions = array(
            'access SmallGroupTracking' => array(
                $prefix . ts('access SmallGroupTracking'),
                ts('View and select discipleship community groups, small groups, and add attendance records'),
            ),
            'edit SmallGroupTracking' => array(
                $prefix . ts('edit SmallGroupTracking'),
                ts('Manage the creation/deletion of Discipleship communities & small groups'),
            ),
        );

        //foreach ($permissions as $name => $attr) {
        //    $permissions[$name] = array_shift($attr);
        //}
        return $permissions;
    }

    /**
   * Given a permission string or array, check for access requirements. For
   * VOL-71, if this is a permissions-challenged Joomla instance, don't enforce
   * CiviVolunteer-defined permissions.
   *
   * @param mixed $permissions The permission(s) to check as an array or string.
   *        See parent class for examples.
   * @return boolean
   */
  public static function check($permissions) {
    $permissions = (array) $permissions;

    if (!CRM_Core_Config::singleton()->userPermissionClass->isModulePermissionSupported()) {
      array_walk_recursive($permissions, function(&$v, $k) {
        if (array_key_exists($v, CRM_Smallgrouptracking_Permission::getSmallgrouptrackingPermissions())) {
          $v = CRM_Core_Permission::ALWAYS_ALLOW_PERMISSION;
        }
      });
    }

    return parent::check($permissions);
  }
}
