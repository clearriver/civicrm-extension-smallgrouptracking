<?php

/*
 * 
 */


//PhpConsole\Helper::register();

function civicrm_api3_small_group_tracking_getreport($params) {
    $totalWeeks = 52; // per year
    //$start_date = $params['start_date'];
    //$end_date = $params['end_date'];
    $year = $params['year'];
    $year = '2016';
    $prevYear = $year-1 . '52'; 
    $nextYear = $year+1 . '01';
    $start_date = $year . '-01-01';
    $end_date = $year . '-12-31';
    
    //PC::prevYear($prevYear);
    $query = "
        select 
        (select parent_group_id from civicrm_group_nesting where child_group_id = group_group_id limit 1) as dc_group_id,
           gr.title as dc_leader,
        cc.display_name as group_leader, 
           TRIM(LEADING '0' FROM MID(YEARWEEK(meeting_date),5,2)) weekNumber,
           year(meeting_date) year,
           log.meeting_date,
           log.meeting_type,
           log.present_count,
           log.group_leader_cid
        from custom_smallgroup_meeting_log log
        inner join civicrm_contact cc on cc.id = log.group_leader_cid
        inner join civicrm_group gr on gr.id = (select parent_group_id from civicrm_group_nesting where child_group_id = group_group_id limit 1)
        WHERE YEARWEEK(meeting_date) > '".$prevYear."' AND YEARWEEK(meeting_date) < '".$nextYear."'        
        order by group_leader, weekNumber";

    $dao = CRM_Core_DAO::executeQuery($query);

   
    $dcGroupId = null;
    while ($dao->fetch()) {
        
        $attendance[$dao->group_leader_cid][$dao->weekNumber] = $dao->toArray();
        if($dao->dc_group_id != $dcGroupId) {
            $dcGroupId = $dao->dc_group_id;
            $dcPastors[$dao->dc_group_id] = array('leader' => $dao->dc_leader, 'id' => $dao->dc_group_id );
        }
        
        //$results[$dao->weekNumber][$dao->weekNumber][] = array(array($dao->group_leader => $dao->group_leader_cid),array($dao->toArray()));
    }
    //PC::results($attendance);
    //PC::dcPastors($dcPastors);
    $groupCount = sizeof($attendance); // number of small groups we have data for
    //PC::groupCount($groupCount);
    $dcPastors = SortDC($dcPastors); // sorts array in DC order
    //PC::attendance($attendance);
    
    /*
     * Organize into object we will return
     * Object[year]
     */
    $resultObj = null; // define empty object
    $weekCount = 0;
    $sglid = null;
    $weekHeader = null; //will holds the table headers for the start date of each week
    $count = 0;
    
    $dcAttendanceCount = null; // holds totals attendance for each DC each week
    
    /*
     * FILL IN MISSING WEEKS
     * Loop through and add missing weeks to each leader
     */
    foreach ($attendance as $key => &$value) {
        $count=1; // for only populating weekheader once in for lop
        $cpValues = array_pop($value); //get last attendance record so we can get info we need
        $dc_group_id = $cpValues['dc_group_id'];
        $dc_leader = $cpValues['dc_leader'];
        $group_leader = $cpValues['group_leader'];
        $group_leader_cid = $cpValues['group_leader_cid'];
        
        for ($i = 1; $i <= $totalWeeks; $i++) {
            if(!isset($value[$i])) {
                $present_count = 0;
                $value[$i] = array(
                    'dc_group_id' => $dc_group_id,
                    'dc_leader' => $dc_leader,
                    'group_leader' => $group_leader,
                    'weekNumber' => $i,
                    'year' => $year,
                    'present_count' => $present_count,
                    'group_leader_cid' => $group_leader_cid,
                    'meeting_type' => 'missing',
                    );
                
                    $dcAttendanceCount = DCWeekTotalAttendance($dcAttendanceCount, $value, $i);
            }
            else {
                $dcAttendanceCount = DCWeekTotalAttendance($dcAttendanceCount, $value, $i);
            }
            
            if($count==1) {
                $weekHeader[$i] = GetWeekStartDate($i, $year);   
            }
        }
    }
    //PC::FillWeeks($attendance);

    $week_attendance_total = WeekAttendanceTotal($dcAttendanceCount, $totalWeeks);
    
    $results = array('attendance' => $attendance, 'dc' => $dcPastors, 'weekHeader' => $weekHeader, 'dc_attendance_total' => $dcAttendanceCount, 'week_attendance_total' => $week_attendance_total);
    return civicrm_api3_create_success($results);
}

/**
 * Calculate the total attendance for each week for each DC
 * 
 * @param type $dcAttendanceCount
 * @param type $value
 * @param type $i
 * @return type
 */
function DCWeekTotalAttendance($dcAttendanceCount, $value, $i) {
    $present_count = $value[$i]['present_count'];
    if (isset($dcAttendanceCount[$i][$value[$i]['dc_group_id']])) {
        $dcAttendanceCount[$i][$value[$i]['dc_group_id']] += $present_count;
    } else {
        $dcAttendanceCount[$i][$value[$i]['dc_group_id']] = $present_count;
    }
    return $dcAttendanceCount;
}

function WeekAttendanceTotal ($dcAttendanceCount, $totalWeeks) {
    //PC::dcAttendanceCount($dcAttendanceCount);
    $week_attendance_total = null;
    // Loop through all the weeks
    for ($i = 1; $i <= $totalWeeks; $i++) { 
        
        /**
         * Loop through each week and add up totals for entire week
         */
        foreach ($dcAttendanceCount[$i] as $key => $value) {

            if(isset($week_attendance_total[$i])) {
                $week_attendance_total[$i] += $value;
            }
            else {
                $week_attendance_total[$i] = $value;
            }
            
        }
    }
    return $week_attendance_total;
}

/*
 * Organize DC pastors by day of the week
 * 
 * I don't think this is actually working right now
 */
function SortDC($pastors) {
    $sorted = null;
    $monday = array();
    $tuesday = array();
    $wednesday = array();
    $thursday = array();
    $friday = array();
    $saturday = array();
    $sunday = array();
    
    $previousDay = null;
    foreach($pastors as $value) {
        $explode = explode('-', $value['leader']);
        if($explode[0] == 'Monday') {
            //array_unshift() prepends passed elements to the front of the array
            $monday[] =  array('id'=>$value['id'], 'title' =>$explode[0] . "-". $explode[1], 'day' => $explode[0], 'order' => 0);
            $sorted['Monday'] = $monday;
        } 
        else if ($explode[0] == 'Tuesday') {
            $tuesday[] =  array('id'=>$value['id'], 'title' =>$explode[0] . "-". $explode[1], 'day' => $explode[0], 'order' => 1);
            $sorted['Tuesday'] = $tuesday;
        }
        else if ($explode[0] == 'Wednesday') {
            $wednesday[] =  array('id'=>$value['id'], 'title' =>$explode[0] . "-". $explode[1], 'day' => $explode[0], 'order' => 2);
            $sorted['Wednesday'] = $wednesday;
        }
        else if ($explode[0] == 'Thursday') {
            $thursday[] =  array('id'=>$value['id'], 'title' =>$explode[0] . "-". $explode[1], 'day' => $explode[0], 'order' => 3);
            $sorted['Thursday'] = $thursday;
        }
        else if ($explode[0] == 'Friday') {
            $friday[] =  array('id'=>$value['id'], 'title' =>$explode[0] . "-". $explode[1], 'day' => $explode[0], 'order' => 4);
            $sorted['Friday'] = $friday;
        }
        else if ($explode[0] == 'Saturday') {
            $saturday[] =  array('id'=>$value['id'], 'title' =>$explode[0] . "-". $explode[1], 'day' => $explode[0], 'order' => 5);
            $sorted['Saturday'] = $saturday;
        }
        else if ($explode[0] == 'Sunday') {
            $sunday[] =  array('id'=>$value['id'], 'title' =>$explode[0] . "-". $explode[1], 'day' => $explode[0], 'order' => 6);
            $sorted['Sunday'] = $sunday;
        }
    }
    //$sorted = array('monday'=>$monday,'tuesday'=>$tuesday,'wednesday'=>$wednesday,'thursday'=>$thursday,'friday'=>$friday,'saturday'=>$saturday,'sunday'=>$sunday);
    //PC::pastorSorted($sorted);
    return $sorted;
}

/*
 * Gets the week for the given date
 * NOTE: If the date is not a full week then it will give you the wrong week
 * For example 01-01-2016 will return week 53
 */
function GetStartWeek($ddate) {
    //$ddate = "2012-10-18";
    $date = new DateTime($ddate);
    $week = $date->format("W");
    $year = $date->format("Y");
    //PC::startWeek(array('week => ' . $week, 'year => ' . $year));
    $result['week'] = $week;
    $result['year'] = $year;
    return $result;
}

function GetFirstWeekForLeader($cid) {
    $query = 'SELECT
        *
      FROM clearrivernamele.custom_smallgroup_meeting_log
        WHERE group_leader_cid = ' . $cid .
        ' order BY meeting_date'
         . ' LIMIT 1;';
    $dao = CRM_Core_DAO::executeQuery($query);

    $results = array();
    while ($dao->fetch()) {
        $results[] = $dao->toArray();
    }
    //PC::firstLog($results);
    $startWeek = GetStartWeek($results[0]['meeting_date']);
    //PC::GetFirstWeekForLeader($startWeek);
    return $startWeek; // returns week and year
    //return $results;
}

function GetWeekStartDate($week, $year) {
    $dto = new DateTime();
    $dto->setISODate($year, $week);
    $ret['week_start'] = $dto->format('d-M');
    //$dto->modify('+6 days');
    //$ret['week_end'] = $dto->format('Y-m-d');
    return $ret['week_start'];
}
