<?php

//require_once('CustomPHP/ChromePhp.php');
/**
 * Delete Small Group API
 * @param type $params
 * @return type
 */
function civicrm_api3_small_group_tracking_deletesmallgroup($params) {
    //ChromePhp::log('civicrm_api3_small_group_tracking_deletesmallgroup');
    $sgid = $params['sgid']; // group that we want to delete
    $smallGroupLeaderID = $params['cid'];
    $firstname = $params['firstname'];
    $lastname = $params['lastname'];
    
    $leadsMultiple = false; // leads multiple small group so don't remove tag or data
    
    try {
        /*
         * Get Group Leaders CID
         */
        if(!$smallGroupLeaderID) {
            $smallGroupLeaderID = GetLeaderId($sgid);
        }

        /*
        * Check if they lead multiple small groups
        */
        if($smallGroupLeaderID) {
            $leadsMultiple = CheckIfLeadsMultiple($smallGroupLeaderID,$sgid); 
        }
        else {
            return civicrm_api3_create_error('Small Group Leader CID was not set');
        }
    
        /**
         * GET CONTACTS IN THIS GROUP
         */
        $result = civicrm_api3('GroupContact', 'get', array(
            'sequential' => 1,
            'return' => "contact_id",
            'group_id' => $sgid,
        ));

        /**
         *  Loop through contacts and update custom values
         * after getting the custom tables they are stored in
         * 
         * SKIP THE SMALL GROUP LEADER IF THEY LEAD MULTIPLE GROUPS
         */
        $roleField = 'custom_' . GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'small_group_member_role_custom_field');
        $leaderField = 'custom_' . GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'small_group_leader_custom_field');

        foreach ($result['values'] as $val) {
            /*
             * If the leader has multiple groups then make sure that the
             * custom data is not cleared for that contact
             */
            if($leadsMultiple && $val['contact_id'] == $smallGroupLeaderID) {
                // skipping the leader
                //ChromePhp::log("Skipping Leader because leads multiple");
            }
            else { 
                $result = civicrm_api3('CustomValue', 'create', array(
                    'sequential' => 1,
                    'entity_id' => $val['contact_id'],
                    $roleField => NULL, // role
                    $leaderField => NULL, // leader
                ));
            }
        }

        /**
         * Remove "Small Group Leader" Tag from the leader
         */
        if(!$leadsMultiple) {
            //ChromePhp::log('Remove Small Group Leader Tag');
            $tagId = GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'small_group_leader_tag');
            civicrm_api3('EntityTag', 'delete', array(
                'sequential' => 1,
                'tag_id' => $tagId, // small group leader tag ID
                'entity_id' => $smallGroupLeaderID,
            ));
        }
        
        /**
         *  DELETE the group
         */
        $result = civicrm_api3('Group', 'delete', array(
            'debug' => 0,
            'sequential' => 1,
            'id' => $sgid,
        ));
        
        /**
         * Modify custom tables to mark entries as deleted
         */
        ArchiveTableRows($sgid);
        
    } catch (Exception $result) {
        $error = $result->getMessage();
        return civicrm_api3_create_success($error); // if not set to success the page will not display
    }
    return civicrm_api3_create_success($result);
}


function ArchiveTableRows($group_id) {
    // fb('update records in custom tables to is_deleted = 1');
    $query = "UPDATE `custom_smallgroup_meeting_log` "
            . "SET "
            . "`is_deleted` = 1 "
            . "WHERE `group_group_id` = " . $group_id . "; ";
    
    CRM_Core_DAO::executeQuery($query);
    
    $query = "UPDATE `custom_smallgroup_attendance` "
            . "SET "
            . "`is_deleted` = 1 "
            . "WHERE `sg_group_id` = " . $group_id . "; ";

    CRM_Core_DAO::executeQuery($query);
}

/*
 * GET THE GROUP LEADERS CID
 * Since we are passed the group id we need to loop through all the contacts
 * in the group until we find the contact that has the small group
 * leader tag.
 * 
 * Return the id of the contact
 */
function GetLeaderId($sgid) {
    //ChromePhp::log('GetLeaderId('.$sgid.")");
    $foundLeader = false;
    $currentId = null; //holds id as we loop
     /*
     * Get the leader of the group 
     */

    // Get the small group leader tag ID
    $tagID = GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'small_group_leader_tag');

    // GET ALL MEMBERS OF GROUP AND LOOP THROUGH FOR LEADER TAG
    $result = civicrm_api3('GroupContact', 'get', array(
        'sequential' => 1,
        'group_id' => $sgid,
    ));
    //ChromePhp::log('Group Members: ');
    //ChromePhp::log($result);
    // Loop through each contact in this group
    foreach ($result['values'] as $value) {
        $currentId = $value['contact_id'];
        // Get tags associated with this contact
        $tags = civicrm_api3('EntityTag', 'get', array(
           'sequential' => 1,
           'entity_id' => $value['contact_id'],
        ));
        //ChromePhp::log($tags);
        
        // loop through the tags for the contact and find leader
        foreach ($tags['values'] as $tag) {
           if($tag['tag_id'] == $tagID) {
               //ChromePhp::log('Found Leader Id: ' . $currentId);
               // we found the leader
               $foundLeader = true;
               return $currentId;
           }
        }
    }
    return null; 
}

/*
 * CHECK IF LEADER LEADS MULTIPLE GROUPS
 * 
 * Get the dc groups and then all of the groups within. From there iterate
 * through the groups checking to see if the small group leader contact
 * is a part of another group.
 * 
 * Return true or false 
 */
function CheckIfLeadsMultiple($cid, $sgid) {
    //ChromePhp::log('CheckIfLeadsMultiple('. $cid . ',' . $sgid . ')');
    $groupList = GetSmallGroupList(); // retrieves all small groups
    //ChromePhp::log($groupList);
    
    /*
     * FIND LEADER IN GROUP CONTACTS
     * 
     * Search through each group and see if this leader's cid matches any of
     * the other contacts in the groups. If it is found then count it.
     */
    foreach ($groupList as $group) {
        if($group != $sgid) {
            ////ChromePhp::log('Comparing Group: '. $group);
            /**
             * GET CONTACTS IN THIS GROUP
             */
            $result = civicrm_api3('GroupContact', 'get', array(
                'sequential' => 1,
                'return' => "contact_id",
                'group_id' => $group,
            ));
            /*
             * Loop through contacts in group and check for match
             * If we find one then we know that this leader has multiple groups
             * and can immediately return
             */
            foreach ($result['values'] as $value) {
               if($value['contact_id'] == $cid) {
                   //ChromePhp::log('Found leader in another group. Return True.');
                   return true;
               }
            }   
        }
        else {
            //ChromePhp::log('Current group matches sgid, skip search for group');
        }
    }
    return false;
}

