<?php

/* 
 * This will return a list of all small group groups
 */


/**
 * civicrm_api3_member_tracking_gettithes
 * @param type $params
 * @return type
 * Gets the DC groups
 * returns title, children, and parent groups
 */
function civicrm_api3_small_group_tracking_getsmallgroupgroups($params) {
    $dcID =  $params['dc_id'];
    $dcTitle = $params['dc_title']; // passed title for the DC
  
    $sgLeader = $params['sg_leader']; // passed smallgroup leaders name
  
    $startDate = $params['start_date']; // passed smallgroup leaders name
  
    $endDate = $params['end_date']; // passed smallgroup leaders name
  
    $query = "SELECT * " .
    "FROM custom_smallgroup_track_attendance " .
    "WHERE activity_date BETWEEN " . $startDate . " AND " . $endDate;
  

    $dao = CRM_Core_DAO::executeQuery($query);
    $results = array();
    while ($dao->fetch()) {
		$results[] = $dao->toArray();
    }
    
 

    return civicrm_api3_create_success($results);
}