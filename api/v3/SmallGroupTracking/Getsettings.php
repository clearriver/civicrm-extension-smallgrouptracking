<?php
/**
 * API to get all settings for a given group_name from civicrm_settings table
 * because built-in API can't do that, or I don't know how
 */

// require_once('CustomPHP/FirePHPCore/fb.php');

/**
 * Get all settings from civicrm_settings table for a given group name
 * @param type $params
 * group_name or name
 * @return type
 * If group_name and name are provided the values returned is an int
 * 
 * Otherwise it is the name as key to id
 */
function civicrm_api3_small_group_tracking_getsettings($params) {
    $group_name = $params['group_name'];
    $name = $params['name'];
    if($name && $group_name)
        $settings = CRM_Core_BAO_Setting::getItem($group_name, $name);    
    else 
        $settings = CRM_Core_BAO_Setting::getItem($group_name);    
    return civicrm_api3_create_success($settings);
}
