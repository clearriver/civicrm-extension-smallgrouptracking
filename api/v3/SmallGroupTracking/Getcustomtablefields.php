<?php
/**
 * civicrm_api3_small_group_tracking_getcustomtablefields
 * @param type $params
 * 
 * @return type
 * Get custom table fields
 * 
 * returns Array containing all custom fields given a custom group
 */
function civicrm_api3_small_group_tracking_getcustomtablefields($params) {
    
    
    $customTable = $params['custom_table'];
    
    $query = "SELECT label, column_name "
            ."FROM civicrm_custom_field "
            ."INNER JOIN civicrm_custom_group "
            ."ON custom_group_id = civicrm_custom_group.id "
            ."WHERE table_name = '".$customTable."'";

    $dao = CRM_Core_DAO::executeQuery($query);

    $results = array();
    while ($dao->fetch()) {
        $results[] = $dao->toArray();
    }
    
    return civicrm_api3_create_success($results);
}
