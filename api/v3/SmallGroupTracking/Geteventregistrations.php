<?php

/*
 * Receives a list of contact ids and upcoming event ids. 
 * Then checks each contact id to see what events the contact is registered for.
 * 
 * Returns a list with contacts and any events they are registered for.
 */
//PhpConsole\Helper::register();


/**
 * civicrm_api3_small_group_tracking_getsmallgroupattendance
 * @param array $params
 * event_id as array
 * contact_id as array
 * @return type
 */
function civicrm_api3_small_group_tracking_Geteventregistrations($params) {
    $event_id = $params['event_id']; // array holding all the event IDs
    $contact_id = $params['contact_id']; //array of contact ids
    
    $eventList = array();
    
    /**
     * GET EVENTS EACH CONTACT IS REGISTERED FOR
     * 
     */
    foreach ($contact_id as $cid) {
        $result = civicrm_api3('Participant', 'get', array(
            'sequential' => 1,
            'return' => array("event_id"),
            'contact_id' => $cid,
            'event_id' => $event_id,
        ));
        //PC::events_result($result);
        /**
         * Loop through and get event_id and event_title
         */
        $count = count($result['values']);
        for($i = 0; $i < $count; $i++) {
        //foreach($result['values'] as $event) {
        //    $eventList[$cid] .= $event['event_title'] . " ";
            $eventList[$cid] .= $result['values'][$i]['event_title'];
            if ($i < $count - 1) {
                $eventList[$cid] .= ", ";
            }
        }
        //PC::eventList($eventList);
    }
    return civicrm_api3_create_success($eventList);
}
/*
 * Gets the contact name, obviously
 */
/*
function GetContactName($cid, $lastnameFirst) {
    $name = civicrm_api3('Contact', 'get', array(
        'sequential' => 1,
        'id' => $cid,
        'return' => array('display_name','last_name','first_name')
    ));
    
    //ChromePhp::log($name);
    if($lastnameFirst) {
        return ($name['values'][0]['last_name'] . ", " . $name['values'][0]['first_name']);  
    } 
    else {
        return $name['values'][0]['display_name'];
    }
}
*/


