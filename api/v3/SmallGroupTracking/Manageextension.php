<?php

/**
 * API to manage install/uninstall/enable/disable of the extension
 * 
 * This script can accept the following actions:
 * install/uninstall/enable/disable
 * 
 * Functions in this API are called from the primary extension PHP file
 * 
 */
// require_once('CustomPHP/FirePHPCore/fb.php');

function _SmallgrouptrackingGetConfig() {
    /**
     * CONFIG FOR MANAGEMENT
     */
    $sgtManageParams = array(
        'group_title' => 'Discipleship Community',
        'custom_group_name' => 'small_group',
        'custom_group_title' => 'Small Group',
        'field_name1' => 'Small Group Leader',
        'field_name2' => 'Small Group Member Role',
        'group_name' => 'org.namelessnetwork.smallgrouptracking',
    );
    return $sgtManageParams;
}

function civicrm_api3_small_group_tracking_manageextension($params) {

    /**
     * GET CONFIG FOR MANAGEMENT
     */
    $sgtManageParams = _SmallgrouptrackingGetConfig();

    //$action = 'create'; // create, delete, disable
    //$action = 'delete'; // create, delete, disable
    //$action = 'disable'; // create, delete, disable

    $action = $params['action'];
  
    /*
    $groupTitle = 'Discipleship Community'; // root group structure
    $customGroupName = 'small_group';
    $customGroupTitle = 'Small Group';
    $fieldName1 = 'Small Group Leader';
    $fieldName2 = 'Small Group Member Role';
    $group_name = 'org.namelessnetwork.smallgrouptracking'; // civicrm_settings group name
    */
    
    /**
     * UNINSTALL
     */
    
    if ($action == 'uninstall') {
        _SmallgrouptrackingUninstall();
    } 
    /**
     * INSTALL
     */
    
    else if ($action == "install") {
        _SmallgrouptrackingInstall();
    } 
    /**
     * DISABLE
     */
    
    else if ($action == 'disable') {
        _SmallgrouptrackingDisable();
    } 
    /**
     * ENABLE
     */
    
    else if ($action == 'enable') {
        _SmallgrouptrackingEnable();
    } 
    /**
     * GET
     */
    
    else if ($action = 'get') {
        $settings = GetCiviSettingAll($sgtManageParams['group_name']);
        // fb($settings, 'settings');
        
        // fb(CRM_Member_BAO_Membership::getAllContactMembership(2));
    }

    return civicrm_api3_create_success($result);
}

/**
 * INSTALL
 */
function _SmallgrouptrackingInstall() {
    // fb('Creating...');
    /**
     * GET CONFIG FOR MANAGEMENT
     */
    $sgtManageParams = _SmallgrouptrackingGetConfig();

    /**
     * Create Tags
     */
    CreateTags($sgtManageParams['group_name']);

    /**
     * Create Activity
     */
    CreateActivity($sgtManageParams['group_name']);

    /**
     * Create Custom Tables
     */
    CreateCustomTables();

    /**
     * Create the group (DC)
     */
    CreateGroup($sgtManageParams['group_title']);

    /**
     * Create custom group on individual
     */
    $group_id = custom_group_get_id($sgtManageParams['custom_group_name']);

    if (!$group_id) {
        // fb('create custom group');
        $result[] = CreateCustomGroup($sgtManageParams['custom_group_name'], $sgtManageParams['custom_group_title'], null);
        $group_id = custom_group_get_id($sgtManageParams['custom_group_name']);
    } else {
        // fb('custom group exists');
        CreateCustomGroup($sgtManageParams['custom_group_name'], $sgtManageParams['custom_group_title'], $group_id);
    }


    /**
     *  Create field (small group leader)
     */
    // fb('Create field (small group leader)');
    $field = custom_field_get_id($group_id, $sgtManageParams['field_name1']);
    if (!$field && $group_id) {
        // fb('created autocomplete field');
        $params = array(
            'sequential' => 1,
            'custom_group_id' => $group_id,
            'label' => $sgtManageParams['field_name1'],
            'data_type' => "ContactReference",
            'html_type' => "Autocomplete-Select",
            'is_searchable' => 1,
        );
        $result[] = CreateCustomField($params, $sgtManageParams['field_name1'], null);
    } else if ($field && $group_id) {
        // fb('field exists...');
        $params = array();
        CreateCustomField($params, $sgtManageParams['field_name1'], $field);
    }

    /**
     * Create field (role)
     */
    // fb('Create field (role)');
    $field = custom_field_get_id($group_id, $sgtManageParams['field_name2']);
    if (!$field && $group_id) {
        // fb('Created field (role)');
        $params = array(
            'debug' => 1,
            'sequential' => 1,
            'custom_group_id' => $group_id,
            'label' => $sgtManageParams['field_name2'],
            'data_type' => "String",
            'html_type' => "Select",
            'is_searchable' => 1,
            'option_label' => array("Regular", "Irregular", "Visitor", "Inactive"),
            'option_value' => array("Regular", "Irregular", "Visitor", "Inactive"),
            'option_weight' => array(1, 2, 3, 4),
            'option_status' => array(1, 1, 1, 1),
        );
        $result[] = CreateCustomField($params, $sgtManageParams['field_name2'], null);
    } else if ($field && $group_id) {
        // fb('field exists...');
        $params = array();
        CreateCustomField($params, $sgtManageParams['field_name2'], $field);
    }
}

/**
 * UNINSTALL
 */
function _SmallgrouptrackingUninstall() {
    /**
     * GET CONFIG FOR MANAGEMENT
     */
    $sgtManageParams = _SmallgrouptrackingGetConfig();
    
    // fb('Delete...');
    /**
     * Delete Tags
     */
    DeleteTags($sgtManageParams['group_name']);

    /**
     * Delete Custom Tables
     */
    DeleteCustomTables();

    $group_id = custom_group_get_id($sgtManageParams['custom_group_name']);
    /**
     *  Delete custom fields
     */
    $field = custom_field_get_id($group_id, $sgtManageParams['field_name1']);
    if ($field) {
        // fb('Delete Field1: ', $field);
        DeleteCustomField($field);
    } else
        // fb($field, 'field not found with id');

    $field = custom_field_get_id($group_id, $sgtManageParams['field_name2']);
    if ($field) {
        // fb('Delete Field2: ', $field);
        DeleteCustomField($field);
    } else
        // fb($field, 'field not found with id');

    /**
     * Delete the custom group
     */
    if ($group_id) {
        // fb('delete custom group');
        DeleteCustomGroup($group_id);
    } else
        // fb($group_id, 'custom group not found with id');

    /**
     * Delete the group (DC)
     */
    DeleteGroup($sgtManageParams['group_title']);

    /**
     * Delete Activity
     */
    DeleteActivity($sgtManageParams['group_name']);

    /**
     * Delete Civi Settings
     */
    $setting_group_name = $sgtManageParams['group_name'];
    DeleteCiviSetting($setting_group_name, null, null);
}

/**
 * ENABLE
 */
function _SmallgrouptrackingEnable() {
    // fb('Enabling...');

    /**
     * GET CONFIG FOR MANAGEMENT
     */
    $sgtManageParams = _SmallgrouptrackingGetConfig();
    
    /**
     * Enable Custom Group & Fields
     */
    $group_id = custom_group_get_id($sgtManageParams['custom_group_name']);
    if ($group_id) {
        EnableCustomGroup($group_id);
    }
    /**
     * Enable Activity
     */
    EnableActivity($sgtManageParams['group_name']);

    /**
     * Enable Group (DC)
     */
    EnableGroup($sgtManageParams['custom_group_title']);
}

/**
 * DISABLE
 */
function _SmallgrouptrackingDisable() {
    // fb('Disabling...');
    /**
     * GET CONFIG FOR MANAGEMENT
     */
    $sgtManageParams = _SmallgrouptrackingGetConfig();
    
    /**
     * Disable Custom Group & Fields
     */
    $group_id = custom_group_get_id($sgtManageParams['custom_group_name']);
    if ($group_id) {
        // fb('disable custom group');
        DisableCustomGroup($group_id);
    }
    /**
     * Disable Activity
     */
    DisableActivity($sgtManageParams['group_name']);

    /**
     * Disable Group (DC)
     */
    DisableGroup($sgtManageParams['custom_group_title']);
}

function DeleteCustomViews() {
    // fb('dropping views...');
    /**
     * don't do this yet because other extensions may rely on them
     */
    /*
    $query = "DROP TABLE IF EXISTS `civicrm_contact_groups_view`;";
    CRM_Core_DAO::executeQuery($query);

    $query = "DROP TABLE IF EXISTS `civicrm_contact_id_view`;";
    CRM_Core_DAO::executeQuery($query);

    $query = "DROP TABLE IF EXISTS `civicrm_contact_member_merged_view`;";
    CRM_Core_DAO::executeQuery($query);

    $query = "DROP TABLE IF EXISTS `civicrm_contact_member_tithes_view`;";
    CRM_Core_DAO::executeQuery($query);

    $query = "DROP TABLE IF EXISTS `civicrm_contact_member_view`;";
    CRM_Core_DAO::executeQuery($query);

    $query = "DROP TABLE IF EXISTS `civicrm_contact_serving_view`;";
    CRM_Core_DAO::executeQuery($query);

    $query = "DROP TABLE IF EXISTS `civicrm_contact_sgl_view`;";
    CRM_Core_DAO::executeQuery($query);

    $query = "DROP TABLE IF EXISTS `civicrm_contact_sg_view`;";
    CRM_Core_DAO::executeQuery($query);

    $query = "DROP TABLE IF EXISTS `civicrm_contact_view`;";
    CRM_Core_DAO::executeQuery($query);

    $query = "DROP TABLE IF EXISTS `smallgroup_leaders_view`;";
    CRM_Core_DAO::executeQuery($query);
     */
}

function CreateCustomTables() {
    /*
      --
      -- Table structure for table `custom_smallgroup_attendance`
      --
     */
    $query = "DROP TABLE IF EXISTS `custom_smallgroup_attendance`;";
    CRM_Core_DAO::executeQuery($query);

    $query = "CREATE TABLE IF NOT EXISTS `custom_smallgroup_attendance` ( " .
            "`id` int(11) NOT NULL AUTO_INCREMENT, " .
            "`member_id` int(11) DEFAULT NULL, " .
            "`activity_date` date DEFAULT NULL, " .
            "`sg_group_id` int(11) DEFAULT NULL, " .
            "`dc_leader_id` int(11) DEFAULT NULL, " .
            "`group_leader_id` int(11) DEFAULT NULL, " .
            "`activity_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL, " .
            "`member_role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL, " .
            "`attended` int(1) DEFAULT '0', " .
            "`notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL, " .
            "`is_deleted` bit(1) DEFAULT b'0', " .
            "PRIMARY KEY (`id`), " .
            "UNIQUE KEY `member_id` (`member_id`,`activity_date`) " .
            ") ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Manage' AUTO_INCREMENT=0; ";

    // fb($query, 'Create custom_smallgroup_attendance table');
    CRM_Core_DAO::executeQuery($query);

    /*
      --
      -- Table structure for table `custom_smallgroup_meeting_log`
      --
     */
    $query = "DROP TABLE IF EXISTS `custom_smallgroup_meeting_log`;";
    CRM_Core_DAO::executeQuery($query);

    $query = "CREATE TABLE IF NOT EXISTS `custom_smallgroup_meeting_log` ( " .
            "`id` int(11) NOT NULL AUTO_INCREMENT, " .
            "`meeting_date` date DEFAULT NULL, " .
            "`group_leader_cid` int(11) DEFAULT NULL COMMENT 'contact id for the actual small group leader', " .
            "`meeting_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL, " .
            "`group_group_id` int(11) DEFAULT NULL COMMENT 'Small group group ID', " .
            "`present_count` int(2) DEFAULT '0', " .
            "`dc_group_id` int(11) DEFAULT '0' COMMENT 'the group id for the selected DC', " .
            "`is_deleted` bit(1) DEFAULT b'0', " .
            "PRIMARY KEY (`id`) " .
            ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='keeps log of all meetings for small groups. Primarily used a'; ";

    // fb($query, 'Create custom_smallgroup_meeting_log table');
    CRM_Core_DAO::executeQuery($query);
}

function DeleteCustomTables() {
    // fb('drop custom_smallgroup_attendance table');
    $query = "DROP TABLE IF EXISTS `custom_smallgroup_attendance`;";
    CRM_Core_DAO::executeQuery($query);

    // fb('drop custom_smallgroup_meeting_log table');
    $query = "DROP TABLE IF EXISTS `custom_smallgroup_meeting_log`;";
    CRM_Core_DAO::executeQuery($query);
}

function CreateGroup($groupTitle) {
    // fb('create group');
    $group_id = group_get_id($groupTitle);
    $settingName = strtolower(str_replace(" ", "_", $groupTitle));
    if ($group_id) {
        CRM_Contact_BAO_Group::setIsActive($group_id, 1);
        CreateCiviSetting('org.namelessnetwork.smallgrouptracking', $settingName . "_group", $group_id);
        // fb('group already exists');
    } else {
        $result = civicrm_api3('Group', 'create', array(
            'debug' => 1,
            'sequential' => 1,
            'title' => $groupTitle,
            'is_reserved' => 1,
            'is_active' => 1,
            'description' => "(!!!DO NOT EDIT!!!) Root group for small group tracking extension. Do NOT delete.",
        ));

        // Add Setting
        CreateCiviSetting('org.namelessnetwork.smallgrouptracking', $settingName . "_group", $result['id']);

        // fb('group created');
    }
}

function DeleteGroup($groupTitle) {

    $dcSmallGroups = array();
    $group_id = group_get_id($groupTitle); // get root group ID
    $dcChildren = array();
    
    if ($group_id) {
        // fb($group_id, 'Root Group ID: ');
        /**
         * Get DC pastor groups
         */
        $result = civicrm_api3('Group', 'get', array(
            'debug' => 1,
            'sequential' => 1,
            'return' => "children",
            'id' => $group_id,
        ));

        if ($result['values'][0]['children']) {// only do if we actually have children
            $dcChildren = explode(",", $result['values'][0]['children']); // put children to array

            // fb($dcChildren, "DC Pastor Groups: ");
            /**
             * Get DC Small Groups
             */
            foreach ($dcChildren as $id) {
                $result = civicrm_api3('Group', 'get', array(
                    'debug' => 1,
                    'sequential' => 1,
                    'return' => "children",
                    'id' => $id,
                ));
                if ($result['values'][0]['children']) {
                    $dcSmallGroups = array_merge($dcSmallGroups, explode(",", $result['values'][0]['children'])); // puts all children into single array
                }
            }

            if (count($dcSmallGroups) > 0) {
                // fb('dcSmallGroups found');
                $groupsToDelete = array_merge($dcChildren, $dcSmallGroups); // merge DC with child groups
            } else {
                // fb('no dcSmallGroups');
                $groupsToDelete = $dcChildren;
            }
        }
        $groupsToDelete[] = $group_id; // append root group to array

        // fb($groupsToDelete, "DC Small Groups: ");
        foreach ($groupsToDelete as $id) {
            CRM_Contact_BAO_Group::discard($id);
        }
        // fb('group deleted');
    } else {
        // fb('group not found');
    }
}

function DisableGroup($groupTitle) {
    // fb('disableGroup', $groupTitle);
    $group_id = group_get_id($groupTitle);
    if ($group_id) {
        CRM_Contact_BAO_Group::setIsActive($group_id, 0);
        // fb('group disabled');
    } else {
        // fb('group not found');
    }
}

function EnableGroup($groupTitle) {
    // fb('enableGroup', $groupTitle);
    $group_id = group_get_id($groupTitle);
    if ($group_id) {
        CRM_Contact_BAO_Group::setIsActive($group_id, 1);
        // fb('group enabled');
    } else {
        // fb('group not found');
    }
}

function DisableCustomGroup($group_id) {
    CRM_Core_BAO_CustomGroup::setIsActive($group_id, 0);
}

function EnableCustomGroup($group_id) {
    // fb('enable custom group id', $group_id);
    CRM_Core_BAO_CustomGroup::setIsActive($group_id, 1);
}

function CreateCustomGroup($name, $title, $group_id) {
    try {
        if (!$group_id) {
            $result = civicrm_api3('CustomGroup', 'create', array(
                'sequential' => 1,
                'title' => $title,
                'name' => $name,
                'extends' => array(
                    '0' => 'Individual',
                ),
                'weight' => 10,
                'collapse_display' => 1,
                'style' => 'Inline',
                //'help_pre' => 'This is Pre Help For Test Group 1',
                //'help_post' => 'This is Post Help For Test Group 1',
                'is_reserved' => 1,
                'is_active' => 1,
            ));
            // fb('created custom group: ', $result['values']);
            CreateCiviSetting('org.namelessnetwork.smallgrouptracking', 'small_group_custom_group', $result['id']);
            CreateCiviSetting('org.namelessnetwork.smallgrouptracking', 'custom_group_table', 'civicrm_value_' . $name . '_' . $result['id']); // setting with the name of the custom table
            return $result;
        } else {
            /**
             * Settings Created
             * 
             * group_name: org.namelessnetwork.smallgrouptracking
             * name: small_group_custom_group
             * value: id
             */
            CreateCiviSetting('org.namelessnetwork.smallgrouptracking', 'small_group_custom_group', $group_id);
            CreateCiviSetting('org.namelessnetwork.smallgrouptracking', 'custom_group_table', 'civicrm_value_' . $name . '_' . $group_id); // setting with the name of the custom table
            return true;
        }
    } catch (CiviCRM_API3_Exception $e) {
        // handle error here
        $errorMessage = $e->getMessage();
        $errorCode = $e->getErrorCode();
        $errorData = $e->getExtraParams();
        return array('error' => $errorMessage, 'error_code' => $errorCode, 'error_data' => $errorData);
    }
}

function DeleteCustomGroup($group_id) {
    try {
        // fb('delete custom group with id', $group_id);
        $result = civicrm_api3('CustomGroup', 'delete', array(
            'sequential' => 1,
            'id' => $group_id,
        ));
        // fb('delete custom group: ', $result['values']);
    } catch (CiviCRM_API3_Exception $e) {
        // handle error here
        $errorMessage = $e->getMessage();
        $errorCode = $e->getErrorCode();
        $errorData = $e->getExtraParams();
        // fb('create custom field error: ', $errorMessage, $errorCode, $errorData);
        return array('error' => $errorMessage, 'error_code' => $errorCode, 'error_data' => $errorData);
    }
}

function CreateCustomField($params, $name, $field) {
    try {
        $settingName = strtolower(str_replace(" ", "_", $name)); // replace spaces with underscore in name
        if (!$field) {
            $result = civicrm_api3('CustomField', 'create', $params);
            // fb('create custom field: ', $result);

            // Store Setting
            CreateCiviSetting('org.namelessnetwork.smallgrouptracking', $settingName . "_custom_field", $result['id']);
            return $result;
        } else {
            CreateCiviSetting('org.namelessnetwork.smallgrouptracking', $settingName . "_custom_field", $field);
            return true;
        }
    } catch (CiviCRM_API3_Exception $e) {
        // handle error here
        $errorMessage = $e->getMessage();
        $errorCode = $e->getErrorCode();
        $errorData = $e->getExtraParams();
        // fb('create custom field error: ', $errorMessage, $errorCode, $errorData);
        return array('error' => $errorMessage, 'error_code' => $errorCode, 'error_data' => $errorData);
    }
}

function DeleteCustomField($field) {
    try {
        $result = civicrm_api3('CustomField', 'delete', array(
            'sequential' => 1,
            'id' => $field,
        ));
        // fb('delete custom field: ', $result['values']);
    } catch (CiviCRM_API3_Exception $e) {
        // handle error here
        $errorMessage = $e->getMessage();
        $errorCode = $e->getErrorCode();
        $errorData = $e->getExtraParams();
        // fb('delete custom field error: ', $errorMessage, $errorCode, $errorData);
        return array('error' => $errorMessage, 'error_code' => $errorCode, 'error_data' => $errorData);
    }
}

function CreateTags($group_name) {
    // Check if it exists
    $result = civicrm_api3('Tag', 'get', array(
        'sequential' => 1,
        'name' => "Small Group Leader",
    ));

    // Create if not exist
    if (!$result['values']) {
        $result = civicrm_api3('Tag', 'create', array(
            'sequential' => 1,
            'name' => "Small Group Leader",
            'used_for' => 'civicrm_contact',
            'is_reserved' => 1,
        ));
    }

    /**
     * Add Settings
     */
    CreateCiviSetting($group_name, 'small_group_leader_tag', $result['id']);
}

function DeleteTags($group_name) {
    $tag_id = GetCiviSetting($group_name, 'small_group_leader_tag');

    if ($tag_id) {
        $result = civicrm_api3('Tag', 'delete', array(
            'sequential' => 1,
            'id' => $tag_id,
        ));
    }

    /**
     * Delete Setting
     */
    DeleteCiviSetting($group_name, 'small_group_leader_tag', null);
}

/**
 * Create the activity we will store small group attendance in
 * 
 * 
 * result['values']['value'] is the activity_type_id
 */
function CreateActivity($group_name) {
    $activity_id = GetCiviSetting($group_name, 'small_group_activity');
    
    /**
     * Create the activity when there is no existing setting
     */
    if(!$activity_id) {
        $result = civicrm_api3('OptionValue', 'create', array(
            'sequential' => 1,
            'label' => "Small Group Attendance",
            'option_group_id' => 'activity_type', // activity type
            'is_reserved' => 1,
            'description' => "This activity is used to log small group tracking attendance in addition to the custom table logs",
        ));
        CreateCiviSetting($group_name, 'small_group_activity', (int)$result['values'][0]['value']); // record ID for activity
    }
}

function DeleteActivity($group_name) {
    $activity_id = GetCiviSetting($group_name, 'small_group_activity');
    
    /**
     * Delete associated activities
     * 
     * Records are in the following tables:
     * - civicrm_activity_contact
     * - 
     */
    
    /** 
     * Get the OptionValue ID
     */
    if($activity_id) {
        $result = civicrm_api3('OptionValue', 'get', array(
            'sequential' => 1,
            'value' => $activity_id,
        ));
        $option_value_id = $result['id'];

        /**
         * Delete the Small Group Attendance Activity Type
         */
        // fb($option_value_id,'delete optionvalue with id');
        if($option_value_id) {
            $result = civicrm_api3('OptionValue', 'delete', array(
                'sequential' => 1,
                'id' => $option_value_id, // option value ID
            ));
        }
    }
}

function EnableActivity($group_name) {
    $activity_id = GetCiviSetting($group_name, 'small_group_activity');
    /** 
     * Get the OptionValue ID
     */
    $result = civicrm_api3('OptionValue', 'get', array(
        'sequential' => 1,
        'value' => $activity_id,
    ));
    $option_value_id = $result['id'];
    
    CRM_Core_BAO_OptionValue::setIsActive($option_value_id, true);
}

function DisableActivity($group_name) {
    $activity_id = GetCiviSetting($group_name, 'small_group_activity');
    /** 
     * Get the OptionValue ID
     */
    $result = civicrm_api3('OptionValue', 'get', array(
        'sequential' => 1,
        'value' => $activity_id,
    ));
    $option_value_id = $result['id'];
    
    CRM_Core_BAO_OptionValue::setIsActive($option_value_id, false);
}

function CreateRole() {
    //Civicrm Role
    // Drupal Role
    // Apply permissions to SGL
}

function DeleteRole() {
    
}

function DisableRole() {
    //Civicrm Role
    // Drupal Role
}

/** Permissions/Roles
 * 
 * Group: Small Group Leaders with access control
 * Small gorup leader contact is added to the group
 * 
 * ACL Role:
 * Small Group Leader
 * 
 * Assign users to ACL roles
 * ACL Small group leader assigned to Small Group Leader
 */

/**
 * API EXPLORER AUTOFILLS
 * 
 * The metadata is used for setting defaults, documentation &
 * validation.
 * 
 * @param type $params
 * Array of parameters determined by getfields.
 */
function _civicrm_api3_small_group_tracking_manageextension_spec(&$params) {
  $params['action']['name'] = 'action';
  $params['action']['description'] = 'ID for group that contacts are in';
  $params['action']['title'] = 'Action';
  // Fields which should default to the value of another parameter should be marked using 'api.aliases'
  //$params['group_id']['api.aliases'] = array('messageTemplateID', 'message_template_id');
}