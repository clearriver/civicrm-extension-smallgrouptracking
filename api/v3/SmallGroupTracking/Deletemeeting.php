<?php

/* 
 * Removes all meeting entries for a given date and small group leader
 * 
 * Removes entry from the meeting log table
 * removes activity for all contacts in the small group
 * removes entry in the table custom_smallgroup_attendance
 * 
 * Must give it date and small group leader
 */


/**
 * civicrm_api3_member_tracking_gettithes
 * @param type $params
 * @return type
 * Gets the DC groups
 * returns title, children, and parent groups
 */
function civicrm_api3_small_group_tracking_Deletemeeting($params) {
    $date = $params['meeting_date']; 
    $group_leader_cid = $params['group_leader_cid']; 

    RemoveCustomEntry($date, $group_leader_cid);
    RemoveMeetingLogEntry($date, $group_leader_cid);
    RemoveActivityEntry($date, $group_leader_cid);
    
    return civicrm_api3_create_success(true);
}

function RemoveMeetingLogEntry($meeting_date, $group_leader_cid) {
    $query = //"SET SQL_SAFE_UPDATES = 0; " .
            "DELETE FROM custom_smallgroup_meeting_log " .
            "WHERE meeting_date = '" . $meeting_date . "' " .
            "AND group_leader_cid = " . $group_leader_cid . "; " 
            //"SET SQL_SAFE_UPDATES = 1; "
            ;
    
    //ChromePhp::log("RemoveMeetingLogEntry: " . $query);
    CRM_Core_DAO::executeQuery($query);
    //print_r($dao);
}

function RemoveCustomEntry($activity_date, $group_leader_id) {
    $query = //"SET SQL_SAFE_UPDATES = 0; " .
            "DELETE FROM custom_smallgroup_attendance " .
            "WHERE activity_date = '" . $activity_date . "' " .
            "AND group_leader_id = " . $group_leader_id . "; " 
            //"SET SQL_SAFE_UPDATES = 1; "
            ;
    
    //ChromePhp::log("RemoveCustomEntry: " . $query);
    $dao = CRM_Core_DAO::executeQuery($query);
}

function RemoveActivityEntry($date, $group_leader_id) {
    // Small Group Attendance Activity ID = 58
    // Details must have specific key added at the end
    
    // This returns all activity ids we need to remove
    $result = civicrm_api3('Activity', 'get', array(
        'sequential' => 1,
        'activity_type_id' => 58,
        'activity_date_time' => $date,
        'details' => array('LIKE' => "%[sglid=". $group_leader_id ."]"),
    ));
    
    //ChromePhp::log("RemoveActivityEntry: " . $result);
    
    // delete each relevant activity
    foreach ($result['values'] as $val) {
        $result = civicrm_api3('Activity', 'delete', array(
            'sequential' => 1,
            'id' => $val['id'],
        ));
    }
    
}
