<?php

//// require_once('CustomPHP/FirePHPCore/fb.php');
/* 
 * Create a small group discipleship community
 * parameters
 * DC Pastor Name
 * Day
 * 
 * Group Name:
 * Day-DC (Lastname, First Name)
 */

function civicrm_api3_small_group_tracking_createsmallgroupdc($params) {
    $firstname = $params['firstname'];
    $lastname = $params['lastname'];
    $day = $params['day'];
    
    // Group Title
    $dcTitle = $day.'-DC ('.$lastname.', '.$firstname.')'; // 
     
    /**
     * Get settings for extension and set the ID for the root group
     */
    $settings = GetCiviSettingAll('org.namelessnetwork.smallgrouptracking');      
    $parentId = $settings['discipleship_community_group'];

    //ChromePhp::log($result[0]["id"]);
    
    /* Check for Existing Group
     * If a group already exists with that name an error message is returned:
     * "DB error: already exists"
     */
    
    // Create the DC Group
    
    
    $result = civicrm_api3('Group', 'create', array(
      'debug' => 0,
      'sequential' => 1,
      'title' => $dcTitle,
      'parents' => $parentId,
    ));
    
    //// fb($result, 'Createsmallgroupdc result');

    return civicrm_api3_create_success($result);
}