<?php

/* Gets dates for a given day within a specific date range
 * 
 */


// require_once('CustomPHP/FirePHPCore/fb.php');

function civicrm_api3_small_group_tracking_getdaydatesinrange($params) {
    
    //// fb('php');
    $start_date = $params['start_date']; 
    $end_date = $params['end_date'];
    $day = $params['day'];
    
    // DYNAMIC QUERY VARS
    $dateQuery = "";
    $addAnd = FALSE; // if we want to include "AND"
    $meetingQuery = "";
    $groupLeaderCidQuery = "";
    $groupGroupIdQuery = "";
    
    // VARIABLE MANAGEMENT
    if($start_date && !$end_date) { // start date
        $end_date = $start_date;
        $dateQuery = "a.Date BETWEEN " . $start_date . " AND " . $end_date . " ";
        //// fb(' if($start_date && !$end_date)');
    }
    else if(!$start_date && $end_date) { // end date
        $start_date = $end_date;
        $dateQuery = "a.Date BETWEEN " . $start_date . " AND " . $end_date . " ";
        //// fb('else if(!$start_date && $end_date)');
    }
    else if ( $start_date && $end_date){ // both dates
        $dateQuery = "a.Date BETWEEN " . $start_date . " AND " . $end_date . " ";
        //// fb('else if ( $start_date && $end_date)');
    }
    $addAnd = TRUE;
    if( !$start_date && !$end_date ){ // no dates
        $dateQuery = "";
        $addAnd = FALSE;
        //// fb('if( !$start_date && !$end_date )');
    }

    $query = "select a.Date " .
            "from ( " .
            "select curdate() - INTERVAL (a.a + (10 * b.a) + (100 * c.a)) DAY as Date " .
            "from (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as a " .
            "cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as b " .
            "cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as c " .
            ") a " .
            "WHERE " . $dateQuery . " " .
            "and DAYOFWEEK(a.Date) = " . $day . " " .
            "order by a.Date";

    //// fb($query, 'query');
    $dao = CRM_Core_DAO::executeQuery($query);
    
    $results = array();
    while ($dao->fetch()) {
        $results[] = $dao->toArray();
    }
    
    //// fb($results, "results");
    //ChromePhp::log($results);

    return civicrm_api3_create_success($results);
}
