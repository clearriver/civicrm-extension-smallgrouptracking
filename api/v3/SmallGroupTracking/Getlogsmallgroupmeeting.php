<?php

/* Gets small group meetings for referencing history
 * 
 */

/**
 * API EXPLORER AUTOFILLS
 * 
 * The metadata is used for setting defaults, documentation &
 * validation.
 * 
 * @param type $params
 * Array of parameters determined by getfields.
 */
function _civicrm_api3_small_group_tracking_getlogsmallgroupmeeting_spec(&$params) {
  $params['start_date']['name'] = 'start_date';
  $params['start_date']['description'] = '';
  $params['start_date']['title'] = 'start_date';
  // Fields which should default to the value of another parameter should be marked using 'api.aliases'
  //$params['group_id']['api.aliases'] = array('messageTemplateID', 'message_template_id');
  $params['end_date']['name'] = 'end_date';
  $params['end_date']['description'] = '';
  $params['end_date']['title'] = 'end_date';
  
  $params['meeting_type']['name'] = 'meeting_type';
  $params['meeting_type']['description'] = '';
  $params['meeting_type']['title'] = 'meeting_type';
  
  $params['sgl_id']['name'] = 'sgl_id';
  $params['sgl_id']['description'] = 'Small Group Leader ID';
  $params['sgl_id']['title'] = 'sgl_id';
  
  $params['sgg_id']['name'] = 'sgg_id';
  $params['sgg_id']['description'] = "Small Group's group ID";
  $params['sgg_id']['title'] = 'sgg_id';
  
}

function civicrm_api3_small_group_tracking_getlogsmallgroupmeeting($params) {
    //PhpConsole\Helper::register();
    //// fb('php');
    $start_date = $params['start_date']; 
    $end_date = $params['end_date'];
    $meeting_type = $params['meeting_type'];
    $group_leader_cid = $params['sgl_id'];
    $group_group_id = $params['sgg_id'];
    
    $order = $params['order']; // ascending, or descending
    
    // DYNAMIC QUERY VARS
    $dateQuery = "";
    $addAnd = FALSE; // if we want to include "AND"
    $meetingQuery = "";
    $groupLeaderCidQuery = "";
    $groupGroupIdQuery = "";
    
    //PC::debug('meeting_type', $meeting_type);
    // VARIABLE MANAGEMENT
    if($start_date && !$end_date) { // start date
        $end_date = $start_date;
        $dateQuery = "meeting_date BETWEEN " . $start_date . " AND " . $end_date . " ";
        //// fb(' if($start_date && !$end_date)');
    }
    else if(!$start_date && $end_date) { // end date
        $start_date = $end_date;
        $dateQuery = "meeting_date BETWEEN " . $start_date . " AND " . $end_date . " ";
        //// fb('else if(!$start_date && $end_date)');
    }
    else if ( $start_date && $end_date){ // both dates
        $dateQuery = "meeting_date BETWEEN " . $start_date . " AND " . $end_date . " ";
        //// fb('else if ( $start_date && $end_date)');
    }
    $addAnd = TRUE;
    if( !$start_date && !$end_date ){ // no dates
        $dateQuery = "";
        $addAnd = FALSE;
        //// fb('if( !$start_date && !$end_date )');
    }
    
   
        
    if($meeting_type) {
        if($addAnd) {
           $meetingQuery = "AND "; 
        }

        $meetingQuery .= " meeting_type IN (";
        for($i = 0; $i <= sizeof($meeting_type)-1; $i++) {
            if($i>0) {
                $meetingQuery .= ", ";
            }
            $meetingQuery .= "\"" . $meeting_type[$i] . "\" ";
        }
        $meetingQuery .= ") ";

        $addAnd = TRUE;
        //// fb('if($meeting_type)');
    }
    
    if($group_leader_cid) {
        if($addAnd) {
           $groupLeaderCidQuery = "AND "; 
        }
        
        $groupLeaderCidQuery .= "group_leader_cid = " . $group_leader_cid . " ";
        $addAnd = TRUE;
    }

    if ($group_group_id) {
        if ($addAnd) {
            $groupGroupIdQuery = "AND ";
        }

        $groupGroupIdQuery .= "group_group_id = " . $group_group_id . " ";
        $addAnd = TRUE;
    }
    
    // for sorting ascending or descending
    if($order) {
        $orderBy .= "ORDER by meeting_date " . $order . ";";
    }
    else { 

        $orderBy .= "ORDER by meeting_date;";
    }
    
    // Query for LOG Insert
    $query = "SELECT * " .
            "FROM custom_smallgroup_meeting_log " .
            "WHERE `is_deleted`=0 AND " .
            $dateQuery .
            $meetingQuery .
            $groupLeaderCidQuery .
            $groupGroupIdQuery .
            $orderBy;

    //PC::debug($query);
    //ChromePhp::log($query);
    //// fb($query, 'query');
    $dao = CRM_Core_DAO::executeQuery($query);
    
    $results = array();
    while ($dao->fetch()) {
        $results[] = $dao->toArray();
    }
    
    //ChromePhp::log($results);

    return civicrm_api3_create_success($results);
}
