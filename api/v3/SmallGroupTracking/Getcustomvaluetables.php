<?php

/*
 * Get the tables that are generated for custom values
 * This is a list of all tables matching civicrm_value
 */



/**
 * civicrm_api3_small_group_tracking_getsmallgroupattendance
 * @param array $params
 * @return type
 */
function civicrm_api3_small_group_tracking_Getcustomvaluetables($params) {
    $query = "SELECT table_name FROM INFORMATION_SCHEMA.TABLES where table_name LIKE 'civicrm_value%'";
    $dao = CRM_Core_DAO::executeQuery($query);
    
    $results = array();
    // Put list of all names into results
    while ($dao->fetch()) {
        $results[] = $dao->toArray();
    }
    
    // Loop through each table name appending the query
    
    
    // This will give us all the COLUMNS for each of the tables
    // Need to move it to it's own function or API call
    $queryResults = array();
    $results2 = array();
    if (sizeof($results) > 0) {
        foreach ($results[0] as $table) {
            $query = "DESCRIBE " . $table; // shows column names
            $dao = CRM_Core_DAO::executeQuery($query);
            
            while ($dao->fetch()) {
                $results2[] = $dao->toArray();
            }
            $queryResults[] = $results2;
        }
    }    
    
    

    return civicrm_api3_create_success($queryResults);
}