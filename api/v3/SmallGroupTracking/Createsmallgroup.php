<?php

/*
 * Create a small group 
 * 
 * Does the Following:
 * Creates the Small Group group 
 * DC as the parent
 * adds the Small Group Leader tag to the contact
 * Adds the contact to the group
 * 
 * Thoughts...
 * How do we handle name conflicts? 
 * 
 */

//require_once('CustomPHP/FirePHPCore/fb.php');

/**
 * civicrm_api3_small_group_tracking_createsmallgroup
 * @param type $params
 * firstname, lastname, day, dcFirstname, dcLastname 
 * @return type
 * Creates a small group
 * 
 * returns ?
 */
function civicrm_api3_small_group_tracking_createsmallgroup($params) {
    // fb($params,"Create Small Group API");
    $smallGroupLeaderID = $params['cid']; // contact id
    $dcGroupId = $params['dcGroupId']; // dc group id
    
    $firstname = ucfirst(strtolower($params['firstname']));
    //$firstname = 'Evan';
    $lastname = ucfirst(strtolower($params['lastname']));
    
    $middlename = ucfirst(strtolower($params['middlename']));
    //$lastname = 'Mills';
    $day = ucfirst(strtolower($params['day']));
    //$day = 'Friday';
    //$dcFirstname = strtolower($params['dcFirstname']);
    $dcFirstname = ucfirst(strtolower($params['dcFirstname']));
    //$dcFirstname = strtolower("Zach");
    //$dcLastname = strtolower($params['dcLastname']);
    $dcLastname = ucfirst(strtolower($params['dcLastname']));
    //$dcLastname = strtolower("Myers");
    $errors = array(); // store errors here
    // GET DC FOR GROUP
    
    $alreadyLeader = false;
    
    
    // Run if we don't have a dcGroupId
    if(!$dcGroupId) {
        $dcTitle = $day . '-DC (' . $dcLastname . ', ' . $dcFirstname . ')';
        // fb('DC Title: ' . $dcTitle);


        $query = "select id from civicrm_group where title = '" . $dcTitle . "'";
        $dao = CRM_Core_DAO::executeQuery($query);
        $dao->fetch();
        $result = array();
        $result[] = $dao->toArray();
        $dcGroupId = $result[0]["id"]; // used as parent ID for sg group
        // fb('DC Id: ' . $result[0]["id"]);
        if ($dcGroupId == "") {
            // fb('DC Group Not Found');
            return civicrm_api3_create_error('DC Not Found');
        }
    }
    
    // Run if small group leader contact ID is not given
    if(!$smallGroupLeaderID) {
        // fb('missing contact id for leader');
        // Get the Small Group Leader ID
        $result = civicrm_api3('Contact', 'get', array(
            'sequential' => 1,
            'first_name' => $firstname,
            'last_name' => $lastname,
        ));
        $smallGroupLeaderID = $result['values'][0]['id'];

        if ($smallGroupLeaderID == "") {
            //ChromePhp::log('DC Group Not Found');
            return civicrm_api3_create_error('Contact Not Found for Small Group Leader');
        }
    }
    
    // THIS SHOULD BE THE PATH WE GO
    else { // get the display name
        
        
        /*
         * Check if they are in a small group
         * IF they are not a small group leader then REMOVE them from the current
         * group
         */
        
        // Get the small group leader tag ID
        $tagID = GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'small_group_leader_tag');
        
        // Get tags associated with this contact
        $result = civicrm_api3('EntityTag', 'get', array(
            'sequential' => 1,
            'entity_id' => $smallGroupLeaderID,
        ));
        foreach ($result['values'] as $value) {
            if($value['tag_id'] == $tagID) {
                $alreadyLeader = true;
                break;
            }
        }
        
        // Remove from group they are currently in
        if(!$alreadyLeader) {
            RemoveFromCurrentGroup($smallGroupLeaderID);
        }
        
        // fb('get contact name');
        $result = civicrm_api3('Contact', 'get', array(
            'sequential' => 1,
            'return' => array("display_name", "first_name", "last_name", "middle_name"),
            'id' => $smallGroupLeaderID,
        ));
        $displayName = $result['values'][0]['display_name'];
        $lastname = $result['values'][0]['last_name'];
        $firstname = $result['values'][0]['first_name'];
        $middlename = $result['values'][0]['middle_name'];
    }

    // Add "Small Group Leader" Tag to the leader
    if(!$alreadyLeader) {
        $result = civicrm_api3('EntityTag', 'create', array(
            'sequential' => 1,
            'tag_id' => $tagID,
            'entity_id' => $smallGroupLeaderID,
        ));

        // UPDATE CUSTOM FIELDS
        // Small Group -> Role/Leader
        $custom1 = 'custom_' . GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'small_group_member_role_custom_field');
        $custom2 = 'custom_' . GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'small_group_leader_custom_field');

        $result = civicrm_api3('CustomValue', 'create', array(
            'sequential' => 1,
            'entity_id' => $smallGroupLeaderID,
            $custom1 => "Regular", // role
            $custom2 => $smallGroupLeaderID, // leader
        ));
    }
    // fb('added small group leader tag');

    

    // fb('updated custom fields');

    // Create the Group
    //$sgTitle = $firstname . " " . $lastname . " Small Group"; // Group Title
    $sgTitle = $displayName . " Small Group"; // Group Title
    $sgName = $sgTitle; //internal name for the small group
    // fb('Create small group Group');
    
    // CREATE GROUP and Add Contact to the Group
    // We need to catch this because if the group already exists it will kill it
    try {
        if($alreadyLeader) {
            $sgTitle = $sgTitle . '('. rand(0,99) .')'; // if we have multiple groups for this leader then we need to append a random number so we don't conflict with group
        }
        $result = civicrm_api3('Group', 'create', array(
            'debug' => 0,
            'sequential' => 1,
            'title' => $sgTitle,
            //'name' => $sgName,
            'parents' => $dcGroupId,
        ));
        $sgGroupID = $result['values'][0]['id'];
        
        // Add the leader to their own group
        civicrm_api3('GroupContact', 'create', array(
            'sequential' => 1,
            'contact_id' => $smallGroupLeaderID,
            'group_id' => $sgGroupID,
        ));
        // fb('small group group created');
    } catch (Exception $result) {
        
        // fb('Caught Exception: ', $result->getMessage(),"\n");
       
        $error = $result->getMessage();
        return civicrm_api3_create_success($error); // if not set to success the page will not display
    }
   
    return civicrm_api3_create_success($result);
}


/**
 * Removes the contact from a small group if they are currently part of one
 * 
 */
function RemoveFromCurrentGroup($cid) {
    //fb('RemoveFromGroup');
    $group_list = GetSmallGroupList();
    
    
    /*
     * Get groups this contact is in
     */
    $result = civicrm_api3('GroupContact', 'get', array(
        'sequential' => 1,
        'contact_id' => $cid,
    ));
    $contact_groups = $result['values'];

    /*
     * Compare the contact's groups to all small groups
     * If a match is found, remove the contact from the group
     */
    foreach ($contact_groups as $group) {
        if (in_array($group['group_id'], $group_list)) {
            //fb($group['group_id'],'group match found');

            /*
             * Remove from matched group
             */
            RemoveFromGroup($cid, $group['group_id']);
            break;
        }
    }
}

/*
 * REMOVE CONTACT FROM A GROUP
 */
function RemoveFromGroup($cid, $gid) {
    $result = civicrm_api3('GroupContact', 'delete', array(
        'sequential' => 1,
        'contact_id' => $cid,
        'group_id' => $gid, // group id
    ));
    //fb('Removed leader from group');
}

/**
 * API EXPLORER AUTOFILLS
 * 
 * The metadata is used for setting defaults, documentation &
 * validation.
 * 
 * @param type $params
 * Array of parameters determined by getfields.
 */
function _civicrm_api3_small_group_tracking_createsmallgroup_spec(&$params) {
  $params['cid']['name'] = 'Group Leader CID';
  $params['cid']['description'] = 'ID for contact that will be group leader';
  $params['cid']['title'] = 'Small Group Leader CID';

  $params['dcGroupId']['name'] = 'DC Group ID';
  $params['dcGroupId']['description'] = 'ID for DC pastor group';
  $params['dcGroupId']['title'] = 'DC Group ID';  
}