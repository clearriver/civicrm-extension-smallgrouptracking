<?php
/*
 * Records attendance for church
 */

//PhpConsole\Helper::register();
function civicrm_api3_small_group_tracking_addchurchattendance($params) {
    $date = $params['date'];
    $type = $params['type'];
    $adult_count = $params['adult_count'];
    $child_count = $params['child_count'];
    $date = ProcessDate($date);
    $id = null;
    
    /*
     * Check for existing record
     */
    $type_query = 'Type="' . $type . '";';

    $query = "SELECT * FROM custom_churchnetwork_attendance " .
            "WHERE " .
            "Date = " . $date . " AND " .
            $type_query;
    
    try {
        $dao = CRM_Core_DAO::executeQuery($query);
        $results = array();
        while ($dao->fetch()) {
            PC::dao($dao->id);
            $id = $dao->id;
            //$results[] = $dao->toArray();
        }
    }
    catch (Exception $e) {

        return civicrm_api3_create_success(false);
    }
    $id_query = '';
    $id_val = '';
    if($id) {
        $id_query = "id, ";
        $id_val = $id . ", ";
    }
    
    /*
     * ADD RECORD
     */
    $query = "REPLACE " . 
    "INTO custom_churchnetwork_attendance (" .
    $id_query .
            "Date, " .
    "Type, " .
    "Adult_Count, " .
    "Child_count) " .
    "VALUES (" . $id_val . " '" .
            $date . "', '" .
            $type . "', '" .
            $adult_count . "', '" .
            $child_count . "') " 
            ;
    
    //PC::query($query);
    /**
     * Execute Query
     */
    try {
        $dao = CRM_Core_DAO::executeQuery($query);
        return civicrm_api3_create_success('');
    }
    catch (Exception $e) {

        return civicrm_api3_create_success(false);
    }
    //return civicrm_api3_create_success(false);
}

/**
 * API EXPLORER AUTOFILLS
 * 
 * The metadata is used for setting defaults, documentation &
 * validation.
 * 
 * @param type $params
 * Array of parameters determined by getfields.
 */
function _civicrm_api3_small_group_tracking_addchurchattendance_spec(&$params) {
  $params['date']['name'] = 'Date';
  $params['date']['description'] = 'Date to record';
  $params['date']['title'] = 'Date';

  $params['type']['name'] = 'Type';
  $params['type']['description'] = 'Sunday, Team Meeting, DC';
  $params['type']['title'] = 'Attendance Type';  
  
  $params['adult_count']['name'] = 'adult_count';
  $params['adult_count']['description'] = 'number of adults';
  $params['adult_count']['title'] = 'adult_count';  
  
  $params['child_count']['name'] = 'child_count';
  $params['child_count']['description'] = 'number of children';
  $params['child_count']['title'] = 'child_count';  
}
if (!function_exists('ProcessDate')) {

    function ProcessDate($date) {
        $newDate = str_replace("-", "", $date); // removes hyphen
        $newDate = str_replace("/", "", $date); // removes hyphen
        return $newDate;
    }

}