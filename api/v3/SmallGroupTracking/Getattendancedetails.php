<?php

/*
 * retreives the history of small group attendance
 * first uses cid of contact
 * gets values from activity contact where cid matches
 * 
 * Returns the memberAttendance as object values:
 * memberAttendance[values][cid][date] =  0 or 1
 * memberAttendance[values][cid][cid] =  cid
 * memberAttendance[values][cid][name] =  display_name
 * memberAttendance[values][cid][present_count] =  how many times they were present
 * memberAttendance[values][cid][meeting_count] = total number of meetings 
 */



/**
 * civicrm_api3_small_group_tracking_getsmallgroupattendance
 * @param array $params
 * @return type
 */
function civicrm_api3_small_group_tracking_Getattendancedetails($params) {
    $date = $params['activity_date'];
    $activity_date = ProcessDate($date);
    $sg_group_id = $params['sg_group_id'];
    $query = "select * " .
            "FROM custom_smallgroup_attendance " .
            "WHERE activity_date = '" . $activity_date . "' " .
            "AND sg_group_id = " . $sg_group_id . ";";
    
    $dao = CRM_Core_DAO::executeQuery($query);

    
    $results = array();
    while ($dao->fetch()) {
        $results[] = $dao->toArray();
    }
    
    //ChromePhp::log($results);

    return civicrm_api3_create_success($results);
}
/*
 * Gets the contact name, obviously
 */
/*
function GetContactName($cid, $lastnameFirst) {
    $name = civicrm_api3('Contact', 'get', array(
        'sequential' => 1,
        'id' => $cid,
        'return' => array('display_name','last_name','first_name')
    ));
    
    //ChromePhp::log($name);
    if($lastnameFirst) {
        return ($name['values'][0]['last_name'] . ", " . $name['values'][0]['first_name']);  
    } 
    else {
        return $name['values'][0]['display_name'];
    }
}
*/
/*
 * Removes "-" from the date
 */

if (!function_exists('ProcessDate')) {
    function ProcessDate($date) {
        $newDate = str_replace("-", "", $date); // removes hyphen
        $newDate = str_replace("/", "", $date); // removes hyphen
        return $newDate;
    }
}

