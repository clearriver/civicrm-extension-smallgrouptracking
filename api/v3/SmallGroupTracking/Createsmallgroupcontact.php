<?php

/*
 * Add a contact to a small group
 * 
 */

//require_once('CustomPHP/FirePHPCore/fb.php');

/**
 * civicrm_api3_small_group_tracking_createsmallgroupcontact
 * @param type $params
 * @return type
 */
function civicrm_api3_small_group_tracking_createsmallgroupcontact($params) {
    // fb($params, 'createsmallgroupcontact');
    $cid = $params['cid']; // contact id
    $sgId = $params['sgid']; // small group group id
    $sglId = $params['sglid']; // leader's id
    $role = $params['role'];

    /**
     * Check to see if this person is a small group leader.
     * If they are, then do NOT allow them to be moved to another group
     */
    $tag_id = GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'small_group_leader_tag');
    $result = civicrm_api3('EntityTag', 'get', array(
        'sequential' => 1,
        'entity_id' => $cid, // contacts id
    ));
    
    
    if(sizeof($result['values'] > 0)) { // Make sure there is a tag
        foreach ($result['values'] as $val) {
            if($val['tag_id'] == $tag_id) { // if the tag matches id of sgl
                // fb('tagged, small group leader');
                // This person is a small group leader!
                return civicrm_api3_create_error("This person is tagged as a \"Small Group Leader\" and cannot be added to this group.");
            }
        }
    }
    
    // CHECK/REMOVE IF PART OF ANOTHER SMALL GROUP
    $removalResult = civicrm_api3('SmallGroupTracking', 'removesmallgroupcontact', array(
        'sequential' => 1,
        'cid' => $cid,
    ));
        
    // UPDATE CUSTOM FIELDS
    // Small Group -> Role/Leader
    
    // Get the IDs for the custom fields
    $custom1 = 'custom_' . GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'small_group_member_role_custom_field');
    $custom2 = 'custom_' . GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'small_group_leader_custom_field');
    

    $result = civicrm_api3('CustomValue', 'create', array(
        'sequential' => 1,
        'entity_id' => $cid,
        $custom1 => $role, // role
        $custom2 => $sglId, // leader
    ));

    // Add Contact to the Group
    $result = civicrm_api3('GroupContact', 'create', array(
        'sequential' => 1,
        'contact_id' => $cid,
        'group_id' => $sgId,
    ));
    
    $group = civicrm_api3('Group', 'get', array(
        'sequential' => 1,
        'id' => $removalResult['lastgroup'],
    ));
    
    $contact = civicrm_api3('Contact', 'get', array(
        'sequential' => 1,
        'id' => $cid,
    ));
    
    $result['lastgroup'] = $group['values'][0]['title'];
    $result['contactname'] = $contact['values'][0]['display_name'];
    
    // Check i

    return $result;
}
