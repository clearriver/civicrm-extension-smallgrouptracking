<?php

/*
 * Add a contact to a small group
 * 
 */


// require_once('CustomPHP/FirePHPCore/fb.php');
/**
 * civicrm_api3_small_group_tracking_removesmallgroupcontact
 * @param type $params
 * @return type
 */
function civicrm_api3_small_group_tracking_removesmallgroupcontact($params) {
    // fb("civicrm_api3_small_group_tracking_removesmallgroupcontact");
    //ChromePhp::log($params);
    $cid = $params['cid']; // contact id
    $sgId = $params['sgid']; // small group group id
    // If the sgId is not provided, see if they are in a small group by name
    if (!$sgId) {
        //ChromePhp::log("removesmallgroupcontact: no sgId, search group");
        $groups = civicrm_api3('GroupContact', 'get', array(
            'sequential' => 1,
            'contact_id' => $cid,
        ));
        //ChromePhp::log($groups["values"]);
        // loop through and check for group containing "small group"
        foreach ($groups["values"] as $group) {
            //ChromePhp::log(strpos($group["title"], "Small Group"));
            if (strpos($group["title"], "Small Group") !== false) {
                $sgId = $group["group_id"];
                // fb($sgId, 'Small Group (group) Id to remove from');
                //ChromePhp::log('Contact is part of Small Group ID: ' . $sgId);
                break; // stop the loop
            } else {
                // fb('WARNING: Small Group (group) not found');
            }
        }
    }
    
    // Remove Contact from the Group
    if ($sgId) {
        // fb('Remove contact with CID from Group ID: ' . $cid . '/' . $sgId);
        $result = civicrm_api3('GroupContact', 'delete', array(
            'sequential' => 1,
            'contact_id' => $cid,
            'group_id' => $sgId, // group id
        ));
        // fb($result, 'GroupContact Delete');
    }
    // UPDATE CUSTOM FIELDS
    // Small Group -> Role/Leader
    // fb('Update Custom Fields');
    
    // Get the IDs for the custom fields
    $custom1 = 'custom_' . GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'small_group_member_role_custom_field');
    $custom2 = 'custom_' . GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'small_group_leader_custom_field');
    
    
    $result = civicrm_api3('CustomValue', 'create', array(
        'sequential' => 1,
        'entity_id' => $cid,
        $custom1 => null, // role
        $custom2 => null, // leader
    ));
    
    $result['lastgroup'] = $sgId;

    // fb($result);
    return $result;
}
