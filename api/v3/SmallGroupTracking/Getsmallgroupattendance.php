<?php

/*
 * retreives the history of small group attendance
 * first uses cid of contact
 * gets values from activity contact where cid matches
 * 
 * Returns the memberAttendance as object values:
 * memberAttendance[values][cid][date] =  0 or 1
 * memberAttendance[values][cid][cid] =  cid
 * memberAttendance[values][cid][name] =  display_name
 * memberAttendance[values][cid][present_count] =  how many times they were present
 * memberAttendance[values][cid][meeting_count] = total number of meetings 
 */


// require_once('CustomPHP/FirePHPCore/fb.php');

/**
 * civicrm_api3_small_group_tracking_getsmallgroupattendance
 * @param array $params
 * @return type
 */
function civicrm_api3_small_group_tracking_getsmallgroupattendance($params) {
    
    /*
      $params = array(
      'start_date' => 20141022,
      'end_date' => 20141022,
      //'sgl_id' => 2,
      'sgg_id' => 38,
      'activity_type_id' => 58,
      'cids' => array(2881, 2)
      );
     */
    $cids = array();
    $startDate = $params['start_date']; // passed smallgroup leaders name
    $endDate = $params['end_date']; // passed smallgroup leaders name
    $source_contact_id = $params['sgl_id']; // small group leader id
    $activity_type_id = $params['activity_type_id']; // id for small group attendance
    $group_group_id = $params['sgg_id'];
    $meeting_type = $params['meeting_type'];
    
    $cids = $params['cids'];
    $present_count = 0; // counts the number of times they are present
    $meeting_count = 0;
    

    $results = array();
    $dates = array();
    $memberAttendance = array();

    //PC::debug($meeting_type,'meeting_type');
    
    /**
     *  GET THE LOGGED MEETING DATES
     */
    $result = civicrm_api3('SmallGroupTracking', 'getlogsmallgroupmeeting', array(
        'sequential' => 1,
        'start_date' => $startDate,
        'end_date' => $endDate,
        'sgg_id' => $group_group_id,
        'meeting_type' => $meeting_type,
    ));
    
    //// fb('civicrm_api3_small_group_tracking_getsmallgroupattendance: got logged meeting dates');
    // Store meeting dates and type
    // possibly store the date as the primary key
    $i = 0;
    foreach ($result['values'] as $val) {
        $dates[$i]['date'] = FormatDate($val['meeting_date']);
        $dates[$i]['type'] = $val['meeting_type'];
        //$dates[] = $val['meeting_date'];
        $i++;
    }
    $meeting_count = $i;
    //// fb($meeting_count, 'meeting count');
    //ChromePhp::log($dates);

    /**
     * Deal with situation where CIDs are not given
     * We need to get members from the actual attendance history
     * we want ALL records, even if they are not currently in the small group
     * 
     * This info is in custom_smallgroup_meeting_attendance
     * Get records that = sg_group_id
     * 
     * Should get logs that are only between the DATE range
     * 
     */
    if (!$cids) {
        //// fb('civicrm_api3_small_group_tracking_getsmallgroupattendance: No CID Provided');
        $present_count = 0; // reset to 0 
        //$meeting_count = 0;
        $i = 0;
        
        // HANDLE MEETING TYPE FILTER
        $meeting_type_query = '';
        $type_count = sizeof($meeting_type);
        if($meeting_type) {
            $meeting_type_query = "AND activity_type IN (";
            for($i = 0; $i <= sizeof($meeting_type)-1; $i++) {
                if($i>0) {
                    $meeting_type_query .= ", ";
                }
                $meeting_type_query .= "\"" . $meeting_type[$i] . "\" ";
            }
            $meeting_type_query .= ") ";
        }
        /**
         * This query will return all contacts that have records within the range
         * 
         * We need to get the CIDs for each of the people in the small group.
         * To do this we will group the results by the member_id, which is
         * the CID
         *
         */
        $query = "SELECT " .
                "member_id, " .
                "sg_group_id, " .
                "group_leader_id, " .
                "civicrm_contact.display_name, " .
                "activity_date, " .
                "activity_type, " .
                "member_role, " .
                "attended, " .
                "notes " .
                "FROM custom_smallgroup_attendance " .
                "left join civicrm_contact on civicrm_contact.id = custom_smallgroup_attendance.member_id " .
                "where custom_smallgroup_attendance.is_deleted=0 AND sg_group_id = " . $group_group_id . " " .
                "and ((activity_date >= " . $startDate . ") and (activity_date <= " . $endDate . ")) " .
                $meeting_type_query .
                "group by member_id;";

        // fb($query, 'civicrm_api3_small_group_tracking_getsmallgroupattendance: query');
        //PC::debug($query, "$query");
        $dao = CRM_Core_DAO::executeQuery($query);
        
        
        while ($dao->fetch()) {
            $results[] = $dao->toArray();
            //$results[$cid][$i] = $dao->toArray();
        }

        /**
         * Now we have a list of all records for small group contacts within
         * the given date range.
         * 
         * We need to get the unique CIDs 
         */
        //// fb($results);

        /**
         *  Loop through each contact in the results and get their CID
         */
        $contacts = array();
        foreach ($results as $contact) {
            $cids[] = $contact['member_id']; // put cids into the array
            //$contacts[$contact['member_id']]
        }
        //// fb($cids,'CIDs');
        
        /**
         * Go through each contact getting their records for the given range
         * 
         * Each CID is looped over looking up a matching date
         */

        foreach ($cids as $cid) {
            
            $present_count = 0;
            foreach ($dates as $date) {
                $results = array(); // clear array so no results will be null
                //$meeting_count++;
                $query = "SELECT " .
                        "member_id, " .
                        "sg_group_id, " .
                        "group_leader_id, " .
                        "civicrm_contact.display_name, " .
                        "activity_date, " .
                        "activity_type as meeting_type, " .
                        "member_role, " .
                        "attended, " .
                        "notes " .
                        "FROM custom_smallgroup_attendance " .
                        "left join civicrm_contact on civicrm_contact.id = custom_smallgroup_attendance.member_id " .
                        "where custom_smallgroup_attendance.is_deleted=0 AND sg_group_id = " . $group_group_id . " " .
                        "and activity_date = " . $date['date'] . " " .
                        "and member_id = " . $cid . " " .
                        $meeting_type_query .
                        "order by activity_date;";

                //// fb($query);
                $dao = CRM_Core_DAO::executeQuery($query);
                while ($dao->fetch()) {
                    $results[$cid] = $dao->toArray();
                    
                    //if($cid == 5321) {
                    //    // fb($results[$cid], 'nick');
                    //}
                    //$results[$cid][$i] = $dao->toArray();
                }
                //// fb('contactDateResult', $results);

                /**
                 * Assign the return values
                 */
                if ($results[$cid]) {
                    // check value based on activity status
                    if ($results[$cid]['attended'] == 1) {
                        //$memberAttendance[$cid]['meetings'][$date['date']] = 1; // present
                        $memberAttendance[$cid][$date['date']] = 1; // present
                        $present_count++;

                        
                    } else {
                        //$memberAttendance[$cid]['meetings'][$date['date']] = 0; // not present
                        $memberAttendance[$cid][$date['date']] = 0; // not present
                    }
                    $memberAttendance[$cid]['name'] = $results[$cid]['display_name'];
                } else {
                    //if($cid == 5321) {
                        // fb('no result', $cid);
                    //}
                    // dealing with someone missing results
                    //$memberAttendance[$cid]['meetings'][$date['date']] = 0;
                    $memberAttendance[$cid][$date['date']] = null; // not present


                    $memberAttendance[$cid]['name'] =  GetContactName($cid, false);
                }

                // Set the present count for someone that has 0 attendance for given dates
                if ($present_count == 0) {
                    //$memberAttendance[$cid]['present_count'] = 0 . "/" . $meeting_count;
                }
            }
            // UPDATE NUMBER OF TIMES PRESENT
            $memberAttendance[$cid]['present_count'] = $present_count . "/" . $meeting_count;
        }
    } else { // CIDs provided
        // fb('civicrm_api3_small_group_tracking_getsmallgroupattendance: CIDs Provided');
        // Here we are getting record from the ACTIVITY
        foreach ($cids as $cid) {
            $present_count = 0; // reset to 0 
            //$meeting_count = 0;
            $i = 0;
            foreach ($dates as $date) {
                //$meeting_count++;
                $query = "select " .
                        "civicrm_contact.id as cid," .
                        "civicrm_contact.display_name," .
                        "civicrm_activity.subject as subject," .
                        "civicrm_activity.activity_date_time," .
                        "civicrm_activity.details," .
                        "civicrm_activity.location," .
                        "civicrm_activity.status_id," . // # 11:present, 12:absent 
                        // civicrm_activity.source_record_id,
                        // civicrm_activity_contact.source_contact_id, # small group leader id
                        // civicrm_activity_contact.record_type_id as record_type_id,
                        // civicrm_activity_contact.id as activity_id,
                        // civicrm_activity_contact.contact_id as activity_cid,
                        "civicrm_activity.activity_type_id as activity_type_id " .
                        "FROM civicrm_contact " .
                        "left join civicrm_activity_contact on civicrm_activity_contact.contact_id = civicrm_contact.id " .
                        "left join civicrm_activity on civicrm_activity.id = civicrm_activity_contact.activity_id " .
                        "where activity_type_id = " . $activity_type_id . " " . // # small group attendance activity type
                        //"and civicrm_activity.activity_date_time BETWEEN " . $startDate . " AND " . $endDate . " " .
                        "and civicrm_activity.activity_date_time = " . $date['date'] . " " .
                        "and civicrm_contact.id = " . $cid . " " .
                        "and record_type_id = 3 " . // only contact as target for activity
                        "order by civicrm_activity.activity_date_time;";
                //ChromePhp::log($query);
                $dao = CRM_Core_DAO::executeQuery($query);

                while ($dao->fetch()) {
                    $results[] = $dao->toArray();
                }


                // Store if present or absent
                /*
                 * memberData is an arry with primary key as the cid of the contact\
                 * the secondary key is the meeting date
                 * the value is if they were present or absent as an int
                 * 
                 * Something here is VERY wrong, results are not comming back correctly.
                 */
                if ($results[$cid]) {
                    // check value based on activity status
                    if ($results[$cid][$i]['status_id'] == 12) {
                        //$memberAttendance[$cid]['meetings'][$date['date']] = 1; // present
                        $memberAttendance[$cid][$date['date']] = 1; // present
                        $present_count++;

                        // UPDATE NUMBER OF TIMES PRESENT
                        $memberAttendance[$cid]['present_count'] = $present_count . "/" . $meeting_count;
                    } else {
                        //$memberAttendance[$cid]['meetings'][$date['date']] = 0; // not present
                        $memberAttendance[$cid][$date['date']] = 0; // not present
                    }
                    $memberAttendance[$cid]['name'] = GetContactName($cid, true);
                } else {
                    // dealing with someone missing results
                    //$memberAttendance[$cid]['meetings'][$date['date']] = 0;
                    $memberAttendance[$cid][$date['date']] = null; // not present


                    $memberAttendance[$cid]['name'] = GetContactName($cid, true); // set with lastname first
                }

                // Set the present count for someone that has 0 attendance for given dates
                if ($present_count == 0) {
                    $memberAttendance[$cid]['present_count'] = 0 . "/" . $meeting_count;
                }

                //$memberAttendance[$cid]['meetings']['type'] = $date['type']; // what type of meeting
                //$memberAttendance[$cid]['type'] = $date['type']; // what type of meeting
                //$memberAttendance[$cid]['cid'] = $cid;
                //ChromePhp::log($memberData);
            }
        }
    }
    //ChromePhp::log($memberAttendance);
    //ChromePhp::log($query);
    //ChromePhp::log($results);
    // Connect contact with activity
    // civicrm_activity_contact.contact_id = civicrm_contact.id


    /* activityGet will let you search by the source_contact_id
     * This can then be matched up with the activity_contact
     */

    /* ActivityContactGet
     * ActivityContactGet.activity_id = ActivityGet.id
     * 
     * return record_type_3 which is target
     */

    /* API ATTEMPT

      $results = civicrm_api3('ActivityContact', 'get', array(
      'sequential' => 1,
      'contact_id' => $cid,
      'record_type_id' => 3,   // target
      'return' => 'activity_id'
      ));

      // get each result
      foreach ($results['values'] as $result) {
      ChromePhp::log($result);
      }

     */
    //ChromePhp::log($results);
    
    /**
     * Return the date details and contact details
     * values[] size is 2, where 0 holds dates, and 1 hold contacts
     */
    // fb($memberAttendance, 'MemberAttendance');
    return civicrm_api3_create_success(array($dates,$memberAttendance));
}

/*
 * Gets the contact name, obviously
 */

function GetContactName($cid, $lastnameFirst) {
    $name = civicrm_api3('Contact', 'get', array(
        'sequential' => 1,
        'id' => $cid,
        'return' => array('display_name', 'last_name', 'first_name')
    ));

    //ChromePhp::log($name);
    if ($lastnameFirst) {
        return ($name['values'][0]['last_name'] . ", " . $name['values'][0]['first_name']);
    } else {
        return $name['values'][0]['display_name'];
    }
}

/*
 * Removes "-" from the date
 */

function FormatDate($date) {
    $newDate = str_replace("-", "", $date); // removes hyphen
    return $newDate;
}

class meetingDate {
    
}
