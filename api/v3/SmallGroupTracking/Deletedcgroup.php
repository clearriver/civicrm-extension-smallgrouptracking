<?php


// require_once('CustomPHP/FirePHPCore/fb.php');



/*
 * Delete a dc group 
 * ONLY if child groups do not exist
 * 
 */
function civicrm_api3_small_group_tracking_deletedcgroup($params) {
    $force = $params['force']; // force delete when value is 1
    $id = $params['id']; // group id
    $tmpGroupId = 0;
    //$childGroups = 0; // holds ids for child groups
    
    
    /* 
     * HANDLE IF THERE ARE GROUPS UNDER THE DC GROUP
     */
    try {  
        $childGroupResult = GetChildGroups($id);// GET CHILD GROUPS FOR DC WE ARE DELETING
        
        if ($childGroupResult && $force) { // DO when children exist


            /*
             * Originally this created a temp group and re-assigned child groups
             * to it. However, we should force the user to re-assign groups 
             * before removing a DC
             */
            
            
            //$result = GetTempGroup(); // Check for temp group
            
            if (!$result['values']) { 
                // fb("Temp Group Does Not Exist, Creating...");
             //   $tmpGroupId = CreateTempGroup(); // Create temp group
                //$firephp->log($tmpGroupId, 'Temp Group Created');
            } 
            else { // set the id for existing group
             //   $tmpGroupId = $result['values'][0]['id'];
                //$firephp->log($tmpGroupId, 'Temp Group Exists');
            }

            // Assign children to temp group
            $childGroups = explode(',', $childGroupResult['values'][0]['children']);
            //$firephp->log($childGroups, 'childGroups');
            //AssignChildGroupsToTemp($childGroups,$id);
            
            DeleteChildGroups($childGroups, $id);
            //$firephp->log($childGroups, 'Child Group assigned to Temp Group');
            
            return civicrm_api3_create_success($result);
        }
        else if ($childGroupResult) {
            $result = array (
                'alert' => 'There are small groups still in this DC.',
                'error' => 1,
            );
            return civicrm_api3_create_success($result);
        }
        else { // No Child groups
            DeleteDCGroup($id);
        }
        
    } catch (Exception $result) {

        $error = $result->getMessage();
        return civicrm_api3_create_success($error); // if not set to success the page will not display
    }

    return civicrm_api3_create_success($result);
}


function DeleteDCGroup($id) {
    // DELETE DC GROUP
    $result = civicrm_api3('Group', 'delete', array(
        'sequential' => 1,
        'id' => $id,
    ));
}

function DeleteChildGroups($childGroups, $id) {
    for ($i = 0; $i < sizeof($childGroups); $i++) {
        // DELETE SMALL GROUP
        $result = civicrm_api3('Group', 'delete', array(
            'sequential' => 1,
            'id' => $childGroups[$i],
        ));
    }
    // Delete DC Group
    DeleteDCGroup($id);
}

function AssignChildGroupsToTemp($childGroups, $id) {
    for ($i = 0; $i < sizeof($childGroups); $i++) {
        // Update Child groups assigning parent
        $result = civicrm_api3('Group', 'create', array(
            'sequential' => 1,
            'id' => $childGroups[$i],
            'parents' => $id,
        ));
    }
}

function CreateTempGroup() {
    $result = civicrm_api3('Group', 'create', array(
        'sequential' => 1,
        'title' => "sgt_temp",
    ));
    $tmpGroupId = $result['values'][0]['id'];
    return $tmpGroupId;
}

function GetTempGroup() {
    $result = civicrm_api3('Group', 'get', array(
        'sequential' => 1,
        'title' => "sgt_temp",
    ));
    return $result;
}

function GetChildGroups($id) {
    $result = civicrm_api3('Group', 'get', array(
        'sequential' => 1,
        'return' => array("children"),
        'id' => $id,
    ));
    //return $result['values'][0]['children'];
    return $result;
}
