<?php

//PhpConsole\Helper::register();

/**
 * get all small group contacts based on group id
 * 
 * @param type $params
 * @return type
 */
function civicrm_api3_small_group_tracking_getsmallgroupcontacts($params) {
    //// fb('go');
    //CustomTables();

    $groupID =  $params['group_id'];    

    /**
     * GET CUSTOM GROUP TABLE
     * 
     * This is the name of the database table for "Small Group field group"
     */
    $custom_group_table = GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'custom_group_table');
    
    /**
     * GET CUSTOM FIELD FOR ROLE
     * 
     * This is the name of the column in the custom group table
     */
    $field_id = GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'small_group_member_role_custom_field'); // id for the contact role in a group
    $field_role = 'small_group_member_role_' . $field_id; // column of the small group table (CUSTOM GROUP TABLE)
    
    /**
     * NEW QUERY
     */
    // Assign SELECT clause
    $query = 
        "SELECT " .
        "`civicrm_contact`.`id` AS `id`, " .
        "`civicrm_contact`.`last_name` AS `last_name`, " .
        "`civicrm_contact`.`first_name` AS `first_name`, " .
        "`civicrm_contact`.`display_name` AS `display_name`, " .
        "civicrm_group_contact.group_id as group_id, " .
        "GROUP_CONCAT(DISTINCT `civicrm_membership_type`.`name` SEPARATOR ',')  AS `membership_type`, "  .
        $custom_group_table . "." . $field_role . " AS role "; // get the contacts role from the custom small group table
    
    /*
     * GET CUSTOM COLUMNS
     * 
     * Gets the custom columns specified in the member settings tab and adds them to the select
     * 
     */
    $customColumns = explode(',',GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'CustomFields'));  // separate the string into an array
    $columnTables = [];
    //// fb($customColumns, 'customColumns');
    if($customColumns[0] != '') {
        foreach ($customColumns as $column) {
            $columnMembers = explode(':', $column);  // separate the table name from the specified display name
            array_push($columnTables, $columnMembers[0]); // add the custom table to the array
           
            /*
             * Add custom tables and fields to the select
             * 
             * EXAMPLE: custom_table.column_name AS pretty_name
             */
            $query .= ',' . $columnMembers[0] . '.' . $columnMembers[1] . " AS `" . $columnMembers[1] . "` "; // 0 = db table, 1 = column name in the table, 2 = Display Name we specified in member tab settings
        }
    }
    
    //// fb($columnTables);
    
    
    /*
     * Membership status (api membershipStatus>Get()
     * 1 = Pending
     * 2 = Current
     * 4 = Expired
     * 
     * Returns the ID and label
     * values[0]{}.id
     * values[0]{}.label
     */
    
    /*
    $membershipStatus = civicrm_api3('MembershipStatus', 'get', array(
        'sequential' => 1,
        'is_active' => 1,
        'return' => array("label"),
    ));
    */
    
    // Append FROM clause
    $query .=
        "FROM " .
        "`civicrm_contact` " .
        "INNER JOIN civicrm_group_contact on civicrm_contact.id = civicrm_group_contact.contact_id AND civicrm_group_contact.status = 'Added' " .
        "LEFT JOIN `civicrm_membership` ON `civicrm_contact`.`id` = `civicrm_membership`.`contact_id` AND (civicrm_membership.status_id = 2 OR civicrm_membership.status_id = 1 OR civicrm_membership.status_id = 4) " .
        "LEFT JOIN `civicrm_membership_type` ON `civicrm_membership`.`membership_type_id` = `civicrm_membership_type`.`id` " .
        "LEFT JOIN " . $custom_group_table . " ON civicrm_contact.id = " . $custom_group_table . ".entity_id ";  // join to the custom small group table 
    
    // Get custom column tables and join them to FROM clause
    foreach (array_unique($columnTables) as $table) {
        $query .= 'LEFT JOIN ' . $table . ' ON civicrm_contact.id = ' . $table . '.entity_id ';
    }
    
    // Append WHERE clause
    $query .=
        "WHERE " .
        "(`civicrm_contact`.`first_name` IS NOT NULL) " .
            "AND (`civicrm_contact`.`last_name` IS NOT NULL) " .
            "AND (`civicrm_contact`.`is_deleted` = 0) " .
            "AND `civicrm_contact`.`contact_type` = 'Individual' " .
            "AND civicrm_group_contact.group_id = " . $groupID . " " .
            "GROUP BY civicrm_contact.id ORDER BY civicrm_contact.last_name";

    
    //PC::GetSmallGroupContactsQuery($query);
    

    $dao = CRM_Core_DAO::executeQuery($query);
 
    $results = array();
    //// fb($dao->toArray());
    while ($dao->fetch()) {
        
		$results[] = $dao->toArray();
    }
    
    //PC::MemberResults($results);
    
    //$cgTable = CRM_Core_DAO_CustomGroup::getTableName();
    //// fb($cgTable,'customgouptable');
    return civicrm_api3_create_success($results);
}

/**
 * Load settings for custom values to include
 */
function LoadSettings() {
    
}

/**
 * API EXPLORER AUTOFILLS
 * 
 * The metadata is used for setting defaults, documentation &
 * validation.
 * 
 * @param type $params
 * Array of parameters determined by getfields.
 */
function _civicrm_api3_small_group_tracking_getsmallgroupcontacts_spec(&$params) {
  $params['group_id']['name'] = 'group_id';
  $params['group_id']['description'] = 'ID for group that contacts are in';
  $params['group_id']['title'] = 'Small Group (group) ID';
  // Fields which should default to the value of another parameter should be marked using 'api.aliases'
  //$params['group_id']['api.aliases'] = array('messageTemplateID', 'message_template_id');
}