<?php

/*
 * Gets church attendance info
 */



/**
 * civicrm_api3_small_group_tracking_getsmallgroupattendance
 * @param array $params
 * @return type
 */
function civicrm_api3_small_group_tracking_Getchurchattendance($params) {
    $date = $params['date'];
    $activity_date = ProcessDate($date);
    $sg_group_id = $params['sg_group_id'];
    $query = "select * " .
            "FROM custom_churchnetwork_attendance " .
             ";";
    
    $dao = CRM_Core_DAO::executeQuery($query);

    
    $results = array();
    while ($dao->fetch()) {
        $results[] = $dao->toArray();
    }
    
    //ChromePhp::log($results);

    return civicrm_api3_create_success($results);
}