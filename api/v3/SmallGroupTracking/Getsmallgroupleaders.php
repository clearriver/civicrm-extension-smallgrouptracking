<?php

/* 
 * Lookup small group leaders and filter by their DC group parent
 */


// require_once('CustomPHP/FirePHPCore/fb.php');

function civicrm_api3_small_group_tracking_getsmallgroupleaders($params) {
    $dcID =  $params['dc_id'];
   
    $query = "select " .
        "civicrm_group.parents as parent_id, " .
        "civicrm_group.title, " .
        "civicrm_group_contact.group_id, " .
        "civicrm_contact.id as contact_id, " .
        "civicrm_contact.display_name " .
        "from civicrm_group_contact " .
        "left join civicrm_contact on civicrm_contact.id = civicrm_group_contact.contact_id " .
        "left join civicrm_entity_tag on civicrm_entity_tag.entity_id = civicrm_contact.id " .
        "left join civicrm_group on civicrm_group.id = civicrm_group_contact.group_id and civicrm_group_contact.status = 'Added' " .
        "where civicrm_entity_tag.tag_id = " . GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'small_group_leader_tag') . " " .
        "and civicrm_group.parents = " . $dcID . " order by civicrm_contact.last_name";

    // ChromePhp::log($query);
    
    //// fb($query);
    $dao = CRM_Core_DAO::executeQuery($query);
    $results = array();
    while ($dao->fetch()) {
		$results[] = $dao->toArray();
    }
    
    
    // fb($results);
    return civicrm_api3_create_success($results);
}