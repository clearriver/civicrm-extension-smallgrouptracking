<?php

/* 
 * This will return a list of DC groups and the children IDs associated with
 * each of the DC groups
 * 
 * The contacts can then be filtered based on the children group IDs 
 */

// require_once('CustomPHP/FirePHPCore/fb.php');
/**
 * civicrm_api3_member_tracking_gettithes
 * @param type $params
 * @return type
 * Gets the DC groups
 * returns title, children, and parent groups
 */
function civicrm_api3_small_group_tracking_addsmallgroupattendance($params) {
    
    $attendance = json_decode($params['attendance']);
    
    //Loop through members to construct insert
    
    $query = "REPLACE " . 
    "INTO custom_smallgroup_attendance (" .
    "member_id, " .
    "activity_date, " .
    "dc_leader_id, " .
    "sg_group_id, " .
    "group_leader_id, " .
    "activity_type, " .
    "member_role, " .
    "attended, " .
    "notes) " .
    "VALUES ";
    
    for ($i = 0;$i < count ($attendance->members); $i++) {
        if ($i > 0) {
            $query .= ",";
        }
        
        if (is_null($attendance->members[$i]->ID) == false) {
            $query .= "(" . $attendance->members[$i]->ID . ",";
        } else {
            continue;
        }
        
        if (is_null($attendance->members[$i]->activityDate) == false) {
            $query .= "STR_TO_DATE('" . $attendance->members[$i]->activityDate . "','%m/%d/%Y'),";
        } else {
            continue;
        }
        
        if (is_null($attendance->dcLeaderID) == false) {
            $query .= $attendance->dcLeaderID . ",";
        } else {
            $query .= "null,";
        }
        
        if (is_null($attendance->sgGroupID) == false) {
            $query .= $attendance->sgGroupID . ",";
        } else {
            $query .= "null,";
        }
        
        if (is_null($attendance->sgLeaderID) == false) {
            $query .= $attendance->sgLeaderID . ",";
        } else {
            $query .= "null,";
        }
        
        if (is_null($attendance->members[$i]->activityType) == false) {
            $query .= "'" . $attendance->members[$i]->activityType . "',";
        } else {
            $query .= "null,";
        }
        
        if (is_null($attendance->members[$i]->memberRole) == false) {
            $query .= "'" . $attendance->members[$i]->memberRole . "',";
        } else {
            $query .= "null,";
        }
        
        if (is_null($attendance->members[$i]->attended) == false) {
            if ($attendance->members[$i]->attended == false) {
                $query .= "0,";
            } else {
                $query .= "1,";
            }
        } else {
            $query .= "null,";
        }
        
        if (is_null($attendance->members[$i]->notes) == false) {
            $query .= "'" . $attendance->members[$i]->notes . "')";
        } else {
            $query .= "null)";
        }
    }
    
    //Do insert
    // fb($query, 'INSERT QUERY');
    
    /**
     * Execute Query
     */
    try {
        $dao = CRM_Core_DAO::executeQuery($query);
        return civicrm_api3_create_success('');
    }
    catch (Exception $e) {

        return civicrm_api3_create_success(false);
    }
    //return civicrm_api3_create_success(false);
}