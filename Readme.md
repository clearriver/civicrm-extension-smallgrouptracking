![](https://civicrm.org/sites/civicrm.org/developers/civi-header-bar-logo.png) Small Group Tracking Extension
======================================
----------

*This documentation is a work in progress (obviously) ...*

OVERVIEW
========
This CiviCRM extension is designed to add functionality for tracking small groups and member attendance. 

**The groups are organized under the following CiviCRM group structure:**

- Discipleship Community
	- Discipleship Community Sub-group(s)
		- Small Group(s)

The **Discipleship Community (DC)** group is created when the extension is installed. All other groups created within the system are created with this group as the parent. A **DC sub-group** must be created through the user interface prior to adding small groups. 

A custom group of data is created for individual contacts. This custom group holds a contact reference to a contact's small group leader, as well as their attendance role within the group.

Small group leaders are tagged "Small Group Leader".

Attendance and meetings are logged in custom MySQL tables. A CiviCRM activity is also logged for each contact when attendance is recorded to allow for searching within CiviCRM's core.


INSTALLATION
============
Download and extract the extension into the custom extension directory defined on the CiviCRM installation.

POST-INSTALLATION SETUP
============

Permissions
-----------------------
After installation be sure to set permissions for user roles. 

There are two new permissions: 

- **Access SmallGroupTracking**
	- Access and record attendance for a small group 
- **Edit SmallGroupTracking**
	- Access, record attendance, and manage group structure (create/edit/delete)

Menu Access
-----------------------
A new menu item will be available under **Contacts > Small Group Tracking**. If the menu item is not visible after setting permissions, then clear the CiviCRM Caches (civicrm/admin/setting/updateConfigBackend?reset=1).

USING THE EXTENSION
============
YouTube videos are available that give details for the usage of the system [HERE](https://www.youtube.com/playlist?list=PLkpoFVyFMHWPpSDVTAgV9DsSFfRnelqAb).

Creating a Small Group Structure
-----------------------
Before creating a small group, and DC group must be created. By default the DC groups are organized by day of the week, last name and first name. The first and last name of the group can be customized as needed by the organization. 

Small groups are created and placed within a DC group. A small group leader must exist as a contact in the system. 

The following are examples of how small groups may be organized in the system:

**DC Group Structure Example 1:**
In this example, small groups are organized by classes and the day they meet.

- Discipleship Community
	- Monday-DC (Men, Alpha)
		- Leader 1
		- Leader 2
		- Leader 3
	- Monday-DC (Women, Alpha)
		- Leader 1
	- Tuesday-DC (Couples, Newly Married)
		- Leader 1
		- Leader 2

**DC Group Structure Example 2:**
In this example pastors oversee groups of small groups for a given day.

- Discipleship Community
	- Monday-DC (Joe, Pastor)
		- Leader 1
		- Leader 2
		- Leader 3
	- Monday-DC (Paul, Pastor)
		- Leader 1
	- Tuesday-DC (Saul, Pastor)
		- Leader 1
		- Leader 2


DEVELOPER INFO
==============


Attendance Logging
------------------
MySQL Table Method
------------------
Attendance is logged using two methods. The first method (and faster method) is storing attendance and meeting logs in two custom tables. 



Small group meetings are logged in a custom table. This is used to keep track of the actual meetings for each small group. This is necessary to handle contacts that have not attended during the entirety of a selected range of dates.

### Level 3 Header

CiviCRM Activity Method
-----------------------
The second method creates a CiviCRM activity record using the activity type: **Small Group Attendance **

This method was implemented to allow for the use of CiviCRM built-in search and report functionality. Without the activity data this information would not be available to the core of CiviCRM.


STORED SETTINGS
---------------


Group name = org.namelessnetwork.smallgrouptracking

SQL Table: civicrm_settings

group_name    							| name           				| value		  
------------- 							|:-------------					| -----:
org.namelessnetwork.smallgrouptracking	|small_group_leader_tag			| int		|
org.namelessnetwork.smallgrouptracking	|discipleship_community_group	| int 		|
org.namelessnetwork.smallgrouptracking	|small_group_custom_group		| int 		|
org.namelessnetwork.smallgrouptracking	|custom_group_table				| string 	| name of the table that CiviCRM generates when a custom_group is created
org.namelessnetwork.smallgrouptracking	|small_group_leader_custom_field	| int 		|
org.namelessnetwork.smallgrouptracking	|small_group_member_role_custom_field	| int 
org.namelessnetwork.smallgrouptracking	|discipleship_community_group	| int 

    GetCiviSetting($group_name, $name)
Pulls settings from the civicrm_settings table

API Additions
-------------