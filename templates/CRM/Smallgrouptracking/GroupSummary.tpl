<!--
<div id='divAddress'>
    <h3>Group Address</h3>
    <div id='groupAddress' style=''>
        <div class='street'>
            4413 Amesbury Dr.
        </div>
        <div class='cityStateZip'>
            W Lafayette, IN 47906
        </div>
    </div>
</div> 
-->
<div id='divMemberStats'>
    <h3>Group Member Stats</h3>
    <table>
        <thead class='sticky'>
            <tr>
                <th>Regular</th><th>Irregular</th><th>Visitor</th><th>Inactive</th>
            </tr>
        </thead>
        <tbody>
            <tr id='summary-stats'>
                <td id='stat-regularMembers'></td><td id='stat-irregularMembers'></td><td id='stat-visitors'></td><td id='stat-inactive'></td>
            </tr>
        </tbody>
    </table>
</div>