<!-- https://clearriver.namelessnetwork.org/civicrm/smallgrouptracking -->
<!-- Resources Reference
        adding external files, etc
        http://wiki.civicrm.org/confluence/display/CRMDOC/Resource+Reference
-->
{crmScript weight="0" ext=org.namelessnetwork.smallgrouptracking file=js/plugins/jquery.filtertable.min.js}
{crmStyle ext=org.namelessnetwork.smallgrouptracking file=css/smallgrouptracking.css}
{crmStyle ext=org.namelessnetwork.smallgrouptracking file=css/paginate.css}
{crmScript weight="0" ext=org.namelessnetwork.smallgrouptracking file=js/angular/angular.js}
{crmScript weight="0" ext=org.namelessnetwork.smallgrouptracking file=js/utils.js}
{crmScript weight="1" ext=org.namelessnetwork.smallgrouptracking file=js/jquery-throttle-debounce.js}
{crmScript weight="1" ext=org.namelessnetwork.smallgrouptracking file=js/StickyHeaders.js}
{crmScript weight="1" ext=org.namelessnetwork.smallgrouptracking file=js/plugins/tableHeadFixer.js}
{crmScript weight="2" ext=org.namelessnetwork.smallgrouptracking file=js/smallgrouptracking.js}
{crmScript weight="2" ext=org.namelessnetwork.smallgrouptracking file=js/AttendanceEntry.js}
{crmScript weight="2" ext=org.namelessnetwork.smallgrouptracking file=js/AttendanceHistory.js}
{*{crmScript weight="3" ext=org.namelessnetwork.smallgrouptracking file=js/Reports.js}*}
{crmScript weight="2" ext=org.namelessnetwork.smallgrouptracking file=js/plugins/jquery.pajinate.min.js}

{crmScript weight="3" ext=org.namelessnetwork.smallgrouptracking file=js/sgt_sgDcGroupManage.js} <!-- manages dc and sg group creation/deletion -->
{crmScript weight="1" ext=org.namelessnetwork.smallgrouptracking file=js/smallgrouptracking_attendanceEntry.js}
{crmScript weight="3" ext=org.namelessnetwork.smallgrouptracking file=js/Dcsummary.js}
{crmScript weight="3" ext=org.namelessnetwork.smallgrouptracking file=js/date.js}

{*
{crmScript ext=org.namelessnetwork.smallgrouptracking url=https://code.jquery.com/ui/1.11.2/jquery-ui.js}
{crmStyle ext=org.namelessnetwork.smallgrouptracking url=https:////code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css}
*}

{*
{crmScript ext=org.namelessnetwork.smallgrouptracking url=https://cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js}
{crmStyle ext=org.namelessnetwork.smallgrouptracking url=https://cdn.datatables.net/1.10.3/css/jquery.dataTables.min.css}
*}
{*{crmScript ext=org.namelessnetwork.smallgrouptracking file=membertracking.js}*}

<!-- include file="CRM/Smallgrouptracking/BlockUI.tpl" -->
{include file="CRM/Smallgrouptracking/BlockUI.tpl"}

<!--    
<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
-->

<!--
    Uses custom API SmallGroupTracking

HTML5 Custom Data Attributes (data-*) : http://html5doctor.com/html5-custom-data-attributes/
Note: Data attribute names which contain hyphens will be stripped of their hyphens and converted to CamelCase

data-cid - contact id
data-sglid - small group leader id
data-sginfo-leaderid - data for info section

Process
- leader is selected
- check for existing div
- Clone the small group info div if none exists
- update the data on it
- when switching leaders we can simply hide the div


-->

<!-- CREATE DISCIPLESHIP COMMUNITY -->

<!-- CREATE/DELETE SMALL GROUP GROUP
maybe have a crm dialog that pops up 
-->

<!-- NOW RUNS FROM A FORM
<div id="divSgCreateDelete">
    <form>
    <h1>Create/Delete "Small Group" Group</h1>
    <div>
        <label for='sgLeaderFirstname'>Firstname</label>
        <input id='sgLeaderFirstname'type="text"></input>
        <label for='sgLeaderFirstname'>Lastname</label>
        <input id='sgLeaderLastname'type="text"></input>
        <input type='autocomplete'></input>
    </div>
    <div>
        <h2>Create Fields</h2>
        <label for='sgDay'>Day</label>
        <input id='sgDay'type="text"></input>
        <label for='dcFirstname'>DC Pastor Firstname</label>
        <input id='dcFirstname'type="text"></input>
        <label for='dcLastname'>DC Pastor Lastname</label>
        <input id='dcLastname'type="text"></input>
    </div>
    <div style='height:35px;'>
        <a class='button crm-popup' id='bCreateSmallGroup' type='button' data-popup-type='form'>Create</a>  
        <a class='button' id='bDeleteSmallGroup' type='button' >Delete</a>
    </div>
    </form>
</div>
-->
<!--
<div class='ui-widget'>
    <label for='autoTest'>AutoComplete: </label>
    <input id='autoTest'>
</div>
-->

<!-- 
ASSIGN SETTINGS TO VAR SO WE CAN ACCESS FROM JAVASCRIPT 
We loop through the php var passed via smarty to assign
-->
<div id="settings-cell" style="display:none;">
    
    {literal}
        <script type="text/javascript">
        var sgt_settings = {
    {/literal}
    {foreach from=$settings key=key item=val}
        '{$key}'{literal}:{/literal}{literal}'{/literal}{$val|string_format:"%d"}{literal}',{/literal}
    {/foreach}
    {literal}
        };
        </script>
    {/literal}

</div>

<div class="browserNotSupported" style='display:none;'>
    <h1 class='warning'>Attention! We're sorry, but your web browser is not supported with this application.</h1>
</div>
<div>
    <div id='divHidden' style='display:none'>

        <!-- DIALOG POPUPS -->
        <div id="dialog-confirm" class='removeContact' title="Are You Sure?">
            <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Are you sure you want to do this?</p>
        </div>

         <div id="dialog-confirm" class='deleteMeeting' title="Are You Sure?">
            <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Are you sure you want to do delete this meeting entry?</p>
        </div>
    </div>

    <div id="pageTop" style="display:block; display:relative;">
        <h3 id="subTitleDC" class="calloutRow">Discipleship Community</h3>
        <span class="topButtons" style=''>
            {if $editPermission == true}
                <div id="adminMenu">Admin Controls</div>
                <a class='button crm-popup' id='bCreateDcGroup' type='button' data-popup-type='form'>Create DC Group</a>  
                <a class='button crm-popup' id='bCreateSmallGroup' type='button' data-popup-type='form'>Create Small Group</a>  
                <a class='button crm-popup' id='bChangeSmallGroupLeader' type='button' data-popup-type='form'>Change Small Group Leader</a> 
                <a class='button crm-popup' id='bChangeSmallGroupDC' type='button' data-popup-type='form'>Change Small Group DC</a>
                <a class='button crm-popup' id='bDeleteDcGroup' type='button' data-popup-type='form'>Delete DC Group</a>
                <a class='button crm-popup' id='bDeleteSmallGroup' type='button' data-popup-type='form'>Delete Small Group</a>
                <!-- CONFIG MEMBERS TAB VIEW -->
                <a class='button crm-popup' id='bMembersSettings' type='button' data-popup-type='form' style="">Configure Members Columns</a>   
            {/if}
        </span>
    </div>
    <div id='divMain'>
        <div id='divLeftColumn'>
            <div class="DCselector">
                <div id="dcwrapper">
                    <select id="thisDC" name="field_selectDC">
                        <option />
                    </select>
                </div>
                <div id="smallgroupwrapper">
                    <select id="smallgroupSelect">
                        <option />
                    </select>
                </div>
            </div>
            <div class='section smallGroupSummary-section'>
                {*include file="CRM/Smallgrouptracking/GroupSummary.tpl"*}
                <div id='divMemberStats'>
                    <h3>Group Member Stats</h3>
                    <table>
                        <thead class='sticky'>
                            <tr>
                                <th>Regular</th><th>Irregular</th><th>Visitor</th><th>Inactive</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr id='summary-stats'>
                                <td id='stat-regularMembers'></td><td id='stat-irregularMembers'></td><td id='stat-visitors'></td><td id='stat-inactive'></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end left column -->
        <!-- right 2 columns -->
        <div id='divContent'>


            <div id='divMiddleColumn'>
                <!--
                <div id="divFirstLoad" class="section"> 
                    Select a Discipleship Community Leader and then a Small Group Leader.
                </div>
                -->

                <!-- DC SUMMARY TAB -->
                <div id="divDcSummary" class='content' >
                    {if $editPermission == true}
                    <!-- include file="CRM/Smallgrouptracking/GroupSummary.tpl" -->
                    {include file="CRM/Smallgrouptracking/Dcsummary.tpl"}
                    {/if}
                </div>

                <div id='divInfoSection'>
                    <!-- append sg info to this -->
                </div>
                <!-- GROUP INFORMATION TEMPLATE-->
                <!-- data-sginfo-leaderid = small group leaders id -->
                <div id='divSGInfoTemplate' class='section smallGroupTabs-section' data-sginfo-sglid="" style='display:none;'>

                    <div id="tabs" style=''>
                        <!-- LOADING BLOCK -->
                        <div id="blockUI-Control" style="display:none">
                            <div class="blockUI" style="display:none"></div>

                            <div class="blockUI blockOverlay" style="z-index: 1000; border: none; margin: 0px; padding: 0px; width: 100%; height: 100%; top: 0px; left: 0px; opacity: 0.6; cursor: wait; position: absolute; background-color: rgb(0, 0, 0);">

                            </div>
                            <div class="blockUI blockMsg blockElement" style="z-index: 1011; display: none; position: absolute; left: 387px; top: 52px;"></div>
                            <!--<div class='blockUI closeBlockUI' style="z-index: 1011; position: absolute; width:100%; text-align:center; top: 52px;">Close Loading Overlay</div>-->
                        </div>
                        <!-- SETUP TABS -->
                        <ul style='height:29px;'>
                            {*{if $editPermission == true}*}
                            <li><a href="#tabs-attendance">Attendance</a></li>
                            {*{/if}*}
                            <li><a href="#tabs-attendanceHistory">Attendance History</a></li>
                            <li><a href="#tabs-members">Members</a></li>
                            {literal}
                            <script>
                                // WE ONLY WANT TO SHOW THIS FOR CLEAR RIVER
                                CRM.$(document).ready(function ($) {
                                    var hostURL = window.location.host;
                                    if(hostURL.indexOf('clearriver') == 0) {
                                        $('.childcareTab').show();
                                    }
                                });
                            </script>
                            {/literal}
                            <li class="childcareTab" style="display:none;"><a href="#tabs-childcare">Childcare Reimbursement</a></li>
                            {if $editPermission == true}
                            <li class="tabs-reports"><a href="#tabs-reports">Reports</a></li>
                            {/if}
                        </ul>
                        <!-- MEMBERS TAB -->
                        <div id="tabs-members" class='content tabs-members' >
                            <div>
                                <!-- include file="CRM/Smallgrouptracking/Members.tpl" -->
                                {include file="CRM/Smallgrouptracking/Members.tpl"}
                            </div>
                        </div>
                        <!-- ATTENDANCE ENTRY TAB -->
                        <div id="tabs-attendance" class='content' >
                            {* if $editPermission == true *}
                            {include file="CRM/Smallgrouptracking/AttendanceEntry.tpl"}
                            {* /if *}
                        </div>
                        <!-- ATTENDANCE HISTORY -->
                        <div id="tabs-attendanceHistory" class='content tabs-attendanceHistory' >
                            <!-- include file="CRM/Smallgrouptracking/AttendanceHistory.tpl" -->
                            {include file="CRM/Smallgrouptracking/AttendanceHistory.tpl"}
                        </div>
                        <!-- CHILDCARE -->
                        <div id="tabs-childcare" class='content tabs-childcare' >
                            <!-- include file="CRM/Smallgrouptracking/AttendanceHistory.tpl" -->
                            {include file="CRM/Smallgrouptracking/Childcare.tpl"}
                        </div>
                        <!-- REPORTS -->
                        {if $editPermission == true}
                        <div id="tabs-reports" class='content tabs-reports' >
                            <!-- include file="CRM/Smallgrouptracking/Reports.tpl" -->
                            {*{include file="CRM/Smallgrouptracking/Reports.tpl"}*}
                        </div>
                        {/if}
                    </div> 
                    <div class="clear"></div>
                </div>
            </div> 
            {if $editPermission == true}
                <div id="div-report" class='content section-report' >
                    <!-- include file="CRM/Smallgrouptracking/Reports.tpl" -->
                    {include file="CRM/Smallgrouptracking/Reports.tpl"}
                </div>
            {/if}
                    <!-- end middle column -->
            <!-- ############ TEMPLATE EXAMPLES ############ -->
            <!--
            <div>
                <a class="button_name button" href="#">
             <span>
              <div class="icon icon_name-icon"></div>
              Button Text
             </span>
                </a>
             <div class="clear"></div>   
            </div>
            -->
            {*
            <div class="crm-accordion-wrapper crm-accordion_title-accordion crm-accordion-closed">
            <div class="crm-accordion-header">
            <div class="icon crm-accordion-pointer"></div>
            Group Leaders DATA
            </div><!-- /.crm-accordion-header -->
            <div class="crm-accordion-body">
            accordion body
            </div><!-- /.crm-accordion-body -->
            <div class="clear"></div>
            </div><!-- /.crm-accordion-wrapper -->
            *}
            {*
            <p>Date: <input type="text" id="datepicker"></p>
            <div class="clear"></div>

            <h3>This new page is generated by CRM/Smallgrouptracking/Page/Smallgrouptracking.php</h3>
            {$groupLeaders}
            <p>{$error}</p>

            <p>The current time is {$currentTime}</p>


            <p>{ts 1=$currentTime}(In your native language) The current time is %1.{/ts}</p>

            <!-- ############ END TEMPLATE EXAMPLES ############ -->
            *}

        </div> <!-- right 2 column divContent -->
        <div id='divRightColumn'>

        </div>
    </div> <!-- divMain -->
</div> <!-- sgtApp -->
