<!-- Add new contact to small group -->
<div id='divAddContact' class="section">
    <div id='divCreateSmallGroupContact' class='refCreate'> 
        <h3>Add Contact to this Small Group</h3>
        <input class='select2' name="field_addSmallGroupContact" placeholder="{ts}- select contact -{/ts}" />
        <span>Role:
            <select id='addContactRole' type='select' name='role' data-cid='" + value.id + "'>" +
                <option value='Regular'>Regular</option>
                <option value='Irregular'>Irregular</option>
                <option value='Visitor'>Visitor</option>
                <option value='Inactive'>Inactive</option>
            </select>
        </span>
        <span><a class='button' id='bAddSmallGroupContact' type='button'>Add</a></span>
    </div>
</div>

<h1 class="hEditAttendance" style="">EDITING ATTENDANCE</h1>
<h1 class="hAddAttendance" style="padding-bottom:10px;">SUBMIT ATTENDANCE</h1>
<div id="divAttendanceEntryLeft">
    <div class="section">
        <h3>Meeting Details</h3>
        <span>Date: <input type="text" id="datepicker"></span>
        <span>Type:
            <select id='meetingType' name="smallgroup">
                <option value="Discussion">Discussion</option>
                <option value="Party">Party</option> 
                <option value="DC">DC</option>
                <option value="No Meeting">No Meeting</option>
            </select>
        </span>
    </div>

    <!-- ATTENDANCE TABLE (REGULAR) -->   
    <div class="section">
        <span><h3>Regular Attenders</h3></span>
        <div id="tableAttendance" class='selector tableRoot'>
            <div class='tableHeader'>
                <div>
                    <span>Present</span>
                    <span>Name</span>
                    <span>Role</span>
                    <span>Notes</span>
                    <span></span>
                </div>   
            </div>
            <div id='attendanceRow' class='tableBody'>
                <!-- Append Here -->
            </div>
        </div>
    </div>
    <!-- ATTENDANCE TABLE (VISITOR) -->        
    <div class="section">
        <span><h3>Visitors</h3></span>
        <div id="tableAttendanceVisitor" class='selector tableRoot'>
            <div class='tableHeader'>
                <div>
                    <span>Present</span>
                    <span>Name</span>
                    <span>Role</span>
                    <span>Notes</span>
                    <span></span>
                </div>   
            </div>
            <div id='attendanceRowVisitor' class='tableBody'>
                <!-- Append Here -->
            </div>
        </div>
    </div>
    <!-- ATTENDANCE TABLE (VISITOR) -->  
    <div class="section">
        <span><h3>Irregular Attenders</h3></span>
        <div id="tableAttendanceIrregular" class='selector tableRoot'>
            <div class='tableHeader'>
                <div>
                    <span>Present</span>
                    <span>Name</span>
                    <span>Role</span>
                    <span>Notes</span>
                    <span></span>
                </div>   
            </div>
            <div id='attendanceRowIrregular' class='tableBody'>
                <!-- Append Here -->
            </div>
        </div>
    </div>
    <!-- ATTENDANCE TABLE (VISITOR) -->      
    <div class="section">
        <span><h3>Inactive</h3></span>
        <div id="tableAttendanceInactive" class='selector tableRoot'>
            <div class='tableHeader'>
                <div>
                    <span>Present</span>
                    <span>Name</span>
                    <span>Role</span>
                    <span>Notes</span>
                    <span></span>
                </div>   
            </div>
            <div id='attendanceRowInactive' class='tableBody'>
                <!-- Append Here -->
            </div>
        </div>
    </div>
    <!--
    <div id='divAddContacts'>
        <h2>Add Contact to Small Group</h2>
        <div id='divCreateSmallGroupContact' class='refCreate'> 
            <input name="field_smallGroupContact" placeholder="{ts}- select contact -{/ts}" />
    
        </div>
    </div>
    <a class='button' id='bAddAnotherContact' type='button'>+</a>
    <a class='button' id='bRemoveAnotherContact' type='button'>-</a>
    <a class='button' id='bAddSmallGroupContact' type='button'>Add Contacts to Group</a>
    -->
    <span>
        <div id='divCancelEditAttendance' class="section">
            <a class='button' id='bCancelEditAttendance' type='button'>Cancel Edit</a>
        </div>
    </span>
    <span>
        <div id='divUpdateAttendance' class="section">
            <a class='button' id='bUpdateAttendance' type='button'>Update Attendance</a>
        </div>
    </span>
    <div id='divSubmitAttendance' class="section">
        <a class='button' id='bAddAttendance' type='button'>Submit Attendance</a>
    </div>



</div>

<div id="divAttendanceEntryRight">

    <h3>Previous Logs</h3>
    <div class="page_navigation"></div>
    <div class="logList">
        <ul id="ulPrevGroupLogs" class="content">
        </ul>
    </div>

</div>

<!-- CLEAR FLOAT SO BORDER WRAPS AROUND EVERYTHING -->
<div style="clear:left;"></div>