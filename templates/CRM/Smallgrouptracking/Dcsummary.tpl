<div id='divAccordionDcSummary' class="crm-accordion-wrapper crm-accordion_title-accordion crm-accordion-closed collapsed">
    <div class="crm-accordion-header">
        <div class=""></div>
        DC ATTENDANCE SUMMARY

    </div><!-- /.crm-accordion-header -->
    <div id="divAccordion" style="max-width:1000px;" class="crm-accordion-body">

        <div class='subheader'>
            <span>Start Date: <input type="text" id="dcSummaryStartDate"></input></span>
            <span>End Date: <input type="text" id="dcSummaryEndDate"></input></span>
            <span><button id='bRefreshDcSummaryTable' style='font-size:0.75em;'>Refresh</button></span>
        </div>
        <!-- LOADING BLOCK UI -->
        <div id="blockUI-Control-DCSUMMARY" style="display:none">
            <div class="blockUI" style="display:none"></div>

            <div class="blockUI blockOverlay" style="z-index: 1000; border: none; margin: 0px; padding: 0px; width: 100%; height: 100%; left: 0px; opacity: 0.6; cursor: wait; position: absolute; background-color: rgb(0, 0, 0);">

            </div>
            <div class="blockUI blockMsg blockElement" style="z-index: 1011; display: none; position: absolute; left: 387px; top: 52px;"></div>
            <!--<div class='blockUI closeBlockUI' style="z-index: 1011; position: absolute; width:100%; text-align:center; top: 52px;">Close Loading Overlay</div>-->
        </div>
        <div id="divDcSummaryTable" class="padding-all">

            <table id="dcSummaryTableTemplate" class='selector' style="vertical-align:top;width:auto;">
                <thead class=''>
                </thead>
                <tbody id='dcSummaryBody'>

                    <!-- Append Here -->
                </tbody>
            </table>
        </div>
        <div class=" padding-all">
            <table id="dcSummaryTableTotals" class='selector' style="vertical-align:top;width:auto;">
                <thead class=''>
                </thead>
                <tbody id='dcSummaryTotalsBody'>
                </tbody>
            </table>
        </div>
    </div><!-- /.crm-accordion-body -->
    <div class="clear"></div>
</div><!-- /.crm-accordion-wrapper -->
