
{*{crmStyle ext=org.namelessnetwork.smallgrouptracking file=css/reports.css}*}
<div id="ReportApp">
    <div id="divDateRange">
        <table class="dateRange">
            <tr>
                <th class="tg-4272" colspan="3">Date Range</th>
            </tr>
            <tr>
                <td class="tg-start" style="width:100px"><span>Start: <input type="text" id="startDate" ng-input="start"></input></span></td>
                <td class="tg-end" style="width:100px"><span>End: <input type="text" id="endDate" ng-input="end"></input></span></td>
                <td class="tg-submit" ><a class='button' id='bGetHistory' type='button'>Get Report</a></td>
            </tr>
        </table>
    </div>
    
    <div id="reportFilter">
        <label>Filter Results</label>
        <input id="search" type="text" />
    </div>
    
    <div id="divMeetingKey">  
        <table class="tg">
            <tr>
                <th class="tg-4272" colspan="5">Meeting Type Key</th>
            </tr>
            <tr>
                <td class="tg-vkov"><input id='meeting_discussion' type="checkbox" style='display:inline-block;' >Discussion</td>
                <td class="tg-lcoa"><input id='meeting_DC' type="checkbox" style='display:inline-block;' >DC</td>
                <td class="tg-bmek"><input id='meeting_Party' type="checkbox" style='display:inline-block;' >Party</td>
                <td class="tg-bmef"><input id='meeting_none' type="checkbox" style='display:inline-block;' >No Meeting</td>
                <td class="meeting_type_missing"><input id='meeting_missing' type="checkbox" style='display:inline-block;' >No Record</td>
            </tr>

        </table>
    </div>

    <div id="divReport">
        {literal}
        <table id="tableReport" class='selector display report-section'>
            <thead class='week_dates'>
                <tr>
                    <th colspan="1"  style='min-width:200px'>Week Of</th>
                    <!-- WEEK DATE HEADER -->
                </tr>
            </thead>
            <tbody>
                <tr class="meeting_row">
                    <!-- DC PASTOR -->
                    <td class='dc_pastor'></td>
                    <!-- GROUP LEADER -->
                    <td class="group_leader"></td>
                    <!-- PRESENT COUNT -->
                    <td class="attendance"></td>
                    <!--<th>{{member.present_count}}</th>-->
                </tr>
            </tbody>
            <tfoot>
                <tr class='week_attendance_total_row'>
                    <td colspan='1'>Week Attendance Total</td>
                    <!--week_attendance_total-->
                </tr>
            </tfoot>
        </table>
        {/literal}
        <!-- DC TABLE SECTIONS -->
        <div id='Monday' class='report-section'>
        </div>
        <div id='Tuesday' class='report-section'>
        </div>
        <div id='Wednesday' class='report-section'>
        </div>
        <div id='Thursday' class='report-section'>
        </div>
        <div id='Friday' class='report-section'>
        </div>
        <div id='Saturday' class='report-section'>
        </div>
        <div id='Sunday' class='report-section'>
        </div>
    </div>
</div>
