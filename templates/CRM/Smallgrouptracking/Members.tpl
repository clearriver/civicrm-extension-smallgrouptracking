<!-- CODE FOR THIS IS IN Smallgrouptracking.js in the LoadContacts() -->

<!-- UPCOMING EVENTS -->
<div id="upcoming-events"> 
    <span class="label">UPCOMING EVENTS</span>
    <div class="template">
        <div class="title"></div>
        <div class="date"></div>
    </div>
</div>
<!-- TABLE FILTER INPUT -->
<div class="input-filter-container"><label for="input-filter">Filter the table:</label> <input type="search" id="member-input-filter" size="15" placeholder="search"></div>
<table id="memberList" class='selector'>
    <col style="width:auto">
    <col style="width:auto">
    <col style="width:auto">
    <col style="width:auto">
    <col style="width:auto">
    <col style="width:auto">
    <col style="width:auto">
    <col style="width:auto">
    <col style="width:auto">
    <col style="width:auto">
    <col style="width:auto">
    <col style="width:auto">
    <col style="width:auto">
    <col style="width:auto">
    <col style="width:auto">
    <thead class='theadMemberList'>
        <tr>
            <th class='snippetIcon' scope='col'><div class=''></div></th>
            <th class='thMemberName' scope='col'><div class=''>Name</div></th>
            <th scope='col'><div>Label</div></th>
            <!-- Start of generated columns -->
            {foreach from=$customHeaders item='header'}
                <th scope='col' data-field="{$header[1]}" data-generated="true"><div>{$header[2]}</div></th>
            {/foreach}
            <!-- End of generated columns -->
            <th class='serving_info' scope='col' data-show="{$showServingArea}" {if $showServingArea eq 'false'}style="display:none"{/if}><div>Serving Area</div></th>
            <!--<th class='event_reg_info' scope='col'><img src='/sites/all/modules/civicrm/bower_components/ckeditor/skins/kama/images/spinner.gif' class='spinner'></img><div class='header'>...Loading Event Registrations</div></th>-->
            <th class='event_reg_info' scope='col'><div class='header'>Registered Events</div></th>
        </tr>
    </thead>
<tbody id='memberListBody'>
    <tr>
        <td></td>
    </tr>
    <!-- Append Here -->
</tbody>
<!-- SMARTY API CALL TEST -->
</table>

<!-- ANGULAR TEST -->

<div  ng-controller="MemberInfoController">
    <!--
    <table id="memberList2" class='selector' style="vertical-align:top;width:auto;">
        <col style="width:auto">
        <col style="width:100px">
        <col style="width:100px">
        <col style="width:100px">
        <col style="width:100px">
        <thead class=''>
            <tr>
                <th scope='col'><div class=''>Name</div></th><th scope='col'><div>Label</div></th><th scope='col'><div>Beliefs</div></th><th scope='col'><div>Freedom</div></th><th scope='col'><div>Serving</div></th><th scope='col'><div>Community</div></th>
        </tr>
        </thead>
        
        <tbody id='memberListBody2'>
            <tr ng-repeat="person in people">
                <td>[[person.name]]</td><td>[[person.label]]</td><td>[[person.freedom]]</td><td>[[person.truth]]</td><td>[[person.service]]</td><td>[[person.community]]</td>
            </tr>

        </tbody>

    </table>
    <button ng-click="loadMembers(38)">Click</button>
    -->
</div>