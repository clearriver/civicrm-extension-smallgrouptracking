<div id="AttendanceHistoryApp" ng-app="SmallgroupTracking" ng-controller="AttendanceHistory">
    <div id="divDateRange">
        <table class="dateRange">
            <tr>
                <th class="tg-4272" colspan="3">Date Range</th>
            </tr>
            <tr>
                <td class="tg-start" style="width:100px"><span>Start: <input type="text" id="startDate" ng-input="start"></input></span></td>
                <td class="tg-end" style="width:100px"><span>End: <input type="text" id="endDate" ng-input="end"></input></span></td>
                <td class="tg-submit" ><a class='button' id='bGetHistory' type='button' ng-href="#here" ng-click="execute()">Get History</a></td>
            </tr>
        </table>
    </div>
    
    <div id="attendanceFilter">
        <label>Filter by Name</label>
        <input id="search" type="text" ng-model="search.name" />
    </div>
    
    <div id="divMeetingKey">  
        <table class="tg">
            <tr>
                <th class="tg-4272" colspan="4">Meeting Type Key</th>
            </tr>
            <tr>
                <td class="tg-vkov"><input id='meeting_discussion' type="checkbox" style='display:inline-block;' >Discussion</td>
                <td class="tg-lcoa"><input id='meeting_DC' type="checkbox" style='display:inline-block;' >DC</td>
                <td class="tg-bmek"><input id='meeting_Party' type="checkbox" style='display:inline-block;' >Party</td>
                <td class="tg-bmef"><input id='meeting_none' type="checkbox" style='display:inline-block;' >No Meeting</td>
            </tr>

        </table>
    </div>

    <div id="divAttendanceHistory">
        {literal}
        <table id="tableHistorySource" class='selector display'>
            <thead>
                <tr>
                    <th><a ng-click="sort('name')">Name</a></th>
                    <th ng-repeat="header in headers" ng-class="header.meeting_type"><a ng-click="sort(header.simple_date)">{{header.meeting_date}}</a></th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="member in attendeesSet = (attendees | filter:search | orderBy:predicate:reverse)">
                    <td>{{member.name}}</td>
                    <td ng-repeat="header in headers" ng-class="presentClass(member[header.simple_date])">{{presentChecker(member[header.simple_date])}}</td>
                    <th>{{member.present_count}}</th>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td>TOTALS:</td>
                    <td ng-repeat="header in headers">{{dateTotal(header.simple_date)}}</td>
                </tr>
            </tfoot>
        </table>
        {/literal}
    </div>
</div>
