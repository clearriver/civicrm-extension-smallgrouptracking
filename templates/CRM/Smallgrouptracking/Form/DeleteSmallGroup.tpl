{* HEADER *}

<div class="crm-submit-buttons">
{include file="CRM/common/formButtons.tpl" location="top"}
</div>

{* FIELD EXAMPLE: OPTION 1 (AUTOMATIC LAYOUT) *}

{foreach from=$elementNames item=elementName}
  <div class="crm-section" style=''>
    <div class="label">{$form.$elementName.label}</div>
    <div class="content">{$form.$elementName.html}</div>
    <div class="clear"></div>
  </div>
{/foreach}

{*<input id='field_sg_leader' style='display:none;' name="field_sg_leader" placeholder="{ts}- select organization -{/ts}" />*}

{* FIELD EXAMPLE: OPTION 2 (MANUAL LAYOUT)

  <div>
    <span>{$form.favorite_color.label}</span>
    <span>{$form.favorite_color.html}</span>
  </div>

{* FOOTER *}
<div class="crm-submit-buttons">
{include file="CRM/common/formButtons.tpl" location="bottom"}
</div>

{literal}
    <script type="text/javascript">
        CRM.$(function ($) {
      
   
          //console.log($("#field_dc_group").val);
      
          /* DC SELECTION CHANGE */
          $("#field_dc_group").change(function() {
            $('#field_sg_leader').select2('data', {id: null, text: null}); // clear currently selected group
              //console.log("DC Group ID: " + $("#field_dc_group").val());
            $('[name=field_sg_group]').crmEntityRef({
                entity: 'group',
                api: {params: {parents: $("#field_dc_group").val()}},
                create: true
            });
        
          });
          $( document ).ready(function() {
            $('#field_sg_group').hide();
          });
      });
    </script>
{/literal}