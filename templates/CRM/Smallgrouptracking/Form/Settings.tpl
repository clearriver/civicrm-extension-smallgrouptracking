{* HEADER *}

{crmStyle ext=org.namelessnetwork.smallgrouptracking file=css/Settings.css}

<div class="description">
    Choose the columns that will be displayed on the Small Group Members tab.
</div>
<div class="description clarification">
    Columns are base on Custom Groups and Fields that have been defined by the admin.
</div>

<!--<div class="crm-submit-buttons">
{include file="CRM/common/formButtons.tpl" location="top"}
</div>-->

<table data-custom-fields="{$customFields}">
    <thead>
        <tr>
            {foreach from=$elementNames item=elementName}
            <td>
                <div class="label">{$form.$elementName.label}</div>
            </td>
            {/foreach}
        </tr>
    </thead>
    <tbody>
        <tr>
            {foreach from=$elementNames item=elementName}
            <td>
                <div class="crm-section-null">
                    <div class="content">{$form.$elementName.html}</div>
                </div>
            </td>
            {/foreach}
        </tr>
    </tbody>
</table>
        
        <label><input 
                id="showServingArea" 
                name="showServingArea" 
                type="checkbox" 
                value="checked"
                {$showServingArea}>Show Serving Area</label>

<div class="crm-submit-buttons">
{include file="CRM/common/formButtons.tpl" location="bottom"}
</div>
