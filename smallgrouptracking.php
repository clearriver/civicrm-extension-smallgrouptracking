<?php

require_once 'smallgrouptracking.civix.php';
//require_once('/CustomPHP/FirePHPCore/fb.php');
require_once('api/v3/SmallGroupTracking/Manageextension.php');

/**
 * Management functions (install/uninstall/enable/disable) are created
 * in an API manageextension
 * 
 * 
 */

/**
 * Implementation of hook_civicrm_config
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function smallgrouptracking_civicrm_config(&$config) {
    _smallgrouptracking_civix_civicrm_config($config);
}

/**
 * Implementation of hook_civicrm_xmlMenu
 *
 * @param $files array(string)
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_xmlMenu
 */
function smallgrouptracking_civicrm_xmlMenu(&$files) {
    _smallgrouptracking_civix_civicrm_xmlMenu($files);
}

/**
 * Implementation of hook_civicrm_install
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function smallgrouptracking_civicrm_install() {
    _SmallgrouptrackingInstall();
    return _smallgrouptracking_civix_civicrm_install();
}

/**
 * Implementation of hook_civicrm_uninstall
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_uninstall
 */
function smallgrouptracking_civicrm_uninstall() {
    _SmallgrouptrackingUninstall();
    return _smallgrouptracking_civix_civicrm_uninstall();
}

/**
 * Implementation of hook_civicrm_enable
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function smallgrouptracking_civicrm_enable() {
    _SmallgrouptrackingEnable();
    return _smallgrouptracking_civix_civicrm_enable();
}

/**
 * Implementation of hook_civicrm_disable
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_disable
 */
function smallgrouptracking_civicrm_disable() {
    _SmallgrouptrackingDisable();
    return _smallgrouptracking_civix_civicrm_disable();
}

/**
 * Implementation of hook_civicrm_upgrade
 *
 * @param $op string, the type of operation being performed; 'check' or 'enqueue'
 * @param $queue CRM_Queue_Queue, (for 'enqueue') the modifiable list of pending up upgrade tasks
 *
 * @return mixed  based on op. for 'check', returns array(boolean) (TRUE if upgrades are pending)
 *                for 'enqueue', returns void
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_upgrade
 */
function smallgrouptracking_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
    return _smallgrouptracking_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implementation of hook_civicrm_managed
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_managed
 */
function smallgrouptracking_civicrm_managed(&$entities) {
    /**
     * Manage root group 
     */
    /*
      $entities[] = array(
      'module' => 'org.namelessnetwork.smallgrouptracking',
      'name' => 'Discipleship_Community',
      'entity' => 'group',
      'params' => array(
      'version' => 3,
      'title' => 'Discipleship Community',
      'description' => 'Root group for small group tracking',
      ),
      );

      return _smallgrouptracking_civix_civicrm_managed($entities);
     * 
     */
}

/**
 * Implementation of hook_civicrm_caseTypes
 *
 * Generate a list of case-types
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function smallgrouptracking_civicrm_caseTypes(&$caseTypes) {
    _smallgrouptracking_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implementation of hook_civicrm_alterSettingsFolders
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_alterSettingsFolders
 */
function smallgrouptracking_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
    _smallgrouptracking_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

// ADDS PERMISSIONS 
function smallgrouptracking_civicrm_permission(&$permissions) {
    //$prefix = ts('CiviCRM SmallGroup Tracking', array('domain' => 'org.namelessnetwork.smallgrouptracking')) . ': ';
    //$prefix = ts('CiviCRM SmallGroup Tracking') . ': '; // name of extension or module
    
    // NB: note the convention of using delete in ComponentName, plural for edits
    
    // VOL-71: Until the Joomla/Civi integration is fixed, don't declare new perms
    // for Joomla installs
    if (CRM_Core_Config::singleton()->userPermissionClass->isModulePermissionSupported()) {
        $permissions = array_merge($permissions, CRM_Smallgrouptracking_Permission::getSmallgrouptrackingPermissions());
        
        //$permissions = array_merge($permissions, $permissions['access SmallGroupTracking'] = $prefix . ts('access smallgrouptracking'));
        //$permissions['edit SmallGroupTracking'] = $prefix . ts('edit smallgrouptracking');
    }
}

// ALTER PERMISSIONS for API 
function smallgrouptracking_civicrm_alterAPIPermissions($entity, $action, &$params, &$permissions) {
    $permissions['small_group_tracking']['addsmallgroupattendance'] = array('access SmallGroupTracking');
    $permissions['small_group_tracking']['createsmallgroup'] = array('edit SmallGroupTracking');
    $permissions['small_group_tracking']['createsmallgroupcontact'] = array('access SmallGroupTracking');
    $permissions['small_group_tracking']['createsmallgroupdc'] = array('edit SmallGroupTracking');
    $permissions['small_group_tracking']['deletedcgroup'] = array('edit SmallGroupTracking');
    $permissions['small_group_tracking']['deletemeeting'] = array('access SmallGroupTracking');
    $permissions['small_group_tracking']['deletesmallgroup'] = array('edit SmallGroupTracking');
    $permissions['small_group_tracking']['getattendancedetails'] = array('access SmallGroupTracking');
    $permissions['small_group_tracking']['getcustomvaluetables'] = array('access SmallGroupTracking');
    $permissions['small_group_tracking']['getlogsmallgroupmeeting'] = array('access SmallGroupTracking');
    $permissions['small_group_tracking']['getsmallgroupattendance'] = array('access SmallGroupTracking');
    $permissions['small_group_tracking']['getsmallgroupcontacts'] = array('access SmallGroupTracking');
    $permissions['small_group_tracking']['getsmallgroupgroups'] = array('access SmallGroupTracking');
    $permissions['small_group_tracking']['getsmallgroupleaders'] = array('access SmallGroupTracking');
    $permissions['small_group_tracking']['logsmallgroupmeeting'] = array('access SmallGroupTracking');
    $permissions['small_group_tracking']['removesmallgroupcontact'] = array('access SmallGroupTracking');
    $permissions['small_group_tracking']['getdaydatesinrange'] = array('access SmallGroupTracking');
    $permissions['small_group_tracking']['geteventregistrations'] = array('access SmallGroupTracking');
    $permissions['small_group_tracking']['getreport'] = array('edit SmallGroupTracking');
    $permissions['custom_value']['create'] = array('access SmallGroupTracking');
    $permissions['activity']['create'] = array('access SmallGroupTracking');
}

/**
 *
 * Adds a navigation menu item under report.
 *
 */
function smallgrouptracking_civicrm_navigationMenu(&$params) {
    // get the id of Administer Menu
    $menuId = CRM_Core_DAO::getFieldValue('CRM_Core_BAO_Navigation', 'Contacts', 'id', 'name');

    // skip adding menu if there is no administer menu
    if ($menuId) {
        // get the maximum key under adminster menu
        $maxKey = max(array_keys($params[$menuId]['child']));
        $params[$menuId]['child'][$maxKey + 1] = array(
            'attributes' => array(
                'label' => 'Small Group Tracking',
                'name' => 'SmallGroupTracking',
                'url' => 'civicrm/smallgrouptracking',
                'permission' => 'access SmallGroupTracking',
                'operator' => NULL,
                'separator' => TRUE,
                'parentID' => $menuId,
                'navID' => $maxKey + 1,
                'active' => 1
            )
        );
    }
}

/**
 * See if a CiviCRM custom field group exists
 *
 * @param string $group_name
 *   custom field group name to look for, corresponds to field civicrm_custom_group.name
 * @return integer
 *   custom field group id if it exists, else zero
 */
function custom_group_get_id($group_name) {
    $result = 0;

    // This is an empty array we pass in to make the retrieve() function happy
    $def = array();
    $params = array(
        'name' => $group_name,
    );

    require_once('CRM/Core/BAO/CustomGroup.php');

    $custom_group = CRM_Core_BAO_CustomGroup::retrieve($params, $def);
    // fb($custom_group);
    if (!empty($custom_group)) {
        $result = $custom_group->id;
    } else
        // fb('custom group not found');
    return $result;
}

/**
 * See if a CiviCRM custom field exists
 *
 * @param integer $custom_group_id
 *   custom group id that the field is expected to belong to
 * @param string $field_label
 *   custom field name to look for, corresponds to field civicrm_custom_field.label
 * @return integer
 *   custom field id if it exists, else zero
 */
function custom_field_get_id($custom_group_id, $field_label) {
    $result = 0;

    // This is an empty array we pass in to make the retrieve() function happy
    $def = array();
    $params = array(
        'custom_group_id' => $custom_group_id,
        'label' => $field_label,
    );

    require_once('CRM/Core/BAO/CustomField.php');

    $custom_field = CRM_Core_BAO_CustomField::retrieve($params, $def);

    if (!empty($custom_field)) {
        $result = $custom_field->id;
    }
    return $result;
}

/**
 * Get a GROUP id by passing in the group name.
 * 
 * The group name has "_" for spaces
 * @param type $group_name
 * @return type
 */
function group_get_id($group_name) {
    $result = 0;

    // This is an empty array we pass in to make the retrieve() function happy
    $def = array();
    $params = array(
        'title' => $group_name,
    );

    require_once('CRM/Contact/BAO/Group.php');

    $group = CRM_Contact_BAO_Group::retrieve($params, $def);

    if (!empty($group)) {
        $result = $group->id;
    }
    return $result;
}

/**
 * Get a specific setting from civicrm_settings
 * @param type $group_name
 * @param type $name
 * Setting name
 * @return type
 * object with the settings
 * 
 */
if (!function_exists('GetCiviSetting')) {

    function GetCiviSetting($group_name, $name) {
        $setting = CRM_Core_BAO_Setting::getItem($group_name, $name);
        //// fb($setting, 'Setting for ' . $group_name . ">" . $name);
        return $setting;
    }

}
/**
 * Gets all the settings in civicrm_settings table for a given group_name
 * 
 * @param type $group_name
 * @return type
 * object with settings for the given group name
 * $settings['setting_name'];
 */
if (!function_exists('GetCiviSettingAll')) {

    function GetCiviSettingAll($group_name) {
        $settings = CRM_Core_BAO_Setting::getItem($group_name);
        //// fb($settings, 'Settings for ' . $group_name);
        return $settings;
    }

}
/**
 * Delete a setting from civicrm_setting
 * @param type $group_name
 * @param type $name
 * Name of the setting
 * @param type $value
 * id or value
 */
if (!function_exists('DeleteCiviSetting')) {

    function DeleteCiviSetting($group_name, $name, $value) {
        $addAnd = false; // let us know if we need an and in the query
        $query = "delete from civicrm_setting WHERE ";

        if ($group_name) {
            $query .= "group_name = '" . $group_name . "' ";
            $addAnd = true;
        }

        if ($name) {
            if ($addAnd) {
                $query .= "AND name = '" . $name . "' ";
            } else {
                $query .= "name = '" . $name . "' ";
                $addeAnd = true;
            }
        }

        if ($value) {
            if ($addAnd) {
                $query .= "AND value = '" . $value . "' ";
            } else {
                $query .= "value = '" . $value . "' ";
                $addeAnd = true;
            }
        }

        // Only delete for this domain ID
        $query .= "AND domain_id = " . CRM_Core_Config::domainID();

        //// fb($query, 'DeleteCiviSetting Query');
        CRM_Core_DAO::executeQuery($query);
    }

}

/**
 * Gets a list of all small groups under Discipleship Community
 * and the DC sub-group
 * @return array
 * List of IDs for ALL small group groups
 */
function GetSmallGroupList() {
    $smallGroupList = array();
    /*
     * Get All groups under DC root group
     * The returned child_group_id is the DC pastor group
     */
    $result = civicrm_api3('GroupNesting', 'get', array(
        'sequential' => 1,
        'parent_group_id' => GetCiviSetting('org.namelessnetwork.smallgrouptracking', 'discipleship_community_group'),
    ));
    
    /*
     * Loop through pastor groups and get the small groups under them
     */
    foreach ($result['values'] as $val) {
        /*
         * Get groups in this DC sub-group
         */
        //fb($val['child_group_id'],'DC Pastor Group ID');
        $leaderGroups = civicrm_api3('GroupNesting', 'get', array(
            'sequential' => 1,
            'parent_group_id' => $val['child_group_id'],
        ));
        // fb($leaderGroups,'leader groups');
        /*
         * Loop through groups within the dc sub-group
         * add push the group id to the array
         */
        foreach($leaderGroups['values'] as $group) {
            $smallGroupList[] = $group['child_group_id'];
        }
    }
    //fb($smallGroupList, 'small group group_id');
    return $smallGroupList;
}

/**
 * Add a setting to civicrm_settings table
 * @param type $group_name
 * @param type $name
 * @param type $value
 */
if (!function_exists('CreateCiviSetting')) {

    function CreateCiviSetting($group_name, $name, $value) {
        CRM_Core_BAO_Setting::setItem($value, $group_name, $name);
    }

}

function GetCustomGroupTable() {
    
}
