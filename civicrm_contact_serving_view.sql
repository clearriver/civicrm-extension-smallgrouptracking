CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `civicrm_contact_serving_view` AS
    select 
        `civicrm_contact`.`id` AS `id`,
        `civicrm_contact`.`display_name` AS `display_name`,
        `civicrm_contact`.`last_name` AS `last_name`,
        `civicrm_contact`.`first_name` AS `first_name`,
        GROUP_CONCAT(distinct civicrm_membership_type.name) AS membership_type
    from 
		((((((`civicrm_contact`
        left join `civicrm_membership` ON ((`civicrm_contact`.`id` = `civicrm_membership`.`contact_id`)))
        left join `civicrm_membership_type` ON ((`civicrm_membership`.`membership_type_id` = `civicrm_membership_type`.`id`)))
        )
        )
        )
        )
    where
        ((`civicrm_contact`.`first_name` is not null)
            and (`civicrm_contact`.`last_name` is not null)
            and (`civicrm_contact`.`contact_type` = 'Individual'))
    group by `civicrm_contact`.`last_name` , `civicrm_contact`.`first_name` 
	#GROUP BY civicrm_membership_type.name
