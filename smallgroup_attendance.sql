DROP TABLE IF EXISTS custom_smallgroup_attendance;
CREATE TABLE IF NOT EXISTS custom_smallgroup_attendance (
    sg_track_attendance_id INTEGER AUTO_INCREMENT,

    # start alternate key

    member_id INTEGER,
    activity_date DATE,

    # end alternate key

    group_group_id INTEGER,
    dc_leader_id INTEGER,
    group_leader_id INTEGER,
    activity_type VARCHAR(255),
    member_role VARCHAR(255),
    attended BIT DEFAULT 0,
    notes VARCHAR(255),
    PRIMARY KEY(sg_track_attendance_id),
    UNIQUE KEY(member_id, activity_date)
)