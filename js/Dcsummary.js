/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//(function($, _) {

// Gets a list of all the attendance entries for each group in the selected DC
/*
 * Called when DC is selected
 */
var dcTable;
var dcTableData = new Array();
var dcDataTable; //holds the datatable created
var dcTableDataRow = new Array(); // each row
var dcTableColumns = new Array();
var dateCount = 0;
var dcTotals = new Object(); 
var i = 0;

// passes in the group leaders and the total count of all leaders
function GetDcAttendanceEntries(group, length) { // group is just id
    CRM.$('#blockUI-Control-DCSUMMARY').show(); // loading 
   
    dcTotals = {}; // hold totals for each date, with the date as the key
    i = 0; // will use to check when done with each()
    var dates = InitDates();
    
    //dcTableData.push(group_leaderName);
    dcTableColumns[0] = "Leader";
    
    var meeting_dates = new Array(); // will hold valid meeting dates for range

    CRM.$(function ($) {
        ClearDCSummaryTable();
        
        // GET SMALL GROUP DAY
        var day = $('#subTitleDC').attr('data-day');
        
        // GET VALID DATES FOR EACH DAY IN THE RANGE
        CRM.api3('SmallGroupTracking', 'getdaydatesinrange', {
            "start_date": FormatDate(dates.first),
            "end_date": FormatDate(dates.last),
            "day": day
        }).done(function (result) {
                $.each(result['values'], function (key, val) {
                    meeting_dates.push(val.Date);
                    dcTotals[val.Date.substring(5, 10).replace("-", "/")]  = 0; // init value for dcTotals
                });
                //console.log("meeting_dates", meeting_dates);

                
                if(meeting_dates.length == 0) {
                    CRM.$('#blockUI-Control-DCSUMMARY').hide(); // loading 
                    return;
                }
                
                // Loop through each group leader and get Dates
                $.each(group, function (key, val) {
                    i++;
                    dcTableData.push(val.name); // add name first
                    //console.log("Name: " + val.name);
                    //CreateTableData(val.name, val.gid, dates, length);
                    //console.log(meeting_dates);
                    SetGroupData(val.name, val.gid, dates, length, i, meeting_dates);   
                });
           
        });
    });
}

// GET GROUP DATA
var sgData = new Object();
function SetGroupData(name, gid, dates, length, i, meeting_dates) {
    //console.log("SetGroupData");
    var groupData = new Object(); // reset
    sgData = new Object(); // reset

    // Call to Getlogsmallgroupmeeting passing sgg_id
    // returns list of meeting dates
    CRM.api3('SmallGroupTracking', 'getlogsmallgroupmeeting', {
        "sequential": 1,
        "sgg_id": gid,
        "start_date": FormatDate(dates.first),
        "end_date": FormatDate(dates.last)
    }).done(function (result) {
        if(result['values'].length > 0) {
            
            // assign each record to groupData with the date for the record as the key
            CRM.$.each(result["values"], function (key, val) {
                groupData[val["meeting_date"]] = val["present_count"];
            });
            //console.log('groupsData',gid,groupData);
            sgData[gid] = groupData; // add this groups data to object

            // FINISHED POPULATING DATA
            if (i == length) {
                //console.log('sgData', sgData, length);  

            }
            CreateTable(name, gid, meeting_dates, sgData, length);
        }
        else { // no results
            sgData[gid] = [];
            CreateTable(name, gid, meeting_dates, sgData, length);
        }
    });
}

// Called once for each contact in the small group
// populates attendance totals for dates within given range
var j = 0;
function CreateTable(name, id, dates, sgData, groupCount) {
    
    var present_count = 0;
    var meeting_date = "";
    var noRecord = false;
    CRM.$(function ($) {
        // clone table
        var divTable = $("#divDcSummaryTable").clone(true).appendTo("#divAccordion");
        //var table = $("#dcSummaryTableTemplate").clone(true).appendTo("#divAccordion");
        var table = divTable.find("#dcSummaryTableTemplate");
        divTable.attr('id', '');
        table.attr('id', "tableSummary_" + id);

        // name column
        //CRM.$("#dcSummaryTableTemplate").find("thead").append("<th>Leader</th>");
        var thead = table.find("thead");
        thead.append("<th>" + name + "</th>");  
        table.append("<tr><td style='text-align:right;'>Totals:</td></tr>");
        //console.log("CreateTable()", name, id, dates, sgData, groupCount);
        // LOOP THROUGH MEETING_DATES AND LOOKUP IN sgDATA as KEY
        $.each(dates, function (key, val) {
          //  console.log("LOOP MEETING DATES");
            // If there is no val yet for the count, set to 0
            

            // check for match to the date
            if (sgData[id][val]) {
                present_count = sgData[id][val];
                noRecord = false;
            }
            else { // NO ATTENDANCE RECORDED
                present_count = 0;
                noRecord = true;
            }
            meeting_date = val.substring(5, 10).replace("-", "/");
            
            // Generate HTML
            var columnHeader = "";
            var columnData = "";
            if(!noRecord) {
                columnHeader = "<th style='text-align:center;'>" + meeting_date + "</th>";
                columnData = "<td style='text-align:center;'>" + present_count + "</td>";
            }
            else {
                columnHeader = "<th style='text-align:center;'>" + meeting_date + "</th>";
                columnData = "<td style='text-align:center; background:red;'></td>";
            }
            
            
            thead.append(columnHeader);
            table.find("tr:last").append(columnData);

            // If there isn't a present count for the date yet, init it to 0
            //if (!dcTotals[meeting_date]) {
                //dcTotals[meeting_date] = 0; // default val
                //console.log('dcTotals', dcTotals[meeting_date], meeting_date);
            //}
            dcTotals[meeting_date] = dcTotals[meeting_date] + parseInt(present_count);
            
            j++;
            //console.log(j);
            /*
             * To check if we are done we compare against the number of dates
             * multiplied by the total number of groups
             */
            if((dates.length * groupCount) === j) { 
                CreateDCTableTotals(dcTotals);
                j = 0;
                $('#blockUI-Control-DCSUMMARY').hide(); 
                //$("#bRefreshDcSummaryTable").show(); // show refresh button again
            }
        });
        
    });
}

// Deal with the totals for the entire DC on each date
function CreateDCTableTotals(dcTotals) {
    //console.log("dcTotals", dcTotals);
    CRM.$(function ($) {
        var table = $("#dcSummaryTableTotals");
        var thead = table.find("thead");
        // clear previous
        //thead.empty();
        //table.find("tbody").empty();
        thead.append("<th>COMBINED ATTENDANCE</th>");
        table.append("<tr><td style='text-align:right;'>Totals:</td></tr>");
        
        // Loop through each key in dcTotals
        // Key is the date
        var prev_key = "";
        
        Object.keys(dcTotals).forEach(function (key) {
            //console.log("key: " + key);
            // Generate HTML
            var columnHeader = "<th style='text-align:center;'>" + key + "</th>";
            var columnData = "<td style='text-align:center;'>" + dcTotals[key] + "</td>";
            thead.append(columnHeader);
            table.find("tr:last").append(columnData);
//    if(key == 'values') console.log(key, results[key]);
            //console.log(key, results[key]);
        });
        //dcTotals = new Object(); // clear the DC totals object
    });
}

/*
 * Sets the start/end dates to current month by default
 * uses date.js
 */
function InitDates() {
    var dates = {
                "first": "",
                "last": ""
            };
    // check to see if dates are already entered in input
    CRM.$(function ($) {
        if ($("#dcSummaryStartDate").val() == "" || $("#dcSummaryEndDate").val() == "") {

            var fd = Date.today().clearTime().moveToFirstDayOfMonth();
            var firstday = fd.toString("MM/dd/yyyy");
            
            dates.first = (firstday);

            var ld = Date.today().clearTime().moveToLastDayOfMonth();
            var lastday = ld.toString("MM/dd/yyyy");
            dates.last = (lastday);
            
            $("#dcSummaryEndDate").val(lastday);
            $("#dcSummaryStartDate").val(firstday);
        }
        else {
            dates.first = $("#dcSummaryStartDate").val();
            dates.last = $("#dcSummaryEndDate").val();
        }
    });
    return dates;
}

//})(CRM.$, CRM._);

/**
 * Clear the DC Summary Table before re-populating it
 */
function ClearDCSummaryTable() {
    // Remove Summary Tables
    CRM.$("[id^=tableSummary]").remove();
    
    var table = CRM.$("#dcSummaryTableTotals");
    var thead = table.find("thead");
    // clear previous
    thead.empty();
    table.find("tbody").empty();
    
    //CRM.$('#dcSummaryTableTotals').
}