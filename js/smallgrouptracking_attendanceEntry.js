/* 
 * ATTENDANCE HISTORY SCRIPTS
 */

var isEdit = false; // state of attendence entry. We don't want to show some things when we are doing an edit

/*
 * INTERACTION WITH PAGE
 * @param {type} param
 */
 CRM.$(function ($) {
     // CLICKING ON ATTENDANCE ENTRY TAB
     $("#bTabsAttendance").click(function(){
        GetLoggedMeetings(GetGroupId());
     });
     
    // SUBMIT ATTENDANCE
    $('#bAddAttendance').click(function () {
        InitAddAttendance();
    });
    function InitAddAttendance(callback) {
        var date = $('#datepicker').val();
        if(!date) {
            CRM.alert('You must select a date before submitting attendance');
            return;
        }
        ShowBlockUI(25000);
        
        var members = [];
        // get date
        
        
        // Check to see if meeting exists for this date
        if(prevMeetings.length > 0) {
            //if(debug) console.log(prevMeetings);
            for(var i=0; i < prevMeetings.length; i++) {
                //if(debug) console.log(date + " vs " + prevMeetings[i]);
                if(date ===  prevMeetings[i] && !isEdit) {
                    CRM.alert('A meeting has already been recorded for this date!','Error','error');
                    HideBlockUI();
                    return; // abort function
                }
            }
        }
        
        // ShowLoader(); // show loading overlay
        // meeting type 
        
        /**
         * Loop through each row in the attendance tables and get the values
         * for each contact. These values are pushed into members object
         */
        
        var type = $('#meetingType').val();
        var present_count = 0;  // how many were present
        
        /**
         * REGULAR ATTENDERS
         */
        rows = $('#tableAttendance').find(".rowRef");
        $.each(rows, function () {
            var member_id = $(this).attr('data-cid'); // contact id
            var present = $(this).find("#present").is(':checked');
            if (!present) {
                present = false;
            } else {
                present_count++; 
            }
            var role = $(this).find("#role").val();
            var notes = $(this).find("#notes").val();
            
            members.push(
                {
                    ID:member_id,
                    activityDate:date,
                    activityType:type,
                    memberRole:role,
                    attended:present,
                    notes:notes
                }
            );
        });        
        /**
         * VISITORS ATTENDERS
         */
        rows = $('#tableAttendanceVisitor').find(".rowRef");
        $.each(rows, function () {
            var member_id = $(this).attr('data-cid'); // contact id
            var present = $(this).find("#present").is(':checked');
            if (!present) {
                present = false;
            } else {
                present_count++; 
            }
            var role = $(this).find("#role").val();
            var notes = $(this).find("#notes").val();
            
            members.push(
                {
                    ID:member_id,
                    activityDate:date,
                    activityType:type,
                    memberRole:role,
                    attended:present,
                    notes:notes
                }
            );
        });
        
        /**
         * IRREGULAR ATTENDERS
         */
        rows = $('#tableAttendanceIrregular').find(".rowRef");
        $.each(rows, function () {
            var member_id = $(this).attr('data-cid'); // contact id
            var present = $(this).find("#present").is(':checked');
            if (!present) {
                present = false;
            } else {
                present_count++; 
            }
            var role = $(this).find("#role").val();
            var notes = $(this).find("#notes").val();
            
            members.push(
                {
                    ID:member_id,
                    activityDate:date,
                    activityType:type,
                    memberRole:role,
                    attended:present,
                    notes:notes
                }
            );
        });
        
        /**
         * INACTIVE ATTENDERS
         */
        rows = $('#tableAttendanceInactive').find(".rowRef");
        $.each(rows, function () {
            var member_id = $(this).attr('data-cid'); // contact id
            var present = $(this).find("#present").is(':checked');
            if (!present) {
                present = false;
            } else {
                present_count++; 
            }
            var role = $(this).find("#role").val();
            var notes = $(this).find("#notes").val();
            
            members.push(
                {
                    ID:member_id,
                    activityDate:date,
                    activityType:type,
                    memberRole:role,
                    attended:present,
                    notes:notes
                }
            );
        });
        
        
        
        RecordAttendance(members, type, sglId, selectedSgGroupId, present_count, selectedDcId, date, callback);

        

           
    };
     
    // REMOVE MEMBER
    $('#tabs-attendance').on("click", "#bRemoveContact", function () {
        selectedContactId = $(this).attr('data-cid');
        if(debug) console.log('remove contact: ' + selectedContactId);
        $("#dialog-confirm.removeContact").dialog("open");
    });

    // Update Contact Role
    $('#tabs-attendance').on("change", "#role", function () {
        ShowBlockUI(5000);
        //$("#blockUI-Control").show();
        selectedContactId = $(this).attr('data-cid');
        var role = $('#tabs-attendance').find("select[data-cid='" + selectedContactId + "']").val();
        
        var customKey = "custom_" + sgt_settings.small_group_member_role_custom_field;
                
        // API Call to update role
        var params = {
            'sequential': 1,
            'entity_id': selectedContactId,
        }
        params[customKey] = role; // add role with key to the object
        
        // We may as well update the leader too (in case it was removed by accident)
        customKey = "custom_" + sgt_settings.small_group_leader_custom_field;
        params[customKey] = sglId;
        
        console.log('params to update role', params);
        CRM.api3('CustomValue', 'create', params
                ).done(function (result) {
                    LoadContacts(selectedSgGroupId); // reload contact list
                });

    });
    
    
    // ADD SMALL GROUP CONTACT 
    $('#bAddSmallGroupContact').click(function () {
        var cid = $('[name=field_addSmallGroupContact]').val();
        var role = $('#addContactRole').val();

        if (cid != "") { // make sure a selection is made
            CRM.api3('SmallGroupTracking', 'createsmallgroupcontact', {
                'sequential': 1,
                'cid': cid,
                'sgid': selectedSgGroupId,
                'sglid': sglId,
                'role': role
            }).done(function (result) {
                if(debug) console.log('done');
                
                /**
                 * We will get an error to display if this person is a small
                 * group leader
                 */
                if(result['is_error'] === 1) {
                    CRM.alert(result['error_message'],'Error','error');
                }
                else {
                    LoadContacts(selectedSgGroupId); // reload contact list
                }
                $('#divAddContact .select2-container').select2('val', "");
                $('#bAddSmallGroupContact').hide();
                
                // Alert last group the person was in.
                CRM.alert(result['contactname'] + ' last attended ' + result['lastgroup'].replace(' Small Group', '\'s small group.'), 'Last Group Alert', 'info', {expires:0});
            });
        }
        else {
            if(debug) console.log('no cid');
        }


    });
    
    // if a contact is located, show the add button
    $('[name=field_addSmallGroupContact]').change(function () {
        // handle cases where contact is cleared
        if ($('[name=field_addSmallGroupContact]').val() != "") {
            $('#bAddSmallGroupContact').show();
        }
        else {
            $('#bAddSmallGroupContact').hide();
        }

    });
    
    // ATTENDANCE ENTRY PAGE - REMOVE CONTACT
    $('#bRemoveContact').on("click", function () {
        if(debug) console.log('remove contact A');
        $("#dialog-confirm.removeContact").dialog("open");
    });
    
    // CONFIRM REMOVE OF CONTACT
    $("#dialog-confirm.removeContact").dialog({
        autoOpen: false,
        resizable: false,
        height: 150,
        modal: true,
        buttons: {
            "Yes": function () {
                $(this).dialog("close");
                RemoveSmallGroupContact(selectedContactId);
            },
            "No": function () {
                $(this).dialog("close");
            }
        }
    });
    
    // EDIT MEETING ENTRY
    $('#ulPrevGroupLogs').on("click", "#bEditMeeting", function () {
        var date = $(this).attr('data-date');
        // Dynamically adding data attrs to the dialog to access via confirmation buttons
        //$("#dialog-confirm.deleteMeeting").attr('data-date',date).attr('data-sglid',sglId).dialog("open");

        
        EditLoggedMeeting(date);
    });
    
    // DELETE MEETING ENTRY
    $('#ulPrevGroupLogs').on("click", "#bDeleteMeeting", function () {
        var date = $(this).attr('data-date');
        // Dynamically adding data attrs to the dialog to access via confirmation buttons
        $("#dialog-confirm.deleteMeeting").attr('data-date',date).attr('data-sglid',sglId).dialog("open");
    });
    
        // CONFIRM REMOVE OF MEETING
    $("#dialog-confirm.deleteMeeting").dialog({
        autoOpen: false,
        resizable: false,
        height: 160,
        modal: true,
        buttons: {
            "Yes": function () {
                $(this).dialog("close");
                //if(debug) console.log($(this).attr("data-date"));
                DeleteLoggedMeeting($(this).attr("data-date"),$(this).attr("data-sglid"));
            },
            "No": function () {
                $(this).dialog("close");
            }
        }
    });
    
    $("#bCancelEditAttendance").click(function(){
        ResetEditLoggedMeeting();
        isEdit = false;
    });
    
     $("#bUpdateAttendance").click( function(){
        bUpdateAttendance();
    });
    
     function bUpdateAttendance() {
         // Delete meeting
        var date = FormatDate($("#datepicker").val());
        if(debug) console.log('Update Date: ' + date );
        
        DeleteLoggedMeeting(date,sglId, function() {
            // Record attendance after logged meeting is deleted
            //$('#bAddAttendance').trigger("click", function() {
            InitAddAttendance(function() {
                ResetEditLoggedMeeting();
            })
            //});
        }); 
    }; 

});
 
/*
 * GET LOGGED MEETINGS
 * This refreshes the list of previous logs
 */
var prevMeetings = new Array();
function GetLoggedMeetings(group_id) {
    
    prevMeetings = new Array(); //clear
    CRM.$(function ($) {
        $("#ulPrevGroupLogs").empty();
        CRM.api3('SmallGroupTracking', 'getlogsmallgroupmeeting', {
            "sequential": 1,
            "order" : "desc",
            //"sgl_id": sgl_id // 
            "sgg_id" : group_id
        }).done(function (result) {
            // do something
            console.log(result);
            var appendVal = ""; // value we will append
            if(result['values'] !== null) {
                $.each(result.values, function(key, val) {
                    prevMeetings.push(ConvertDateToSlash(val['meeting_date'])); // update array with previous meetings. this is checked before submitting new attendance
                    appendVal = "<li>" +
                            val['meeting_date'] +
                            "<a id='bEditMeeting' style='cursor:pointer;' data-date='" + val['meeting_date'] +"'>Edit</a>" +
                            "<a id='bDeleteMeeting' style='cursor:pointer;' data-date='" + val['meeting_date'] +"'>Delete</a>" +
                            "</li>";
                    $("#ulPrevGroupLogs").append(appendVal);
                });
            }
            HideBlockUI();
            
            /**
             * Paginate the list of previous logs
             */
            jQuery('#divAttendanceEntryRight').pajinate({
                items_per_page : 15,
                num_page_links_to_display : 3,
                /* show_paginate_if_one : false,*/
            });
        });
        
    });
}

/*
 * DELETE LOGGED MEETING
 * @param {type} meeting_date
 * @param {type} group_leader_cid
 * @returns {null}
 */
function DeleteLoggedMeeting(meeting_date, group_leader_cid, callback) {
    if(debug) console.log("DeleteLoggedMeeting()", meeting_date, group_leader_cid);
    //ShowBlockUI(10000);
    //CRM.$("#blockUI-Control").show();
    if(!callback) {
        CRM.$('#divAttendanceEntryRight').block();
    }
    CRM.api3('SmallGroupTracking', 'deletemeeting', {
        "debug": 1,
        "sequential": 1,
        "meeting_date": meeting_date,
        "group_leader_cid": group_leader_cid
    }).done(function (result) {
        if(!isEdit && !callback) {
            //CRM.$("#blockUI-Control").hide();
            CRM.alert("Meeting log and activity has been deleted", "Attendance Entry Deleted");
            GetLoggedMeetings(GetGroupId()); // update list of logged meetings\
            CRM.$('#divAttendanceEntryRight').unblock();
        } else {
            callback();
            //GetLoggedMeetings(GetLeaderCid()); // update list of logged meetings
        }
        
        //HideBlockUI();
    });
}

// Load meeting into UI for editing
function EditLoggedMeeting(meeting_date) {
    if(debug) console.log("EditLoggedMeeting", meeting_date);
    if(debug) console.log("for selectedSgGroupId", selectedSgGroupId);
    // ShowLoader();
    ShowBlockUI(10000);
    // use the custom table to get the values easily
    // need to hide buttons that allow removing contacts and updating role

    // Swap Submit Attendance Button and show Cancel Edit and Update Attendance

    // Update the date
    var date = ConvertDateToSlash(meeting_date);
    CRM.$(function ($) {

        $("#datepicker").val(date);
        InitEditLoggedMeeting();
        
        // GET ATTENDANCE DETAILS
        CRM.api3('SmallGroupTracking', 'getattendancedetails', {
            "sequential": 1,
            "activity_date": meeting_date,
            "sg_group_id": selectedSgGroupId
        }).done(function (result) {
            console.log(result);
           // ACTIVITY TYPE
            
            if(result['count'] > 0) {
                var activityType = result['values'][0]['activity_type'];
                $("#meetingType").val();
                            //console.log("MEETING TYPE: " + )
                $.each(result['values'], function(key, val){
                    if(val['attended'] == 1) {
                        $('#divAttendanceEntryLeft').find("[data-cid=" + val['member_id']+"]").find("#present").prop('checked',true);
                    }
                    $('#divAttendanceEntryLeft').find("[data-cid=" + val['member_id']+"]").find("#notes").val(val['notes']);

                });
                //HideLoader();
            } else {
                alert('Oops! There was an error with this attendance record (Activity Type Null). You will need to DELETE this meeting and record it again. Sorry!');
            }

            HideBlockUI();
        });
    });
}

/*
 * Sets up the gui for editing a log
 * @returns {undefined}
 */
function InitEditLoggedMeeting() {
    isEdit = true;
    CRM.$(function ($) {
        // PREP GUI BY HIDING/SHOWING/DISABLING
                
        // Hide submit button
        $("#divSubmitAttendance").hide();
        
        // Show Edit Buttons
        $("#divCancelEditAttendance").show();
        $("#divUpdateAttendance").show();
        
        // Make readonly
        $("#datepicker").attr("readonly", "readonly");
        $("#divAddContact").hide();
        
        // Hide Update Role and Remove buttons
        //$('[id*=bRemoveContact]:visible').each(function() { //  the asterisk '*' at the beginning of the selector matches all elements.
            //$(this).hide();
        //});
        //$('[id*=bUpdateRole]:visible').each(function() { //  the asterisk '*' at the beginning of the selector matches all elements.
           // $(this).hide();
        //});
        
        // HIDE COLUMNS
        // using wildcard to hide all that start with id
        $('[id^=tableAttendance] td:nth-child(4),th:nth-child(4)').hide();
        $('[id^=tableAttendance] td:nth-child(5),th:nth-child(5)').hide();
        $('[id^=tableAttendance] td:nth-child(6),th:nth-child(6)').hide();
        
        // Headers
        $('.hEditAttendance').show();
        $('.hAddAttendance').hide();
        $('#divAttendanceEntryRight').hide();
    });
}

/*
 * RESETS the GUI after editing a log
 * @returns {undefined}
 */
function ResetEditLoggedMeeting() {
    CRM.$(function ($) {
        
        // PREP GUI BY HIDING/SHOWING/DISABLING
        
        $("#divSubmitAttendance").show();
        $("#divCancelEditAttendance").hide();
        $("#divUpdateAttendance").hide();
        
        $("#datepicker").removeAttr('readonly');
        $("#datepicker").val(''); // clear date value
        $("#divAddContact").show();

        /*
        $('[id*=bRemoveContact]:hidden').each(function () { //  the asterisk '*' at the beginning of the selector matches all elements.
            $(this).show();
        });
        $('[id*=bUpdateRole]:hidden').each(function () { //  the asterisk '*' at the beginning of the selector matches all elements.
            $(this).show();
        });
        */
       
        // COLUMNS
        $('[id^=tableAttendance] td:nth-child(4),th:nth-child(4)').show();
        $('[id^=tableAttendance] td:nth-child(5),th:nth-child(5)').show();
        $('[id^=tableAttendance] td:nth-child(6),th:nth-child(6)').show();
        
        // HEADERS
        $('.hAddAttendance').show();
        $('.hEditAttendance').hide();
        
        // CLEAR CHECKBOXES
        $('[id*=present]:checkbox').prop('checked',false);
        
        // Clear Notes
        $("[id*=notes]:text").val("");
        
        $('#divAttendanceEntryRight').show();
         //isEdit = false;
    });
   
}

/*
 * DISPLAY LOGGED MEETINGS
 */
function DisplayLoggedMeetings() {
    // Update DOM to show the logged meetings for a given small group
}
function LogMeeting(type, sglId, selectedSgGroupId, present_count, selectedDcId, date, callback) {
    /* RECORD MEETING INTO CUSTOM TABLE (custom_smallgroup_attendance)
    * We need to store all meetings in the custom table for reference
    * when looking up attendance history
    * 
    * Store meeting date, type
    */

   var formattedDate = FormatDate(date); // YYYYMMDD

   CRM.api3('SmallGroupTracking', 'logsmallgroupmeeting', {
       "sequential": 1,
       "date": formattedDate,
       "meeting_type": type,
       "sgl_id": sglId,
       "sgg_id": selectedSgGroupId,
       "present_count": present_count,
       "dc_group_id": selectedDcId
   }).done(function (result) {
       if(debug) console.log(result);

       CRM.alert("Attendance has been recorded!", "Success", 'success');

       if (!isEdit && !callback) { // if editing we call this in another location
           if(debug) console.log('AddAttendence, not editing');
           GetLoggedMeetings(GetGroupId()); //update list of logged meetings
       }
       isEdit = false;

       //GetDcAttendanceEntries(groupLeaders, groupLeaders.length); // refreshes DC attendance summary    
       LoadContacts(selectedSgGroupId); //reload the list in case of changes 

       if(callback) callback();
       //HideLoader();
       //HideBlockUI();
   });
}

function RecordAttendance(members, type, sglId, selectedSgGroupId, present_count, selectedDcId, date, callback) {
    console.log('RecordAttendance');
    var attendance = {};
    var attendanceJSON = null;
    
    attendance.dcLeaderID = null ;
    attendance.sgGroupID = selectedSgGroupId;
    attendance.sgLeaderID = sglId;
    
    attendance.members = members;
    
    attendanceJSON = JSON.stringify(attendance);
    
    // CREATE THE ACTIVITY
    CRM.$(function ($) {
        
        // INSERT INTO CUSTOM TABLE
        // Do once for all
        //console.log(attendanceJSON);
        //console.log(members);
        CRM.api3('SmallGroupTracking', 'addsmallgroupattendance', {
            "sequential": 1,
            "attendance": attendanceJSON
        }).done(function (result) {
            if(debug) console.log(result['values']);
            
            //CREATE ACTIVITY
            //Do once for each member
            if(result['values'] == '') {
                if(debug)console.log('success');
                $.each(attendance.members,function (index, member) {
                    CRM.api3('Activity', 'create', {
                        "sequential": 1,
                        "activity_type_id": sgt_settings.small_group_activity, // small group attendance
                        "activity_date_time": member.activityDate,
                        "target_id": member.ID,
                        //"source_contact_id": 2,
                        "details": member.notes +",[sglid="+attendance.sgLeaderID+"]", // append SGLID for lookups later
                        "status_id": member.attended?12:11,
                        "subject": member.activityType, 
                        "location": member.memberRole,
                        "source_contact_id": attendance.sgLeaderID
                    }).done(function (result) {
                        //if(debug) console.log('activity created');
                        
                    });
                });
                LogMeeting(type, sglId, selectedSgGroupId, present_count, selectedDcId, date, callback);
            }
            else {
                if(debug)console.log('fail');
                alert('There was an error recording attendance. Try submitting again! If the error persists contact admin@clearriverchurch.org');
                HideBlockUI();
            }
        });

    });
}

function RecordSingleAttendance(date, subject, cid, details, present, role) {
    if(debug) console.log('RecordSingleAttendance');
    var status_id = 11;
    var bit = 0;
    // status_id - 12:present, 11:absent
    if (present) {
        status_id = 12;
        bit = 1;
    }
    // CREATE THE ACTIVITY
    CRM.$(function ($) {
        CRM.api3('Activity', 'create', {
            "sequential": 1,
            "activity_type_id": 58, // small group attendance
            "activity_date_time": date,
            "target_id": cid,
            //"source_contact_id": 2,
            "details": details +",[sglid="+sglId+"]", // append SGLID for lookups later
            "status_id": status_id,
            "subject": subject, 
            "location": role,
            "source_contact_id": sglId
        }).done(function (result) {
            //if(debug) console.log(result);
        });

        /*
        // UPDATE CUSTOM FIELDS (ROLE)
        // Small Group -> Role/Leader
        $result = CRM.api3('CustomValue', 'create', {
            'sequential': 1,
            'entity_id': cid,
            'custom_158': role // role
        });
        */
       
        // INSERT INTO CUSTOM TABLE
        CRM.api3('SmallGroupTracking', 'addsmallgroupattendance', {
            "sequential": 1,
            "member_id": cid,
            "sg_group_id": selectedSgGroupId,
            "sg_leader_id": sglId,
            "attended": bit,
            "activity_date": FormatDate(date),
            "notes": details,
            "activity_type": subject,
            "member_role": role
        }).done(function (result) {
        });

    });
}

/*
 * Convert Date to Forward Slash MM/DD/YYYY
 * passing in as YYYY-MM-DD
 */
function ConvertDateToSlash(date) {
    var newDate = "";
    var dateParse = date.split("-");
    newDate = dateParse[1] + "/" + dateParse[2] + "/" + dateParse[0];
    return newDate;
}
