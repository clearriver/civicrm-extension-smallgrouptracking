/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var smallgroupTrackingApp = angular.module("SmallgroupTracking", []);
/**
 * Angular Generation of Attendance History
 * @param {type} param1
 * @param {type} param2
 */
smallgroupTrackingApp.controller("AttendanceHistory", function ($scope) {
    if(debug) console.log('init AttendanceHistory controller');
    $scope.start = '';
    $scope.end = '';
    $scope.headers = [];
    $scope.attendees = [];
    
    $scope.search = '';
    
    $scope.predicate = 'name';
    $scope.reverse = false;
    
    $scope.presentChecker = function (value) {
        //if(debug) console.log('$scope.presentChecker', value);
        if (value === 1) {
            return "X";
        } 
        else if (value === 0) {
            return " ";
        }
        else {
            return "n/a";
        }
    };
    
    $scope.presentClass = function (value) {
        //if(debug) console.log('$scope.presentClass', value);
        if (value === 1) {
            return "memberpresent";
        } 
        else if (value === 0) {
            return "memberabsent";
        }
        else {
            return "norecord";
        }
    };
    
    $scope.dateTotal = function (key) {
        // if(debug) console.log('$scope.dateTotal', key);
        var totalCount = 0;
        angular.forEach($scope.attendeesSet, function(member){
            totalCount += member[key];
        });
        return totalCount;
    };
    
    $scope.sort = function(predicate) {
        if (predicate instanceof Array && $scope.predicate instanceof Array) {
            $scope.reverse = !$scope.reverse;
        }
        else if ($scope.predicate === predicate) {
            $scope.reverse = !$scope.reverse;
        }
        else {
            $scope.reverse = false;
        }
        
        $scope.predicate = String(predicate);
    };
    
    /* Run after clicking bGetHistory*/
    $scope.execute = function () {
        if(debug) console.log('$scope.execute');
        if(sgtBlockClicks) { return; };
        ShowBlockUI(15000);
        
        $scope.start = CRM.$('#startDate').val();
        $scope.end = CRM.$('#endDate').val();
    
        if ($scope.start === "" && $scope.end !== "") {
            $scope.start = $scope.end;
        }
        else if ($scope.start !== "" && $scope.end === "") {
            $scope.end = $scope.start;
        }
        else if ($scope.start === "" && $scope.end === "") {
            console.log('no date selected');
            CRM.alert("Please select a date or range", "Error:");
            HideBlockUI();
            return;
        }
        
        // WE SHOULD NOT GET MEMBERS FROM THIS TABLE
        /**
         * We need to get members from the actual attendance history
         * we want ALL records, even if they are not currently in the small group
         * 
         * This info is in custom_smallgroup_meeting_attendance
         * Get records that = sg_group_id
         * 
         */
        var memberRows = CRM.$("#memberList tr:gt(0)");
        var cids = new Array();
        
        memberRows.each(function () {
            cids.push(CRM.$(this).attr('data-cid'));
        });
        
        // Handle MEETING TYPE FILTER
        var meeting_type = [];
        var discussion = CRM.$('#meeting_discussion').prop('checked') ;
        var party = CRM.$('#meeting_Party').prop('checked') ;
        var DC = CRM.$('#meeting_DC').prop('checked') ;
        var none = CRM.$('#meeting_none').prop('checked') ;
        
        if(discussion) {
            console.log('discussion');
            meeting_type.push('Discussion');
        }
        if(party) {
            meeting_type.push('Party');
        }
        if(DC) {
            meeting_type.push('DC');
        }
        if(none) {
            meeting_type.push('No Meeting');
        }
        
        /**
         * Get the logged meetings for the group within given date range
         * and assign the values to the header
         * 
         * NOTE: This is redundant. We currently get the logged meetings via
         * the API
         */       
        CRM.api3('SmallGroupTracking', 'getlogsmallgroupmeeting', {
            "sequential": 1,
            "start_date": FormatDate($scope.start),
            "end_date": FormatDate($scope.end),
            "sgg_id": selectedSgGroupId, // only get logs related to this group
            "meeting_type": meeting_type
        }).done(function (result) {
//           console.log(result.values);
            //console.log('getlogsmallgroupmeeting');
            //console.log(meeting_type);
            $scope.headers = result.values;
            for (var header in $scope.headers) {
                if ($scope.headers[header].meeting_date !== undefined) {
                    $scope.headers[header].simple_date = parseInt($scope.headers[header].meeting_date.replace(/-/g, ''));
                    
                    // reformat the meeting_date header
                    var headSplit = $scope.headers[header].meeting_date.split("-"); // split header 0-year,1-month,2-day
                    $scope.headers[header].meeting_date = headSplit[1] + "/" + headSplit[2] + " (" + headSplit[0] + ")";
                    //$scope.headers[header].simple_date = parseInt($scope.headers[header].meeting_date.replace(/-/g, ''));
                    
                    //console.log('simple date: ', $scope.headers[header].simple_date);
                }
            }
        });
        
        /**
         * Get the attendance history  records from the custom_attendance table
         */
       
        CRM.api3('SmallGroupTracking', 'getsmallgroupattendance', {
            "sequential": 1,
            "start_date": FormatDate($scope.start),
            "end_date": FormatDate($scope.end),
            // "cids": cids,
            "sgg_id": selectedSgGroupId,
            "meeting_type": meeting_type
            //"activity_type_id": 58
        }).done(function (result) {
            // Get headers
            //$scope.headers = result.values[0]; // includes date and meeting type
//            console.log('getsmallgroupattendance');
//            console.log(meeting_type);
            $scope.attendees = Object.keys(result.values[1]).map( function(key) {
                return result.values[1][key];
            });
            //console.log($scope.attendees, '$scope,attendees');
            
            $scope.$apply();
            //CRM.$("#blockUI-Control").hide(); // hide loading overlay
            HideBlockUI();
        });
    };
});

function resetAttendanceHistory() {
    var appElement = document.getElementById("AttendanceHistoryApp");
    var $scope = angular.element(appElement).scope();
    $scope.$apply(function() {
        $scope.attendees = [];
    });
}

/**
 * Prefix an integer to make it a specific length
 * @param {type} number
 * @param {type} len
 * @returns {unresolved}
 */
function PrefixInt(number, len) {
   return (Array(len).join('0') + number).slice(-length);
}

CRM.$(function($) {
    
    /*
     * GET CURRENT DATE
     */
    var today = new Date().toJSON().slice(0,10); //yyyy-mm-dd
    var temp = today.split("-");
    var endDate = temp[1] + "/" + temp[2] + "/" + temp[0];
    
    /*
     * GET DATE 3 MONTHS EARLIER
     */
    var startDate = new Date();
    startDate.setDate(1); // set day of month
    startDate.setMonth(startDate.getMonth()-3); // sets the month
    startDate = startDate.toJSON().slice(0,10);
    temp = startDate.split("-");
    startDate = temp[1] + "/" + temp[2] + "/" + temp[0];
       
    $(document).ready(function() {
        $('#divDateRange #endDate').val(endDate);
        $('#divDateRange #startDate').val(startDate);
        $('#AttendanceHistoryApp input[type="checkbox"]').prop('checked',true); // check all boxes by default
    });
   
   CRM.$('#meeting_dc').click(function() {
       console.log('click');
      if($(this).prop('checked')) {
          $(this).removeProp('checked');
      }
   });
   
   /*
    * ALWAYS KEEP AT LEAST ONE CHECKBOX FOR MEETING TYPE
    */
   var checkboxCount = 4; // holds how many are checked
   CRM.$('#AttendanceHistoryApp input[type="checkbox"]').on('click', function(event) {
       if(!CRM.$(this).prop('checked') && checkboxCount > 1) {
           checkboxCount--; // reduce num checked
       }
       else if (CRM.$(this).prop('checked')) {
           checkboxCount++; //increase count
       }
       else {
           CRM.$(this).prop('checked',true); //don't allow less than 1 checkbox
       }
   });
    
});