/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

CRM.$(function ($) {
    // Attendance Entry
    $("#datepicker").datepicker(
            {
                // only allow selection if NOT readonly
                beforeShow: function (i) {
                    if ($(i).attr('readonly')) {
                        return false;
                    }
                }
            }
    );
});