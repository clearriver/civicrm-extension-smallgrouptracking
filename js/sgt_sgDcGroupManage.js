/* 
 * Deals with small group and dc creation/deletion
 */
//var basePath = Drupal.settings["basePath"];
var basePath = getBaseURL();
//var sgt_settings;
// intialize jquery for access with $

/**
 * Do this after we retrieve settings!
 * @param {type} settings
 * @returns {undefined}
 */

CRM.$(function ($) {
    MembersSettings_Click(); // load member settings
    ChangeSmallGroupLeader_Click();

    $('#bCreateSmallGroup').click(function (e) {
        /*
         var firstname = $('#sgLeaderFirstname').val();
         var lastname = $('#sgLeaderLastname').val();
         var dcFirstname = $('#dcFirstname').val();
         var dcLastname = $('#dcLastname').val();
         var day = $('#sgDay').val();

         if(firstname != "" && lastname !="" && dcLastname !="" 
         && dcFirstname !="" && day != "") {
         //CreateSmallGroup(firstname, lastname, dcFirstname, dcLastname, day);
         }
         */
        //CRM.dialogArguments()
        var myForm = CRM.loadForm(basePath + 'civicrm/smallgrouptracking/createsmallgroup')
                .on('crmFormLoad', function () {
                    myForm.css("height", "110px");
                })
                .on('crmFormSuccess', function () {
                    //console.log("Form Success: InitRefreshLeaderList();");
                    GetDCSmallGroups(selectedDcId);
                });

    });

    $('#bDeleteSmallGroup').click(function (e) {
        // Get text fields needed to delete
        /*
         var firstname = $('#sgLeaderFirstname').val();
         var lastname = $('#sgLeaderLastname').val();
         if(firstname != "" && lastname !="") {
         DeleteSmallGroup(firstname, lastname);
         }
         */
        var myForm = CRM.loadForm(basePath + 'civicrm/smallgrouptracking/deletesmallgroup').css("height", "200px")
                .on('crmFormSuccess', function () {
                    //console.log("Form Success: InitRefreshLeaderList();");
                    GetDCSmallGroups(selectedDcId);
                    // Update header to remove currently selected leader name
                    $('.smallGroupLeaderHeader').text('Small Group Leader');
                }).on('crmFormLoad', function () {
            myForm.css("height", "70px");
        });

    });

    $('#bCreateDcGroup').click(function (e) {
        var myForm = CRM.loadForm(basePath + 'civicrm/smallgrouptracking/createdcgroup')
                .on('crmFormLoad', function () {
                    myForm.css("height", "145px");
                });
        ;
    });

    $('#bChangeSmallGroupDC').click(function (e) {
        var myForm = CRM.loadForm(basePath + 'civicrm/smallgrouptracking/changesmallgroupdc')
                .on('crmFormLoad', function () {
                    myForm.css("height", "110px");
                })
                .on('crmFormSuccess', function () {
                    //console.log("Form Success: InitRefreshLeaderList();");
                    GetDCSmallGroups(selectedDcId);
                });
    });

    // DELETE DC
    $('#bDeleteDcGroup').click(function (e) {
        var myForm = CRM.loadForm(basePath + 'civicrm/smallgrouptracking/deletedcgroup')
                .on('crmFormLoad', function () {
                    myForm.css("height", "110px");
                })
                .on('crmFormSuccess', function () {
                    //console.log("Form Success: InitRefreshLeaderList();");
                    //GetDCSmallGroups(selectedDcId);
                });
    });



    // AUTOCOMPLETE FIELD 
    $('[name=field_addSmallGroupContact]').crmEntityRef({
        api: {params: {contact_type: 'Individual'}},
        create: false,
    });

    // Build DC SELECTION
    CRM.api3('Group', 'get', {
        "sequential": 1,
        "parents": sgt_settings.discipleship_community_group
    }).done(function(result) {
        result.values.sort(function(a, b){
            var dayCodes = ['','sun','mon','tue','wed','thu','fri','sat'];
            aCode = dayCodes.indexOf(a.title.substring(0,3).toLowerCase());
            bCode = dayCodes.indexOf(b.title.substring(0,3).toLowerCase());
            return aCode - bCode;
        });
        $.each(result.values, function(index, dc){
            var option = $(document.createElement('option'));
            option.attr('value', dc.id);
            option.text(dc.title);
            option.appendTo('[name=field_selectDC]');
        });
    });

    if(typeof window.orientation === 'undefined'){
        $('[name=field_selectDC]').select2({
            placeholder: '- select DC -',
            minimumResultsForSearch: Infinity
        });
        $('#smallgroupSelect').select2({
            placeholder: '- select Smallgroup -',
            minimumResultsForSearch: Infinity
        });
    }

    var thisDC = 0;


    // SMALL GROUP SELECTION
    $('[name=field_selectSG]').crmEntityRef({
        entity: 'group',
        api: {
            params: {
                //contact_type: 'Individual',
                parents: thisDC // dc id
            }},
        create: false
    });

    var thisSG = null;
    $('#bSelectSG').click(function () {
        if (thisDC == null) {

        }
        else {
            thisSG = $('#thisSG').val();
        }
    });
});

function DeleteSmallGroup(firstname, lastname) {
    CRM.api3('SmallGroupTracking', 'deletesmallgroup', {
        "sequential": 1,
        "firstname": firstname,
        "lastname": lastname
    }).done(function (result) {
        if (result['is_error'] == 1) {
            console.log('Error: ' + result['error_message']);
        }
        else {
            GetDCSmallGroups(selectedDcId); // refresh small group list
        }
    });
}

function CreateSmallGroup(firstname, lastname, dcFirstname, dcLastname, day) {
    console.log('Create Small Group');
    CRM.api3('SmallGroupTracking', 'createsmallgroup', {
        "sequential": 1,
        "firstname": firstname,
        "lastname": lastname,
        "day": day,
        "dcFirstname": dcFirstname,
        "dcLastname": dcLastname
    }).done(function (result) {
        if (result['is_error'] == 1) {
            console.log('Error: ' + result['error_message']);
        }
        else {
            GetDCSmallGroups(selectedDcId); // refresh small group list
        }
    });
}

function MoveToDC(selectedSgGroupId, selectedDcId, targetDc) {
    // remove from Currently selected DC

    // PHP CALL: CRM_Contact_BAO_GroupNesting::remove($parentGroupId, $group->id);
    // $dao = new CRM_Contact_DAO_GroupNesting();
    // $query = "DELETE FROM civicrm_group_nesting WHERE child_group_id = $childID AND parent_group_id = $parentID";
    // $dao->query($query);
    // 
    // Add to target DC

}

function ChangeSmallGroupLeader_Click() {
   CRM.$('#bChangeSmallGroupLeader').click(function (e) {
       console.log("path: " + basePath + 'civicrm/smallgrouptracking/ChangeSmallgroupLeader');
        var myForm = CRM.loadForm(basePath + 'civicrm/smallgrouptracking/ChangeSmallgroupLeader').css("height", "400px")
                .on('crmFormSuccess', function () {
                    // Reload Page
                    document.location.reload(true);
                }).on('crmFormLoad', function () {
                    myForm.css("height", "220px");
                });

    }); 
}

function MembersSettings_Click() {
    CRM.$('#bMembersSettings').click(function (e) {
        // Get text fields needed to delete
        /*
         var firstname = $('#sgLeaderFirstname').val();
         var lastname = $('#sgLeaderLastname').val();
         if(firstname != "" && lastname !="") {
         DeleteSmallGroup(firstname, lastname);
         }
         */
        var myForm = CRM.loadForm(basePath + 'civicrm/smallgrouptracking/settings').css("height", "400px")
                .on('crmFormSuccess', function () {
                    // Reload Page
                    document.location.reload(true);
                }).on('crmFormLoad', function () {
                    myForm.css("height", "400px");
                });

    });
}

function getBaseURL() {
    var url = location.href;  // entire url including querystring - also: window.location.href;
    var baseURL = url.substring(0, url.indexOf('/', 14));


    if (baseURL.indexOf('http://localhost') != -1) {
        // Base Url for localhost
        var url = location.href;  // window.location.href;
        var pathname = location.pathname;  // window.location.pathname;
        var index1 = url.indexOf(pathname);
        var index2 = url.indexOf("/", index1 + 1);
        var baseLocalUrl = url.substr(0, index2);

        return baseLocalUrl + "/";
    }
    else {
        // Root Url for domain name
        return baseURL + "/";
    }

}