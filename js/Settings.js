/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

CRM.$(function ($) {
    
    //Initialization
    $('#custom_table_dummy').data('last', $(this).val());
    
    //Event Listeners
    $(document).ready(function(){
        var workingRow = $('#custom_table_dummy').closest('tr');
        var customFields = $(workingRow).closest('table').data('customFields').split(',');
        
        if (customFields[0] === '') return;
        
        /*
         * Take each custom field setting and recreates the fields for editing
         * and adds various elements to reorder and remove rows
         */
        $.each(customFields, function(index, customField){
            var fieldAttributes = customField.split(':');
            var workingRowInputs = $(workingRow).find('select, input');
            
            addNewDummyRow(workingRow);
            addDeleteButton(workingRow);
            addReorderButtons(workingRow);
            workingRowInputs.eq(0).val(fieldAttributes[0]);
            workingRowInputs.eq(1).val(fieldAttributes[1]);
            workingRowInputs.eq(2).attr('value', fieldAttributes[2]);
            
            populateCustomFields(workingRow, fieldAttributes[1]);
            workingRowInputs.eq(0).data('last', workingRowInputs.eq(0).val());
            
            workingRowInputs.eq(0).attr('id', 'custom_table_temp');
            workingRow = $('#custom_table_dummy').closest('tr');
        });
        
        renameSelects(workingRow.closest('tbody'));
    });
    
    $('#custom_table_dummy').change(function(event){
        var fieldRow = $(event.target.closest('tr'));
        var tableBody = $(fieldRow).closest('tbody');
        
        //Ensure that one dummy row always exists
        if (event.target.value === "") {
            removeRow(fieldRow);
        } else if ($(this).data('last') === ''){
            addNewDummyRow(fieldRow);
            addDeleteButton(fieldRow);
            addReorderButtons(fieldRow);
        }
        
        //populate the custom fields
        populateCustomFields(fieldRow);
        
        //rename table and field selects
        renameSelects(tableBody);
        
        $(this).data('last', $(this).val());
    });
    
    $('#custom_field_dummy').change(function(event){
        var label = $(this).find(':selected').data('label');
        $(this).closest('tr').find('input').first().attr('value', label);
    });
    
    //Helper Methods
    function removeRow(row) {
            $(row).remove();
    }
    
    function addNewDummyRow(changedRow) {
            var clonedRow = changedRow.clone(true);
            $(changedRow.closest('tbody')).append(clonedRow);
    }
    
    function addDeleteButton(row) {
        row.find('td:last div:last').append(' ').append(
            $(document.createElement('a'))
                .append('Delete')
                .css('text-decoration','none')
                .css('cursor','default')
                .click(function(event){
                    var tableBody = event.target.closest('tbody');
                    removeRow($(event.target).closest('tr'));
                    renameSelects(tableBody);
                })
            );
    }
    
    function addReorderButtons(row) {
        row.find('td:first div:last').prepend(' ').prepend(
            $(document.createElement('a'))
                .append('&#x25BC;')
                .css('text-decoration','none')
                .css('cursor','default')
                .click(function(event){
                    var row = $(this).closest('tr');
                    row.insertAfter(row.next('tr:not(:has(#custom_table_dummy))'));
                    renameSelects($(this).closest('tbody'));
                })
            ).prepend(
            $(document.createElement('a'))
                .append('&#x25B2;')
                .css('text-decoration','none')
                .css('cursor','default')
                .click(function(event){
                    var row = $(this).closest('tr');
                    row.insertBefore(row.prev('tr'));
                    renameSelects($(this).closest('tbody'));
                })
            );
    }
    
    /* After a custom group is selected this function gets all custom fields within
     * and adds them to the select, if value is provided will set that to provided value.
     * 
     * @param DOMElement changedRow
     * @param String value
     * @returns {undefined}
     */
    function populateCustomFields(changedRow, value) {
        CRM.api3('SmallGroupTracking', 'getcustomtablefields', {
            "sequential": 1,
            "custom_table": $(changedRow).find('select').first().val()
        }).done(function (results) {
            var customFieldSelect = $(changedRow).find('select')[1];
            
            $(customFieldSelect).find('option:gt(0)').remove();
            
            $.each(results.values, function(index, value) {
                $(customFieldSelect)
                        .append($('<option></option>')
                        .attr('value',value.column_name)
                        .text(value.label)
                        .data('label', value.label));
            });
            
            $(customFieldSelect).val(value);
        });
    }
    
    /*
     * Takes the provided table body element and gives the first 3 inputs sequential ids
     */
    function renameSelects(tableBody) {
        $(tableBody).children().each(function(index, row){
            var rowFields = $(this).find('select, input');
            
            if(rowFields.first().val() !== ''){
                $(rowFields[0]).attr('name', 'custom_table_' + index);
                $(rowFields[0]).attr('id', 'custom_table_' + index);
                $(rowFields[1]).attr('name', 'custom_field_' + index);
                $(rowFields[1]).attr('id', 'custom_field_' + index);
                $(rowFields[2]).attr('name', 'alias_' + index);
                $(rowFields[2]).attr('id', 'alias_' + index);
            }
        });
    }
});