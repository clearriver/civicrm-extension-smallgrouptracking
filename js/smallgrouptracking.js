if(debug) console.log('smallgrouptracking.js loaded');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var debug = true;

var extension_name = "org.namelessnetwork.smallgrouptracking"; // used for getting settings, etc
var sgt_settings; // global var to hold settings

// GLOBAL VARS
var selectedSgGroupId = null; // holds the selected small group group id
var sglId = 2; //holds small group leader cid
var selectedContactId = null; // for operations on specific contacts
var selectedDcId = 46; // which DC is selected
var contactIds = new Array(); // holds all the contact IDs for a group
var eventIds = new Array(); // holds ids for all upcoming events
var countRegular = 0;
var countIrregular = 0;
var countVisitor = 0;
var countInactive = 0;
var $memberRowList = null; //holds the table row for each member in the group in the member tab

var sgtBlockClicks = false; //block a click because we are processing somethign?

// JQUERY MUST BE CALLED THIS WAY IN ORDER FOR PLUGINS TO LOAD!
/*
 jQuery(document).ready(function ($) {
 WindowSize();
 //GetDCSmallGroups(46); //preload
 Init();
 $('table').filterTable();
 });
 */

CRM.$(document).init(function($) {

     //GetSettings();
});

CRM.$(document).ready(function ($) {
    //WindowSize();
    
    /**
     * Check to see if the user is running IE8 or less. This is not supported!
     */
    if (isIE()) {
        //console.log(isIE());
        CRM.$('.browserNotSupported').show();
    } else {
        Init();
        SetStickyHeader();
    }
    
    styleMobileMemberHeadings();
});

function styleMobileMemberHeadings(){
    var style = document.createElement('style');
    style.setAttribute('media', 'screen and (max-width: 720px)');
    style.appendChild(document.createTextNode(''));
    document.head.appendChild(style);
    var stylesheet = style.sheet;
    
    if('insertRule' in stylesheet) {
        CRM.$('#memberList thead tr th').each(function (index) {
            stylesheet.insertRule('#memberList td:nth-of-type(' + (index+1) + '):before { content: "' + CRM.$(this).text() + '"; }', 0);
        });
    }
    else if('addRule' in stylesheet) {
        CRM.$('#memberList thead tr th').each(function (index) {
            stylesheet.addRule('#memberList td:nth-of-type(' + (index+1) + '):before', 'content: "' + CRM.$(this).text() + '";', 0);
        });
    }
}

function MemberStats() {
    //if(debug) console.log('Member Stats');
}

CRM.$(function ($) {
    $(window).resize(function () {
        //WindowSize();
    });
});

/* WindowSize()
 * Handle Layout based on browser window size
 * @returns {undefined}
 */
function WindowSize() {
    // small group list is 200px wide;
    CRM.$(function ($) {

        // Get width of content area
        var divContentWidth = $("div.content").width() - 230; // small group list is 200px wide;
        // set #divContent width
        //var divContentWidth = width - 230; 
        $("#divContent").width(divContentWidth);

        if ($("#squeeze").width() < 915 && $("#sidebar-second:visible")) {
            $("#squeeze").css("margin-right", "0px");
            $("#sidebar-second").hide();
            divContentWidth = $("div.content").width() - 240;
            $("#divContent").width(divContentWidth);
        }
        else if ($("#squeeze").width() > 1125 && $("#sidebar-second:hidden")) {
            $("#squeeze").css("margin-right", "210px");
            $("#sidebar-second").show();
            divContentWidth = $("div.content").width() - 240;
            $("#divContent").width(divContentWidth);
        }


    });
}

// jquery-ui tabs
CRM.$(function ($) {
    $("#tabs").tabs();
    
    function responsiveStyling() {
        if ($(window).width() < 720) {
            $('a[href="#tabs-attendanceHistory"]').parent().css('display', 'none');
        } else {
            $('a[href="#tabs-attendanceHistory"]').parent().css('display', 'block');
        }
    }
    
    responsiveStyling();
    $(window).resize(responsiveStyling);
});

CRM.$(function ($) {
    // Attendance History
    $("#startDate").datepicker();
    $("#endDate").datepicker();
});

// Admin controls menu

CRM.$(function ($) {
    $('#adminMenu').click(function(){
        if ($(this).siblings().css('display') !== 'block') {
            $(this).siblings().css('display', 'block');
        } else {
            $(this).siblings().css('display', 'none');
        }
    });
});

// civi built in accordion
//CRM.$('#divAccordionDcSummary').accordion();
/*
CRM.$(function () {
    cj().crmAccordions();
});

CRM.$(function () {
    cj('.crm-ajax-accordion .crm-accordion-header').one('click', function () {
        loadPanes(cj(this).attr('id'));
    });
    cj('.crm-ajax-accordion.crm-accordion-open .crm-accordion-header').each(function (index) {
        loadPanes(cj(this).attr('id'));
    });
});

// jquery-ui accordion
CRM.$(function ($) {
    $("#accordion").accordion();
});
*/
// BUTTONS
function SelectDC() {};
CRM.$(function ($) {
    var rows; // each row for contact in tableAttendance

    // HIDE BUTTONS
    $('#bAddSmallGroupContact').hide(); // only want to show when contact is selected

    // DC SELECTION
    $('[name=field_selectDC]').change(function () {
        thisDC = $('#thisDC').val();
        selectedDcId = thisDC;
        if(debug) console.log('thisDC: ' + thisDC);

        // Update Subtitle
        //$("#subTitleDC").text($('#s2id_thisDC .select2-chosen').text());
        
        // Add day attr to the subTitleDC for lookup as needed
        var titleText = $('#s2id_thisDC .select2-chosen').text().split("-");
        var day = 0;
        switch(titleText[0]) {
            case "Sunday":
                break;
            case "Monday":
                day = 2;
                break;
            case "Tuesday":
                day = 3;
                break; 
            case "Wednesday":
                day = 4;
                break;
            case "Thursday":
                day = 5;
                break;
            case "Friday":
                day = 6;
                break;
            case "Saturday":
                day = 7;
                break;
            default:
                break;
                
        }
        $("#subTitleDC").attr("data-day", day);

        /**
         * Get DC Small groups, but only continue through callback
         * @param {type} smallGroups
         * @returns {undefined}
         */
        GetDCSmallGroups(thisDC, function (smallGroups) {
            // Only run if we have smallgroups and a dc
            if (thisDC != "" && smallGroups) {
                console.log('show leader list');

                //Show DC Summary Button
                $('#divDcSummary').show();

                // Show Small group selection list
                ShowSmallGroupLeaderList();
            }
            else {
                console.log(smallGroups);
            }
        });
    });

    var count = 1;
    var clone;
});

/*
 * FormatDate
 * @param {type} date
 * @returns {FormatDate.dateSplit}
 * Formats the date to YYYYMMDD
 */
function FormatDate(date) {
    var dateSplit = date.split("/");
    var newDate = dateSplit[2] + dateSplit[0] + dateSplit[1];
    return newDate;
}

// SELECTING SMALL GROUP LEADER
function SelectSmallGroupLeader() {
} // does nothing
CRM.$(function ($) {
    $('#smallgroupSelect').change(function () {
        if(sgtBlockClicks) { return; }; // do not allow action if we are already processing
        ShowBlockUI(15000);
        var group_id = $('#smallgroupSelect option:selected').attr('data-gid'); // native get of the attribute
        sglId = $('#smallgroupSelect option:selected').attr('data-cid');
        
        ClearGroupMemberStats();
        
        // Update header with leader name
        $('.smallGroupLeaderHeader').text('Small Group Leader: ' + $('#smallgroupSelect option:selected').text());
        
        selectedSgGroupId = group_id; // assign global var for small group group id
        console.log('SelecteSgGroupId: ' + group_id);
        // RESET SOME STUFF
        $('#bAddSmallGroupContact').hide();

        // REMOVE DC SUMMARY TABLE
        //$("[id^=tableSummary]").remove();
        
        $( "#tabs" ).tabs( "option", "active", 0 ); // set to the first tab
        // Reset HISTORY TABLE
        resetAttendanceHistory();
        
	GetLoggedMeetings(GetGroupId()); // get the log meetings

        if (ResetEditLoggedMeeting) ResetEditLoggedMeeting();

        //Hide initial page
        $('#divFirstLoad').hide();

        //Assign Info Section

        $('#divSGInfoTemplate').attr('data-sginfo-sglid', group_id); // assign sglid to data


        //$('#leaderList a.selected').removeClass('selected'); // remove selected tag

        // Highlight Leader Selected
        $(this).addClass('selected');
        // Pass leader_id to get list of members

        $("#subTitleSG").text($('#smallgroupSelect option:selected').text());
        LoadContacts(group_id);
        //HideBlockUI();
    });

});

function InitRefreshLeaderList() {
    CRM.$(function ($) {
        $("#bRefreshLeaderList").click(function ($) {
            if(sgtBlockClicks) { return; }; // do not allow action if we are already processing
            if(debug) console.log('refresh small group leader list');
            if (selectedDcId && !sgtBlockClicks)
                GetDCSmallGroups(selectedDcId);
        });
    });
}

function RemoveSmallGroupContact(cid) {

    CRM.$(function ($) {
        //$("#blockUI-Control").show();
        if(sgtBlockClicks) { 
            console.log('block clicks');
            return; 
        };
        ShowBlockUI(5000);
        CRM.api3('SmallGroupTracking', 'removesmallgroupcontact', {
            "sequential": 1,
            "cid": cid,
            "sgid": selectedSgGroupId
        }).done(function (result) {
            if(debug) console.log('done');
            LoadContacts(selectedSgGroupId); // reload contact list
            //HideBlockUI();
        });

    });
}

/*
 * LOAD CONTACTS
 * 
 * Loads the contacts for the group as well as the information for the member
 * column table
 * 
 * @param {type} group_id
 * @returns {undefined}
 */
function LoadContacts(group_id) {
    contactIds = new Array(); // clear out contact ids
    CRM.$(function ($) {
        var count = 0;
        countInactive = 0;
        countIrregular = 0;
        countRegular = 0;
        countVisitor = 0;

        CRM.api3('SmallGroupTracking', 'getsmallgroupcontacts', {
            'sequential': 1,
            'group_id': group_id
        }).done(function (data) {
            console.log('getsmallgroupcontacts group_id', data);
            
            /**
             * UPDATE MEMBERS TAB
             * @type String
             */
            var updateMembersTab = true;
            
            // clear current list
            $('#memberListBody').empty(); // clear the body content
            $('#attendanceRow').empty(); // clear list
            $('#attendanceRowVisitor').empty(); // clear list
            $('#attendanceRowIrregular').empty(); // clear list
            $('#attendanceRowInactive').empty(); // clear list
            $memberRowList = new Array();
            // Process each of the values that is returned
            $.each(data.values, function (key, value) {
                contactIds.push(value.id);
                //console.log(value);
                if(updateMembersTab) {

                    // update member list
                    var memberRow = $(document.createElement('tr'))
                            .attr('data-cid', value.id);
                    $memberRowList.push(memberRow); // add to the array
                    // Append profile link field
                    memberRow.append($(document.createElement('td'))
                            .append($(document.createElement('a'))
                            .addClass('crm-summary-link')
                            .attr('href', basePath + 'civicrm/profile/view?reset=1&gid=7&id=' + value.id + '&snippet=4')
                            .append($(document.createElement('div'))
                            .append(' ')
                            .addClass('icon')
                            .addClass('crm-icon')
                            .addClass('Individual-icon'))));

                    // Append contact name field
                    memberRow.append($(document.createElement('td'))
                            .append($(document.createElement('a'))
                            .attr('href', basePath + 'civicrm/contact/view?reset=1&cid=' + value.id)
                            .append(value.display_name)));

                    // Append member status field
                    memberRow.append($(document.createElement('td'))
                            .append(value.role));


                    /**
                     * GENERATE DATA IN MEMBERS TAB COLUMNS
                     * 
                     * Loops through each header and uses the header text as the key to lookup the value
                     * executeQuery does not support spaces, so we need to add underscores to the lookup
                     */
                    $('#memberList thead tr th').each(function (index) {
                        if ($(this).data('generated') === true) {
                            // remove time data from dates
                            value[$(this).data('field')] = value[$(this).data('field')].replace(' 00:00:00', '');
                            
                            memberRow.append($(document.createElement('td'))
                                    .append(value[$(this).data('field')]));
                        }
                    });

                    // Only add field for serving area if the header is displayed
                    //if ($('#memberList th:last').data('show') === true) {
                    if ($('#memberList .serving_info').data('show') === true) {
                        memberRow.append($(document.createElement('td'))
                                .append(value.membership_type));
                    }
                    

                    //Append generated row to member list table
                    $('#memberList > tbody:last').append(memberRow);
                } else {
                    console.log('MembersTab Update Disabled!');
                    
                    //$('#clickCheck').text(key + ":" + value);
                    //if(debug) console.log(value.display_name);

                    // ATTENDANCE FORM
                    // must populate with
                    //member_id, activityDate, dcGroupID, dcLeaderID

                    //$row = $('#attendanceRow .rowRef').clone(true).appendTo("#attendanceRow").show();
                    //$row.attr('data-cid',value.id);
                    //$row.find('#memberName').text = value.display_name;
                }

                // ATTENDANCE TABLE
                // Determine which table to put the contact
                var tableRow = "";
                var thisRole = value.role; //handle old option
                if (thisRole == "Member")
                    thisRole = "Regular";
                
                // console.log('role: ' + value.role);
                switch (value.role) {
                    case "Regular":
                        tableRow = '#attendanceRow';
                        countRegular++;
                        break;
                    case "Member": // legacy label
                        tableRow = '#attendanceRow';
                        countRegular++;
                        break;
                    case "Irregular":
                        tableRow = '#attendanceRowIrregular';
                        countIrregular++;
                        break;
                    case "Visitor":
                        tableRow = '#attendanceRowVisitor';
                        countVisitor++;
                        break;
                    case "Inactive":
                        tableRow = '#attendanceRowInactive';
                        countInactive++;
                        break;
                    default:
                        tableRow = '#attendanceRow';
                        countRegular++;
                        break;
                }
                
                // Define element to track member attendance
                var memberAttendance = $(document.createElement('div'))
                        .attr('class', 'rowRef')
                        .attr('data-cid', value.id);
                
                // Build present column
                memberAttendance.append($(document.createElement('span'))
                        .append($(document.createElement('input'))
                            .attr('id', 'present')
                            .attr('type', 'checkbox')
                            .attr('name', 'present')
                            .attr('value', 'true')
                            .attr('data-cid', value.id)));
                
                // Build name column
                memberAttendance.append($(document.createElement('span'))
                        .attr('id', 'memberName')
                        .append(value.display_name));
                
                // Build role select
                var roleSelect = $(document.createElement('select'))
                        .attr('id', 'role')
                        .attr('name', 'role')
                        .attr('data-cid', value.id);
                
                roleSelect.append($(document.createElement('option'))
                        .attr('value', 'Regular')
                        .append('Regular'));
                
                roleSelect.append($(document.createElement('option'))
                        .attr('value', 'Irregular')
                        .append('Irregular'));
                
                roleSelect.append($(document.createElement('option'))
                        .attr('value', 'Visitor')
                        .append('Visitor'));
                
                roleSelect.append($(document.createElement('option'))
                        .attr('value', 'Inactive')
                        .append('Inactive'));
                
                memberAttendance.append($(document.createElement('span'))
                        .append(roleSelect));
                
                // Build notes column
                memberAttendance.append($(document.createElement('span'))
                        .append($(document.createElement('input'))
                            .attr('id', 'notes')
                            .attr('type', 'text')
                            .attr('name', 'notes')
                            .attr('data-cid', value.id)));
                
                //Add button to remove member from smallgroup
                if(value.id !== sglId) {
                    memberAttendance.append($(document.createElement('span'))
                            .append($(document.createElement('button'))
                                .append('Remove')
                                .attr('class', 'button removeContact')
                                .attr('id', 'bRemoveContact')
                                .attr('type', 'button')
                                .attr('data-cid', value.id)));
                } else {
                    memberAttendance.append($(document.createElement('span')));
                }
                    
                $(tableRow).append(memberAttendance);

                // Set Selected Role
                $(tableRow).find("select[data-cid='" + value.id + "']").val(thisRole).prop('selected', true);
                //$('select#role')

                count++;
                
                /*
                 * DO STUFF AFTER WE HAVE GONE THROUGH ALL ROWS
                 */
                if(count == data.values.length) {
                    if(debug) console.log('Contact IDs', contactIds);
                    /*
                    * Show Upcoming Events
                    * @type undefined
                    */
                    $('#upcoming-events').remove('.clone'); // remove any clones
                    GetUpcomingEvents(ShowUpcomingEvents);
                }
            });

            // UPDATE/SHOW GROUP MEMBER STATS
            UpdateGroupMemberStats(countRegular, countIrregular, countVisitor, countInactive);
            //ShowGroupMemberStats();

            // Mark cells that don't have a value
            $('#memberListBody tr td').each(function () {

                if ($(this).text() == "") {
                    $(this).addClass('null');

                }
                //$(this).addClass('null');
                //if(debug) console.log($(this));
            });
            
     
            HideBlockUI();
            //$("#blockUI-Control").hide(); // show loading overlay

            // INIT TABLE FILTER FOR MEMBERLIST TABLE
            $('#member-input-filter').val(''); // clear input text
            jQuery('#memberList').filterTable({
                inputSelector: '#member-input-filter' // use the existing input instead of creating a new one
            });
            
            
            $('#divSGInfoTemplate').show(); // show the cloned section
        });
    });
} // end load contacts

/**
 * Update DOM to show upcoming events
 * @param {type} events
 * @returns {undefined}
 */
function ShowUpcomingEvents(events, callback) {
    CRM.$(function ($) {
        if(debug) console.log('ShowUpcomingEvents - events', events);
        if(debug) console.log('ShowUpcomingEvents - callback', callback);
        eventIds = new Array();
        if (events[0].title != null) {
            $('div#upcoming-events').show();
            //if(debug) console.log(events);
            $.each(events, function (key, event) {
                eventIds.push(event.id);

                var $clone = $('#upcoming-events .template').clone().appendTo('#upcoming-events');
                $clone.removeClass('template').addClass('clone');
                $clone.find('.title').text(event.title);

                // formate date to MM/DD
                var date = event.start_date.substring(0, 11);
                date = date.split('-');
                date = date[1] + "/" + date[2];

                $clone.find('.date').text(date);

            });
            $('#upcoming-events .template').hide(); // hide template 
            //GetEventRegistrations(events); // get contacts registered for upcoming events
            return callback(eventIds, contactIds);
        } else {
            $('#upcoming-events .template').hide(); // hide template 
            // Hide Upcoming Events
            $('div#upcoming-events').hide();
        }

    });
}
/**
 * Updates the table with group member statistics
 * 
 * @param {type} countRegular
 * @param {type} countIrregular
 * @param {type} countVisitor
 * @param {type} countInactive
 * @returns {undefined}
 */
function UpdateGroupMemberStats(countRegular, countIrregular, countVisitor, countInactive) {
    CRM.$('#stat-regularMembers').text(countRegular);
    CRM.$('#stat-irregularMembers').text(countIrregular);
    CRM.$('#stat-visitors').text(countVisitor);
    CRM.$('#stat-inactive').text(countInactive);
}

/**
 * Clears the group member stats table
 * @returns {undefined}
 */
function ClearGroupMemberStats(){
    CRM.$('#stat-regularMembers').text('');
    CRM.$('#stat-irregularMembers').text('');
    CRM.$('#stat-visitors').text('');
    CRM.$('#stat-inactive').text('');
}

/**
 * Hides Group Member Stats Table
 * @returns {undefined}
 */
function HideGroupMemberStats() {
    CRM.$('#divMemberStats').hide();
    ClearGroupMemberStats();
}

/**
 * Shows Group Member Stats Table
 * @returns {undefined}
 */
function ShowGroupMemberStats() {
    CRM.$('#divMemberStats').show();
}


// Object Type
function HeaderRow() {
    this.name = "Name";
}
function TableRow() {
    this.name = "";
}

//var tableHistory; // holds instance of the datatable for history attendance



// GET LIST OF SMALL GROUPS IN A DC
var groupLeaders = new Array(); // holds list of current group leaders
function GetDCSmallGroups(thisDC, callback) {
//    if(debug) console.log("GetDCSmallGroups(" + thisDC + ")");
    var cid;
    var groupLeaderIds = new Array(); // holds list of ids for group leaders
    var val =
            {
                name: "", // group name
                gid: "",
                cid: ""
            };


    groupLeaders = new Array(); // clear group leaders
    var i = 0;
    CRM.$(function ($) {
        $("#divSGInfoTemplate").hide();

        $('#smallgroupSelect option[data-cid]').remove();
        $('#smallgroupSelect').select2('val','');

        // GET CIDs for all contacts that are small group leaders
        if (thisDC) {
            if(sgtBlockClicks) { return; };
            ShowBlockUI(10000);
            CRM.api3('SmallGroupTracking', 'getsmallgroupleaders', {
                "sequential": 1,
                "dc_id": thisDC
            }).done(function (result) {
                console.log(result);
                if(result.values[0]) { // make sure we have groups in this DC
                    
                    $.each(result.values, function (key, value) {
                        val = {}; // clear val
                        // put data into array to pass to DC Summary Page
                        val.cid = value.contact_id;
                        val.gid = value.group_id; // the group id
                        val.name = value.display_name;
                        groupLeaders.push((val)); // holds group leader groups
                        
                        $('#smallgroupSelect').append(
                                $(document.createElement('option'))
                                    .val(val.gid)
                                    .attr('data-gid', val.gid)
                                    .attr('data-cid', val.cid)
                                    .append(val.name));

                    });
                    HideBlockUI();
                    if(callback) callback(true); // 
                }
                else {
                    console.log('no small group leaders in this dc');
                    CRM.alert('There are NO small groups in the selected group!');
                    HideBlockUI();
                    if(callback) callback(false);
                }
            });
        }
    });
}
;

/**
 * Hides the list of small group leaders (and member stats) that can be selected within
 * a selected DC
 * @returns {undefined}
 */
function HideSmallGroupLeaderList(){
    //CRM.$('.smallGroupLeader-section').hide();
    //CRM.$('#divLeftColumn').hide();
    CRM.$('#smallgroupwrapper').hide();
    CRM.$('.smallGroupSummary-section').hide();
    //ClearGroupMemberStats();
}

/**
 * Shows the list of small group leaders in a DC and the member stats
 * @returns {undefined}
 */
function ShowSmallGroupLeaderList(){
    //CRM.$('#divLeftColumn').show();
    CRM.$('#smallgroupwrapper').show();
    CRM.$('.smallGroupSummary-section').show();
    ClearGroupMemberStats();
}

function InitDcSummary() {
    CRM.$(function ($) {
        $("#dcSummaryStartDate").datepicker();
        $("#dcSummaryEndDate").datepicker();

        $("#bRefreshDcSummaryTable").click(function ($) {
            if(sgtBlockClicks) { return; }; // do not allow action if we are already processing
            //CRM.$(this).hide();
            GetDcAttendanceEntries(groupLeaders, groupLeaders.length);
        });
    });
}

// INIT
function Init() {
    InitRefreshLeaderList();
    InitDcSummary();
    //HideGroupMemberStats();
    HideSmallGroupLeaderList();
    
}


// SET TIMEOUTS FOR BLOCK UI OVERLAY IN CASE WE HIT A BUG
var timerId = null;
function SetBlockUITimeout(timeout) {
    timerId = setTimeout("CRM.$('#blockUI-Control').hide(); alert(\'Your last action timed out! Please report this to admin@clearriverchurch.org\')", timeout); // returns id of timeout
    if(debug) console.log(timerId, "SetBlockUITimeout");
}

// Clears the timoue for the blockui overlay if we it is triggered when expected
function ClearBlockUITimeout(id) {
    if(debug) console.log(timerId, "ClearBlockUITimeout");
    clearTimeout(timerId);
    timerId = null;
}

function ShowBlockUI(timeout) {
    sgtBlockClicks = true;
    //if(debug) console.log('ShowBlockUI');
    SetBlockUITimeout(timeout); // set timeout
    CRM.$("#blockUI-Control").show();
    
    /**
     * PROPER WAY TO SHOW BLOCK
     * CRM.$('ELEMENT').block();
     * CRM.$('ELEMENT').unblock();
     * See C:\xampp\htdocs\drupal\sites\all\modules\civicrm\templates\CRM\Contact\Page\View\Summary.js
     */
}

function HideBlockUI() {
    //if(debug) console.log('HideBlockUI');
    CRM.$("#blockUI-Control").hide();
    ClearBlockUITimeout(timerId); // clear any timers we had set
    sgtBlockClicks = false; // allow clicks again
}

/*
 * Get the CID for the selected leader
 * Returns: CID
 */
function GetLeaderCid() {
    return CRM.$('#smallgroupSelect option:selected').attr('data-cid');
}

/**
 * Get the group id for the selected leader
 * @returns group_id
 */
function GetGroupId() {
    return CRM.$('#smallgroupSelect option:selected').attr('data-gid');
}

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function bTabsMembersClick() {}

CRM.$('#bTabsMembers').click(function () {
    // delay running function, otherwise widths won't work
    setTimeout(function () {
        ResetStickyHeaderWidths();
    }, 250);
    // wait until we run
});


/**
 * Get all settings for the extension
 * @param {type} name
 * Name of the setting you want to retrieve
 * @param {type} callback
 * callback to continue function after done
 * @returns {undefined}
 */
function GetSettings(callback) {
    console.log('get setting');
    CRM.api3('SmallGroupTracking', 'getsettings', {
        "sequential": 1,
        "group_name": "org.namelessnetwork.smallgrouptracking",
        //"name": name
    }).done(function (result) {
        //sgt_settings = result['values'];
        console.log("Settings Loaded: ", result['values']);
        if(callback) {
            callback(result['values']);
        }
        else {
            return result['values'];
        }
    });
}

/**
 * Check to see if a contact is a small group leader
 * @param {type} cid
 * @returns {undefined}
 */
function IsSmallGroupLeader(cid) {
    /**
     * Get tags associted with this contact
     */
    CRM.api3('EntityTag', 'get', {
        "sequential": 1,
        "entity_id": cid
    }).done(function (result) {
        
        /**
         * Loop through tags and check for match to small group leader tag
         */
        $.each(result.values, function (key, val) { 
            if(val.tag_id == sgt_settings.small_group_leader_tag) {
                console.log('This is a small group leader')
                return true;
            }
        });
        return false;
    });
}

/*
 * GET LIST OF UPCOMING EVENTS
 * 
 * RETURNS
 * {
        "is_error": 0,
        "version": 3,
        "count": 2,
        "values": [
            {
                "id": "141",    // event id
                "title": "Welcome Lunch",   
                "event_title": "Welcome Lunch",
                "start_date": "2016-08-28 13:00:00",
                "event_start_date": "2016-08-28 13:00:00",
                "price_set_id": ""
            },
            {
                "id": "142",
                "title": "2016 Fall Area Conference",
                "event_title": "2016 Fall Area Conference",
                "start_date": "2016-11-03 19:00:00",
                "event_start_date": "2016-11-03 19:00:00",
                "price_set_id": "23"
            }
        ]
    }
 */
function GetUpcomingEvents(callback) {
    var today = new Date().toJSON().slice(0,10); // gets todays date yyyy-mm-dd
    CRM.api3('Event', 'get', {
        "sequential": 1,
        "return": ["id","title","start_date"],
        "start_date": {">=": today}
    }).done(function (result) {
        // do something
        console.log("UPCOMING EVENTS: ", result.values);
        
        // only call if we have upcoming events
        if(result['count'] > 0) {
            return callback(result.values, GetEventRegistrations);
        }
        //return result.values;
    });
}

/**
 * MEMBERS TAB: Gets upcoming events that group memebers are registered for and updates
 * the DOM to show.
 * @param {type} events
 * @param {type} contacts
 * @returns {undefined}
 */
function GetEventRegistrations(events, contacts) {
    CRM.api3('SmallGroupTracking', 'geteventregistrations', {
        "sequential": 1,
        "contact_id": contacts,
        "event_id": events
    }).done(function (result) {
        if(debug) console.log("GetEventRegistrations", result);
        /*
         * UPDATE DOM
         */
        CRM.$(function ($) {
            //$('#memberList [data-cid="2"]'); // lookup row
            
            /**
             * Loop through the resulting registrations
             * Key is the contact ID
             */
            $.each(result['values'], function (key, data) {
                if(debug) console.log('result-key', key);
                if(debug) console.log('result-data', data);
                var lookup = '#memberList [' + 'data-cid="' + key + '"]';
                var $contactRow = $(lookup);
                if(debug) console.log('contactRow', $contactRow);
                $contactRow.append($(document.createElement('td'))
                    .append(data).addClass('event_reg'));
            });
        });
    });
}

