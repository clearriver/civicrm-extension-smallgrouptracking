/* 
 * Put functions that may be used by any script here
 */

/**
 * Check to see what version of Internet Explorer the user has
 * @returns {Boolean}
 */
function isIE() {
    if (window.attachEvent && !window.addEventListener) {
        return true;
        // "bad" IE
    } else {
        return false;
    }
}