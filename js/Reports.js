/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var debug = true;
var totalWeeks = 52; // weeks in a year
/*
 * New Date Prototype for getting week for given date
 * 
 * source: http://stackoverflow.com/questions/9045868/javascript-date-getweek
 * @param {type} dowOffset
 * @param {type} year
 * optionally pass in year we want to get info for
 * @returns {Number}
 */
Date.prototype.getWeek = function (dowOffset, year) {
/*getWeek() was developed by Nick Baicoianu at MeanFreePath: http://www.meanfreepath.com */

    dowOffset = typeof(dowOffset) == 'number' ? dowOffset : 0; //default dowOffset to zero
    //var newYear = new Date(this.getFullYear(),0,1);
    if(year) {
        var newYear = new Date(year,0,1);
    }else {
        var newYear = new Date(this.getFullYear(),0,1);
    }
    var day = newYear.getDay() - dowOffset; //the day of week the year begins on
    day = (day >= 0 ? day : day + 7);
    var daynum = Math.floor((this.getTime() - newYear.getTime() - 
    (this.getTimezoneOffset()-newYear.getTimezoneOffset())*60000)/86400000) + 1;
    var weeknum;
    //if the year starts before the middle of a week
    if(day < 4) {
        weeknum = Math.floor((daynum+day-1)/7) + 1;
        if(weeknum > 52) {
            nYear = new Date(this.getFullYear() + 1,0,1);
            nday = nYear.getDay() - dowOffset;
            nday = nday >= 0 ? nday : nday + 7;
            /*if the next year starts before the middle of
              the week, it is week #1 of that year*/
            weeknum = nday < 4 ? 1 : 53;
        }
    }
    else {
        weeknum = Math.floor((daynum+day-1)/7);
    }
    return weeknum;
};


/*
 * GET DATE RANGE FOR A GIVEN WEEK
 * @param {type} weekNo
 * @returns {Array}
 */
function getDateRangeOfWeek(weekNo, year) {
    var d1 = new Date();
    var numOfdaysPastSinceLastMonday = eval(d1.getDay() - 1);
    d1.setDate(d1.getDate() - numOfdaysPastSinceLastMonday);
    //var weekNoToday;
    if(year) {
       var weekNoToday = d1.getWeek(0,year);
    } else {
       var weekNoToday = d1.getWeek();
    }
    var weeksInTheFuture = eval(weekNo - weekNoToday);
    d1.setDate(d1.getDate() + eval(7 * weeksInTheFuture));
    var rangeIsFrom = eval(d1.getMonth() + 1) + "/" + d1.getDate() + "/" + d1.getFullYear();
    d1.setDate(d1.getDate() + 6);
    var rangeIsTo = eval(d1.getMonth() + 1) + "/" + d1.getDate() + "/" + d1.getFullYear();
    
    var range = new Array(); 
    range[0] = rangeIsFrom; 
    range[1] = rangeIsTo; 
    return range;
    //return rangeIsFrom + " to " + rangeIsTo;
}

/*
 * GETS CURRENT WEEK NUMBER BASED ON TODAY
 */
function getWeekNumber(d) {
    // Copy date so don't modify original
    d = new Date(+d);
    d.setHours(0,0,0,0);
    // Set to nearest Thursday: current date + 4 - current day number
    // Make Sunday's day number 7
    d.setDate(d.getDate() + 4 - (d.getDay()||7));
    // Get first day of year
    var yearStart = new Date(d.getFullYear(),0,1);
    // Calculate full weeks to nearest Thursday
    var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);
    // Return array of year and week number
    return weekNo;
}



/*
 * GET DATE RANGES FOR ALL SMALL GROUP MEETINGS IN GIVEN YEAR
 * Loop through each week until we get to current week. 
 * Store dates in object
 */
function GetDates() {
    var currentWeek = getWeekNumber(new Date());
    if(debug) console.log('It\'s currently week ' + currentWeek);
    
    var weekDates = new Object();
    
    /*
     * Loop through all weeks up to current, storing dates
     * i = week number
     */
    for(var i = 1; i < currentWeek; i++) {
        var range = getDateRangeOfWeek(i);
        weekDates[i] = new Object();
        weekDates[i]['start_date'] = range[0];
        weekDates[i]['end_date'] = range[1];
    }
    
    if(debug) console.log('Date ranges for weeks', weekDates);
}


function GetReport() {
    if(debug) console.log('GetReport()');
    CRM.$(function ($) {
        /**
         * Get the logged meetings for the group within given date range
         * and assign the values to the header
         * 
         * NOTE: This is redundant. We currently get the logged meetings via
         * the API
         */
        var year = 2016;
        CRM.api3('SmallGroupTracking', 'getreport', {
            "sequential": 1,
            "year": year
        }).done(function (result) {
            //TODO: Wrap loops over weeks so that it isn't done in each function separately
            if(debug) console.log('GetReport Result',result);
            //BuildDCRows(result['values']['dc']);
            BuildWeekHeader(result['values']['weekHeader'], result['values']['week_attendance_total']);
            BuildDCTable(result['values']['dc']);
            BuildAttendance(result['values']['attendance'], SetSticky);
            BuildDCAttendanceTotal(result['values']['dc_attendance_total']);
            //$scope.order = [  ]
        });
    });
}
jQuery(document).ready(function($) {
    GetReport();
    
    // Set sticky column
    //jQuery("table.dc_table").tableHeadFixer({'left' : 1, 'head' : false});
    //$('#tableReport').stickyTableHeaders({fixedOffset: $('#civicrm-menu')});
});

function SetSticky() {
    if(debug) console.log('SetSticky()');
   // need to update jquery version on other hosts
   // jQuery(".report-section").tableHeadFixer({'left' : 1, 'head' : false});

}

/**
 * Adds the weeks to the header
 * @param {type} weeks
 * @returns {undefined}
 */
function BuildWeekHeader(weeks, week_attendance_total) {
    //if(debug) console.log('BuildWeekHeader()',weeks);
    CRM.$(function ($) {
        for(var key in weeks) {
            $('#tableReport thead tr').append("<th class='date' style='min-width:75px'>" + weeks[key] + "</th>");
            
            BuildWeekAttendanceTotal(key, week_attendance_total); // display the totals for the week
            //BuildDCAttendanceTotal(key);
        }
    });
}

function BuildDCAttendanceTotal(dc_attendance_total) {
    if(debug) console.log('BuildDCAttendanceTotal(dc_attendance_total)', dc_attendance_total);
    CRM.$(function ($) {
        // Loop all weeks
        for (var i = 1; i < 53; i++) {
            // Loop DC pastor in each week
            for (var dcid in dc_attendance_total[i]) {
               // if(debug) console.log('dc_attendance_total[i][dcid]', dc_attendance_total[i][dcid]);
               $('#divReport').find("table.dc_table[data-dcid='" + dcid + "'] tfoot tr").append("<td class='dc_week_total'>" + dc_attendance_total[i][dcid] + "</td>");
            }
        }
    });
}

/*
 * Update DOM to show the attendance totals for all DCs across the week
 * @returns {undefined}
 */
function BuildWeekAttendanceTotal(key, week_attendance_total) {
    // 
    CRM.$('tr.week_attendance_total_row').append("<td class='week_attendance_total_"+key+"' style='min-width:75px'>" + week_attendance_total[key] + "</th>");
}

/*
 * Creates a table for each DC pastor
 * @param {type} dc
 * @returns {undefined}
 */
function BuildDCTable(dc, callback) {
    if(debug) console.log('BuildDCTable(dc)',dc);
    CRM.$(function ($) {
        for (var key in dc) {
            for (var i = 0; i < dc[key].length; i++) {
                var table = $('<table data-order="' + dc[key][i]['order'] + '"></table>').addClass('dc_table').addClass(dc[key][i]['day']).attr('data-dcid', dc[key][i]['id']);
                var row = $('<tr></tr>').addClass('dc_pastor');
                var col = $('<td colspan="54"></td>').text(dc[key][i]['title']).css('min-width','200px');
                
                // FOOTER FOR DC ATTENDANCE TOTAL
                var tfoot = $('<tfoot></tfoot>');
                var foot_tr = $('<tr></tr>').addClass('dc_attendance_total_row');
                var foot_col = $('<tr class="dc_attendance_total_row"><td colspan="1">Total</td></tr>');
                
                row.append(col);
                tfoot.append(foot_col);
                
                table.append(row).append(tfoot);
                
                // Header Dates
                $('#tableReport .week_dates').clone(true).appendTo(table);
                
                $('#' + dc[key][i]['day']).append(table); // place table in proper section
            }
          
        }
       if(callback)return callback();
    });
}

/**
 * Generates the tables for group leader attendance
 * 
 * @param {type} attendance
 * @param {type} callback
 * @returns {undefined}
 */
function BuildAttendance(attendance, callback) {
     if(debug) console.log('BuildAttendance(attendance, callback)',attendance, callback);
    CRM.$(function ($) {
        for(var key in attendance) {
            var leader_id = key;
            
            // ADD GROUP LEADER ROW
            $('#divReport').find("[data-dcid='" + attendance[leader_id][1]['dc_group_id'] +"']").append(
                "<tr class='meeting_row' data-cid="+leader_id+">" +
                "<td class='group_leader' style='min-width: 200px;'>"+attendance[leader_id][1]['group_leader']+"</td>" 
            );    
            $('#divReport').find("[data-dcid='" + attendance[leader_id][1]['dc_group_id'] +"']").append("</tr>");   // find dc table
           
           /*
            * Loop through all weeks for the small group leader
            * @type type|BuildAttendance.attendance
            */
            for(var week in attendance[leader_id]) {
                var meeting_class = ''; // css class for the meeting type
                
                /*
                 * Set class for meeting type
                 */
                switch(attendance[leader_id][week]['meeting_type']) {
                    case "Party":
                        meeting_class = "meeting_type_party";
                        break;
                    case "Discussion":
                        meeting_class = "meeting_type_discussion";
                        break;
                    case "No Meeting":
                        meeting_class = "meeting_type_none";
                        break;
                    case "DC":
                        meeting_class = "meeting_type_dc";
                        break;
                    case "missing":
                        meeting_class = "meeting_type_missing";
                        break;
                    default:
                        //if(debug) console.log('BuildAttendance()', 'no matching meeting');
                        break;
                }
                
                 $('#divReport').find("[data-dcid='" + attendance[leader_id][1]['dc_group_id'] +"']").find("[data-cid='" + leader_id +"']").append(                     
                    "<td class='present_count " + meeting_class + "' style='min-width:75px;text-align:center'>"+attendance[leader_id][week]['present_count']+"</td>"
                    );
            }
        }
        return callback();
    });
    
}


function BuildDCRows(dc) {
    CRM.$(function ($) {
        // Loop through and get all the weeks for headers
        if(dc.monday.length > 0) {
            for(var pastor in dc.monday) {
                
            }
        }
        for (var day in dc) {
            var day = result.values[key];
            for (var week in obj) {
                if (debug)
                    console.log('week', week);

            }
        }
    });
}
/*
function resetAttendanceHistory() {
    var appElement = document.getElementById("ReportApp");
    var $scope = angular.element(appElement).scope();
    $scope.$apply(function() {
        $scope.attendees = [];
    });
}
*/
/**
 * Prefix an integer to make it a specific length
 * @param {type} number
 * @param {type} len
 * @returns {unresolved}
 */
function PrefixInt(number, len) {
   return (Array(len).join('0') + number).slice(-length);
}

CRM.$(function($) {
    
    /*
     * GET CURRENT DATE
     */
    var today = new Date().toJSON().slice(0,10); //yyyy-mm-dd
    var temp = today.split("-");
    var endDate = temp[1] + "/" + temp[2] + "/" + temp[0];
    
    /*
     * GET DATE 3 MONTHS EARLIER
     */
    var startDate = new Date();
    startDate.setDate(1); // set day of month
    startDate.setMonth(startDate.getMonth()-3); // sets the month
    startDate = startDate.toJSON().slice(0,10);
    temp = startDate.split("-");
    startDate = temp[1] + "/" + temp[2] + "/" + temp[0];
       
    $(document).ready(function() {
        $('#divDateRange #endDate').val(endDate);
        $('#divDateRange #startDate').val(startDate);
        $('#AttendanceHistoryApp input[type="checkbox"]').prop('checked',true); // check all boxes by default
    });
   
   CRM.$('#meeting_dc').click(function() {
       console.log('click');
      if($(this).prop('checked')) {
          $(this).removeProp('checked');
      }
   });
   
   /*
    * ALWAYS KEEP AT LEAST ONE CHECKBOX FOR MEETING TYPE
    */
   var checkboxCount = 4; // holds how many are checked
   CRM.$('#AttendanceHistoryApp input[type="checkbox"]').on('click', function(event) {
       if(!CRM.$(this).prop('checked') && checkboxCount > 1) {
           checkboxCount--; // reduce num checked
       }
       else if (CRM.$(this).prop('checked')) {
           checkboxCount++; //increase count
       }
       else {
           CRM.$(this).prop('checked',true); //don't allow less than 1 checkbox
       }
   });
    
});

GetDates();

