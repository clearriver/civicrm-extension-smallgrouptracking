CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `civicrm_contact_view` AS
    SELECT 
        `civicrm_contact`.`id` AS `id`,
        `civicrm_contact`.`modified_date` AS `modified_date`,
        `civicrm_contact`.`created_date` AS `created_date`,
        `civicrm_contact`.`contact_sub_type` AS `contact_sub_type`,
        `civicrm_contact`.`display_name` AS `display_name`,
        `civicrm_contact`.`last_name` AS `last_name`,
        `civicrm_contact`.`first_name` AS `first_name`,
        `civicrm_membership_type`.`name` AS `member_type`,
        `civicrm_value_assimilations_4`.`foundations_truth_17` AS `foundations_truth`,
        `civicrm_value_assimilations_4`.`foundations_freedom_18` AS `foundations_freedom`,
        `civicrm_value_assimilations_4`.`foundations_service_19` AS `foundations_service`,
        `civicrm_value_assimilations_4`.`foundations_community_20` AS `foundations_community`,
        `civicrm_value_assimilations_4`.`mbt_week_1_165` AS `mbt1`,
        `civicrm_value_assimilations_4`.`mbt_week_2_166` AS `mbt2`,
        `civicrm_value_assimilations_4`.`mbt_week_3_167` AS `mbt3`,
        `civicrm_value_assimilations_4`.`mbt_week_4_168` AS `mbt4`,
        `civicrm_value_assimilations_4`.`mbt_week_5_169` AS `mbt5`,
        `civicrm_value_assimilations_4`.`mbt_week_6_170` AS `mbt6`,
        `civicrm_value_small_group_32`.`group_leader_115` AS `group_leader_115`,
        `civicrm_value_small_group_32`.`small_group_leader_147` AS `small_group_leader_147`,
        `civicrm_value_small_group_32`.`small_group_member_label_158` AS `small_group_member_label_158`,
        `civicrm_value_core_events_34`.`summer_conference_122` AS `summer_conference_122`,
        `civicrm_value_core_events_34`.`winter_conference_123` AS `winter_conference_123`,
        `civicrm_value_core_events_34`.`newcomer_lunch_124` AS `newcomer_lunch_124`,
        `civicrm_value_core_events_34`.`fall_retreat_125` AS `fall_retreat_125`,
        `civicrm_value_core_events_34`.`small_group_leader_retreat_126` AS `small_group_leader_retreat_126`,
        `civicrm_value_core_events_34`.`learning_to_pray_for_others_127` AS `learning_to_pray_for_others_127`,
        `civicrm_contact_serving_view`.`membership_type` AS `serving_area`,
        `civicrm_entity_tag`.`tag_id` AS `tag_id`
    FROM
        (((((((`civicrm_contact`
        LEFT JOIN `civicrm_membership` ON ((`civicrm_contact`.`id` = `civicrm_membership`.`contact_id`)))
        LEFT JOIN `civicrm_membership_type` ON ((`civicrm_membership`.`membership_type_id` = `civicrm_membership_type`.`id`)))
        LEFT JOIN `civicrm_value_assimilations_4` ON ((`civicrm_contact`.`id` = `civicrm_value_assimilations_4`.`entity_id`)))
        LEFT JOIN `civicrm_value_core_events_34` ON ((`civicrm_contact`.`id` = `civicrm_value_core_events_34`.`entity_id`)))
        LEFT JOIN `civicrm_value_small_group_32` ON ((`civicrm_contact`.`id` = `civicrm_value_small_group_32`.`entity_id`)))
        LEFT JOIN `civicrm_entity_tag` ON (((`civicrm_contact`.`id` = `civicrm_entity_tag`.`entity_id`)
            AND (`civicrm_entity_tag`.`tag_id` = 6))))
        LEFT JOIN `civicrm_contact_serving_view` ON ((`civicrm_contact_serving_view`.`id` = `civicrm_contact`.`id`)))
    WHERE
        ((`civicrm_contact`.`first_name` IS NOT NULL)
            AND (`civicrm_contact`.`last_name` IS NOT NULL)
            AND (`civicrm_contact`.`contact_type` = 'Individual')
            AND (`civicrm_contact`.`is_deleted` = 0))
    GROUP BY `civicrm_contact`.`id`