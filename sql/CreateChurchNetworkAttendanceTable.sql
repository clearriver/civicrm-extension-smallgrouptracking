/* 
 * Creates the table necessary for recording church attendance info
 */
/**
 * Author:  lee.gooding
 * Created: Feb 15, 2017
 */

CREATE TABLE custom_churchnetwork_attendance (
  id int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  Type varchar(255) DEFAULT NULL COMMENT 'DC, Sunday, Team Meeting',
  Adult_Count int(4) DEFAULT NULL,
  Child_Count int(4) DEFAULT NULL,
  Date date DEFAULT NULL,
  dc_group_id int(11) DEFAULT NULL COMMENT 'The DC group ID associated with this entry.',
  PRIMARY KEY (id),
  UNIQUE INDEX id (id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;