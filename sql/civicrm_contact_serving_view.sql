CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `civicrm_contact_serving_view` AS
    SELECT 
        `civicrm_contact`.`id` AS `id`,
        `civicrm_contact`.`display_name` AS `display_name`,
        `civicrm_contact`.`last_name` AS `last_name`,
        `civicrm_contact`.`first_name` AS `first_name`,
        GROUP_CONCAT(DISTINCT `civicrm_membership_type`.`name`
            SEPARATOR ',') AS `membership_type`
    FROM
        ((`civicrm_contact`
        LEFT JOIN `civicrm_membership` ON ((`civicrm_contact`.`id` = `civicrm_membership`.`contact_id`)))
        LEFT JOIN `civicrm_membership_type` ON ((`civicrm_membership`.`membership_type_id` = `civicrm_membership_type`.`id`)))
    WHERE
        ((`civicrm_contact`.`first_name` IS NOT NULL)
            AND (`civicrm_contact`.`last_name` IS NOT NULL)
            AND (`civicrm_contact`.`contact_type` = 'Individual')
            AND (`civicrm_contact`.`is_deleted` = 0))
    # GROUP BY `civicrm_contact`.`last_name` , `civicrm_contact`.`first_name`
    GROUP BY `civicrm_contact`.`id`
    ORDER BY `civicrm_contact`.`last_name`