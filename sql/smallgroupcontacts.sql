
SELECT 
    civicrm_contact_view.id AS id,
    civicrm_contact_view.display_name AS display_name,
    civicrm_contact_view.last_name AS last_name,
    civicrm_contact_view.first_name AS first_name,
    GROUP_CONCAT(DISTINCT civicrm_contact_view.member_type
        SEPARATOR ',') AS service_type,
    civicrm_contact_view.foundations_truth AS foundations_truth,
    civicrm_contact_view.foundations_freedom AS foundations_freedom,
    civicrm_contact_view.foundations_service AS foundations_service,
    civicrm_contact_view.foundations_community AS foundations_community,
    `civicrm_contact_view`.`mbt1`,
    `civicrm_contact_view`.`mbt2`,
    `civicrm_contact_view`.`mbt3`,
    `civicrm_contact_view`.`mbt4`,
    `civicrm_contact_view`.`mbt5`,
    `civicrm_contact_view`.`mbt6`,
    `civicrm_contact_view`.`serving_area`,
    civicrm_contact_view.small_group_member_label_158 AS small_group_member_label_158,
    civicrm_contact_view.small_group_leader_147 AS small_group_leader_147,
    civicrm_contact_view.summer_conference_122 AS summer_conference_122,
    civicrm_contact_view.winter_conference_123 AS winter_conference_123,
    civicrm_contact_view.newcomer_lunch_124 AS newcomer_lunch_124,
    civicrm_contact_view.fall_retreat_125 AS fall_retreat_125,
    civicrm_contact_view.small_group_leader_retreat_126 AS small_group_leader_retreat_126,
    civicrm_contact_view.learning_to_pray_for_others_127 AS learning_to_pray_for_others_127,
    civicrm_contact_view.tag_id AS tag_id,
    civicrm_contact_groups_view.group_id AS group_id
   FROM (
       civicrm_contact_view
       LEFT JOIN civicrm_contact_groups_view ON (
                       civicrm_contact_groups_view.contact_id = civicrm_contact_view.id
                       )
               )
