CREATE TABLE `clearriver_civicrmnew`.`custom_smallgroup_meeting_log` (
  `custom_smallgroup_attendance_log_id` INT NOT NULL AUTO_INCREMENT,
  `meeting_date` DATE NULL,
  `group_leader_cid` INT NULL COMMENT 'contact id for the actual small group leader',
  `meeting_type` VARCHAR(255) NULL,
  `group_group_id` INT NULL COMMENT 'Small group group ID',
  PRIMARY KEY (`custom_smallgroup_attendance_log_id`),
  # UNIQUE INDEX `meeting_date_UNIQUE` (`meeting_date` ASC))
COMMENT = 'keeps log of all meetings for small groups. Primarily used a' /* comment truncated */ /*s a reference to see all meetings for a groupK*/;

