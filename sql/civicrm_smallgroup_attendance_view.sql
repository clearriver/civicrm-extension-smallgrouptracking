# Looks up small group attendance history for a contact via activity
select 
civicrm_contact.id as cid,
civicrm_contact.display_name,
civicrm_activity.subject,
civicrm_activity.activity_date_time,
civicrm_activity.details,
civicrm_activity.location,
civicrm_activity.status_id,     # 11:present, 12:absent    
# civicrm_activity.source_record_id,
# civicrm_activity_contact.source_contact_id, # small group leader id
civicrm_activity_contact.record_type_id as record_type_id,
# civicrm_activity_contact.id as activity_id,
# civicrm_activity_contact.contact_id as activity_cid,
civicrm_activity.activity_type_id as activity_type_id
FROM civicrm_contact
left join civicrm_activity_contact on civicrm_activity_contact.contact_id = civicrm_contact.id
left join civicrm_activity on civicrm_activity.id = civicrm_activity_contact.activity_id
where activity_type_id = 58
and record_type_id = 3 # target not source contact
# and civicrm_contact.id = 2881 # small group attendance activity type
order by civicrm_activity.activity_date_time
