SELECT 
member_id,
sg_group_id,
group_leader_id,
civicrm_contact.display_name,
activity_date,
activity_type,
member_role,
attended,
notes
FROM clearriver_civicrm.custom_smallgroup_attendance
left join civicrm_contact on civicrm_contact.id = custom_smallgroup_attendance.member_id
where sg_group_id = 38
and member_id = 5321
and ((activity_date > 20141101) and (activity_date < 20150301))
order by activity_date
#group by member_id;