SELECT 
        `civicrm_contact`.`id` AS `id`,
        `civicrm_contact`.`last_name` AS `last_name`,
        `civicrm_contact`.`first_name` AS `first_name`,
        `civicrm_contact`.`display_name` AS `display_name`,
        civicrm_group_contact.group_id as group_id,
         GROUP_CONCAT(DISTINCT `civicrm_membership_type`.`name` SEPARATOR ',')  AS `membership_type`,
         civicrm_value_small_group_55.small_group_member_role_196 as role
    FROM
        `civicrm_contact`
        INNER JOIN civicrm_group_contact on civicrm_contact.id = civicrm_group_contact.contact_id AND civicrm_group_contact.status='Added' # join for contacts that are in a group
        LEFT JOIN `civicrm_membership` ON `civicrm_contact`.`id` = `civicrm_membership`.`contact_id` AND civicrm_membership.status_id = 2
        LEFT JOIN `civicrm_membership_type` ON `civicrm_membership`.`membership_type_id` = `civicrm_membership_type`.`id` 
        LEFT JOIN civicrm_value_small_group_55 ON civicrm_contact.id = civicrm_value_small_group_55.entity_id
    WHERE
        ((`civicrm_contact`.`first_name` IS NOT NULL)
            AND (`civicrm_contact`.`last_name` IS NOT NULL)
            AND (`civicrm_contact`.`is_deleted` = 0)
            AND `civicrm_contact`.`contact_type` = 'Individual'
			AND civicrm_group_contact.group_id = 211
            )
	GROUP BY `civicrm_contact`.`id`
    # ORDER BY `civicrm_contact`.`last_name`