CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `civicrm_contact_groups_view` AS
    SELECT 
        GROUP_CONCAT(`civicrm_group_contact`.`group_id`
            SEPARATOR ',') AS `group_id`,
        `civicrm_contact`.`id` AS `contact_id`,
        GROUP_CONCAT(`civicrm_group`.`name`
            SEPARATOR ',') AS `name`,
        GROUP_CONCAT(`civicrm_group`.`title`
            SEPARATOR ',') AS `title`,
        `civicrm_group`.`children` AS `children`,
        `civicrm_group`.`parents` AS `parents`,
        `civicrm_contact`.`display_name` AS `display_name`
    FROM
        ((`civicrm_group_contact`
        LEFT JOIN `civicrm_contact` ON (((`civicrm_contact`.`id` = `civicrm_group_contact`.`contact_id`)
            AND (`civicrm_group_contact`.`status` = 'Added'))))
        LEFT JOIN `civicrm_group` ON ((`civicrm_group`.`id` = `civicrm_group_contact`.`group_id`)))
    WHERE
        ((`civicrm_contact`.`contact_type` = 'Individual')
            AND (`civicrm_group`.`title` LIKE '%Small Group%')
            AND (`civicrm_contact`.`is_deleted` = 0))
    GROUP BY `civicrm_contact`.`id`