CREATE TABLE `custom_smallgroup_attendance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `activity_date` date DEFAULT NULL,
  `sg_group_id` int(11) DEFAULT NULL,
  `dc_leader_id` int(11) DEFAULT NULL,
  `group_leader_id` int(11) DEFAULT NULL,
  `activity_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `member_role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attended` int(1) DEFAULT '0',
  `notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `member_id` (`member_id`,`activity_date`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Manage';

