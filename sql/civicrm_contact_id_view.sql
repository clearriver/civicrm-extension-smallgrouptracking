CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `civicrm_contact_id_view` AS
    SELECT 
        `civicrm_contact`.`id` AS `id`,
        `civicrm_contact`.`last_name` AS `last_name`,
        `civicrm_contact`.`first_name` AS `first_name`,
        `civicrm_contact`.`display_name` AS `display_name`
    FROM
        `civicrm_contact`
    WHERE
        ((`civicrm_contact`.`first_name` IS NOT NULL)
            AND (`civicrm_contact`.`last_name` IS NOT NULL)
            AND (`civicrm_contact`.`is_deleted` = 0))
    ORDER BY `civicrm_contact`.`last_name`