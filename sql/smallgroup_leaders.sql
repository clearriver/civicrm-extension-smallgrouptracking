# creates a view that has the DC group, SG group, Leader ID, Group Leader Display_name
# will lookup with a where filter for the parent id
# CREATE 
#    ALGORITHM = UNDEFINED 
#    DEFINER = `root`@`localhost` 
#    SQL SECURITY DEFINER
# VIEW `smallgroup_leaders_view` AS
select 
civicrm_group.parents as parent_id,
civicrm_group.title,
civicrm_group_contact.group_id,
civicrm_contact.id as contact_id,
civicrm_contact.display_name
# civicrm_contact.last_name
from civicrm_group_contact
left join civicrm_contact on civicrm_contact.id = civicrm_group_contact.contact_id
left join civicrm_entity_tag on civicrm_entity_tag.entity_id = civicrm_contact.id
left join civicrm_group on civicrm_group.id = civicrm_group_contact.group_id and civicrm_group_contact.status = 'Added'
where civicrm_entity_tag.tag_id = 6 # ensure the contact is a small group leader
# and civicrm_group.parents = 34 # the DC we should filter by
# and civicrm_group_contact.status = "Added"
# and group_id = 46
and civicrm_group.parents = 46
order by civicrm_contact.last_name
