CREATE TABLE `custom_smallgroup_meeting_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meeting_date` date DEFAULT NULL,
  `group_leader_cid` int(11) DEFAULT NULL COMMENT 'contact id for the actual small group leader',
  `meeting_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_group_id` int(11) DEFAULT NULL COMMENT 'Small group group ID',
  `present_count` int(2) DEFAULT '0',
  `dc_group_id` int(11) DEFAULT '0' COMMENT 'the group id for the selected DC',
  `is_deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='keeps log of all meetings for small groups. Primarily used a';

