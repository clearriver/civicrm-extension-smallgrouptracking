# http://stackoverflow.com/questions/21841353/mysql-delete-under-safe-mode
# just delete by a temp table

DELETE FROM custom_smallgroup_attendance WHERE id IN (
SELECT temp.id FROM (
	SELECT id FROM custom_smallgroup_attendance WHERE activity_date = '2014-10-08' AND group_leader_id = 2
  ) AS temp
);